# GNB No es un Banco (english below)

![Portada](https://gnb.komun.org//static/assets/images/logo.png)


GNB es un software diseñado para cooperativas integrales, asociaciones o particulares, que facilita la gestión de sus finanzas y fiscalidad interna. GNB significa en inglés "(G)NB-is (N)ot a (B)ank" o en español "(G)NB (N)o es un (B)anco", una sigla recursiva en homenaje al movimiento de software libre.

## Funcionalidades Clave

### Subcuentas Virtuales
GNB permite tener una única cuenta bancaria con infinitas subcuentas virtuales para los miembros de la cooperativa o asociación. Esto es especialmente útil para capacitar financieramente a los excluidos por el sistema, como activistas, insolventes, y personas sin papeles. Cada usuario puede ver sus saldos, realizar y ver sus transferencias, tanto internas con otros usuarios así como externas (SEPA, retiradas en cajero, etc.)

### Gestión Financiera
Además de las subcuentas virtuales, GNB facilita la creación de facturas y gastos, la gestión de proyectos (cada uno con sus propias cuentas virtuales), y las transferencias entre usuarios. Esto permite una administración detallada y organizada de los recursos financieros.

### Federación y Privacidad
GNB está diseñado para federarse con otras instancias de GNB, creando un ecosistema en el que entidades afines, incluso en diferentes países y usando diferentes monedas, pueden realizar intercambios de manera segura y no trazable. Esto asegura que las transferencias económicas entre distintos colectivos sean lo más privadas posibles.

### Pagos y Transparencia
GNB es muy útil para activar pagos en línea vía tarjeta de crédito, así como pagos recurrentes (por ejemplo, cuotas de socios) tanto internos como externos. También permite llevar una transparencia hacia todos los integrantes, así como un registro detallado de todos los préstamos realizados.

## Robustez y Soporte
Para garantizar su robustez, GNB utiliza el principio de la doble contabilidad y se apoya en ODOO, un sistema ERP libre utilizado por muchas cooperativas. 

Para más información, ver tutoriales y seguir nuestros progresos, puedes visitar nuestro [foro](https://foro.komun.org/c/gnb/21) en Komun.org.

# GNB is Not a Bank

GNB is software designed for integral cooperatives, associations, or individuals, facilitating the management of their finances and internal taxation. GNB stands for "(G)NB-is (N)ot a (B)ank," a recursive acronym in homage to the free software movement GNU.

## Key Features

### Virtual Subaccounts
GNB allows having a single bank account with infinite virtual subaccounts for the members of the cooperative or association. This is especially useful for financially empowering those excluded by the system, such as activists, insolvents, and undocumented individuals. Each user can view their balances, conduct and track transfers, both internal with other users and external (SEPA, ATM withdrawals, etc.).

### Financial Management
In addition to virtual subaccounts, GNB facilitates the creation of invoices and expenses, project management (each with its own virtual accounts), and transfers between users. This enables detailed and organized administration of financial resources.

### Federation and Privacy
GNB is designed to federate with other instances of GNB, creating an ecosystem in which like-minded entities, even in different countries and using different currencies, can securely and non-traceably conduct exchanges. This ensures that financial transfers between various collectives are as private as possible.

### Payments and Transparency
GNB is very useful for activating online payments via credit card, as well as recurring payments (for example, member fees) both internally and externally. It also allows for transparency among all members and provides a detailed record of all loans made.

## Robustness and Support
To ensure its robustness, GNB employs the principle of double-entry accounting and relies on ODOO, a free ERP system used by many cooperatives. 

For more information, tutorials, and to follow our progress, you can visit our [forum](https://foro.komun.org/c/gnb/21) on Komun.org.

## Screenshots

### Transaction History
![Transaction History](https://foro.komun.org/uploads/default/optimized/1X/1810ccede03bac796f36169d13d4d65b8ee3aef2_2_690x432.png)

### Dashboard
![Dashboard](https://foro.komun.org/uploads/default/optimized/1X/bba392ad1241ba86768154446d55b5f29a6e11ca_2_690x364.png)

### Invoices
![Invoices](https://foro.komun.org/uploads/default/optimized/1X/9d8b3bd0e71fcae47ae404154aa58e41935b36f8_2_690x169.png)

### Balance Summary
![Balance Summary](https://foro.komun.org/uploads/default/optimized/1X/869e847f1859b85c4c4f46853d0aa6055653ba42_2_690x157.png)

### Create New Invoices
![Create New Invoices](https://foro.komun.org/uploads/default/optimized/1X/c2fbff8280e3bba29b8ecc04e1eebb9d310d73ed_2_594x500.png)

### Create New Expenses
![Create New Expenses](https://foro.komun.org/uploads/default/optimized/1X/faafd268affc4e72d52f424bb791c843067bc96e_2_497x500.png)
