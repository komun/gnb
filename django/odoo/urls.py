from django.urls import path
from . import views

app_name = "odoo"  # Opcional, pero útil para evitar conflictos de nombres

urlpatterns = [
    path("client_search/<str:client_info>/", views.client_search, name="client_search"),
    path("invoice/", views.InvoiceView.as_view(), name="invoice"),
    path(
        "invoice/edit_line/",
        views.edit_invoice_line,
        name="edit_invoice_line",
    ),
    path(
        "invoice/edit_client/",
        views.edit_invoice_contact,
        name="edit_invoice_contact",
    ),
    path(
        "invoice/validate/<int:invoice_id>/",
        views.validate_invoice,
        name="validate_invoice",
    ),
    path(
        "invoice/correct/<int:invoice_id>/",
        views.correct_invoice,
        name="correct_invoice",
    ),
    path(
        "invoice/duplicate/<int:invoice_id>/",
        views.duplicate_invoice,
        name="duplicate_invoice",
    ),
    path(
        "invoice/download/<int:invoice_id>/",
        views.download_invoice,
        name="download_invoice",
    ),
    path("expense/", views.ExpenseView.as_view(), name="expense"),
    path(
        "expense/validate/<int:expense_sheet_id>/",
        views.validate_expense_sheet,
        name="validate_expense",
    ),
    path("account_move/", views.BankAccountMoveView.as_view(), name="account_move"),
    path(
        "cash_account_move/",
        views.CashAccountMoveView.as_view(),
        name="cash_account_move",
    ),
    path(
        "delete_account_move/",
        views.delete_account_move,
        name="delete_account_move",
    ),
    path(
        "connection-status/",
        views.odoo_connection_status,
        name="odoo_connection_status",
    ),
]
