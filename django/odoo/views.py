from colorama import Fore, Style, init
from pprint import pprint
import json
import base64
import traceback

from collections import defaultdict
import datetime

from django.http import (
    JsonResponse,
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseNotFound,
)
from . import odoo_server

import gnb.utils
import odoo.utils
import odoo.accounts
import odoo.expenses
import odoo.invoices
from odoo.models import OdooProfile

from django.utils import timezone

from gnb.models import (
    Project,
    CashAccount,
    Payment,
    PaymentScope,
    PaymentType,
    PaymentStatus,
)

# from odoo.spanish_vat import validar_vat_cif
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.safestring import mark_safe

init()
import logging

_logger = logging.getLogger("GNB")


def odoo_connection_status(request):
    is_connected = odoo_server.IsAuthenticated()
    return JsonResponse({"is_connected": is_connected})


def client_search(request, client_info):
    print(Fore.YELLOW + f"Searching for clients: {client_info} in Odoo")
    print(Style.RESET_ALL)

    partners_json = []

    # Se empezará a buscar cuando se escriban más de 2 caracteres
    if len(client_info) > 2:
        partner_ids = odoo_server.Search("res.partner", [("vat", "ilike", client_info)])
        partner_ids += odoo_server.Search(
            "res.partner", [("name", "ilike", client_info)]
        )
        if len(partner_ids) > 0:
            print(Fore.GREEN + f"Found clients with : {client_info}")
            print(Style.RESET_ALL)
            fields_to_show = [
                "id",
                "name",
                "email",
                "street",
                "street2",
                "city",
                "state_id",
                "vat",
                "zip",
                "phone",
                "country_id",
            ]

            partners = odoo_server.Read(
                "res.partner", partner_ids, fields=fields_to_show
            )

            for partner in partners:
                partner_json = {
                    field: (
                        partner[field]
                        if field in partner and partner[field] is not False
                        else ""
                    )
                    for field in fields_to_show
                }
                partners_json.append(partner_json)

    print(Style.RESET_ALL)
    return JsonResponse(partners_json, safe=False)


@login_required
def delete_account_move(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:  # an admin is logging as that user
        # Obteniendo el ID del asiento contable que deseas eliminar a partir del nombre
        # ('state', '=', 'posted')
        move_name = request.GET.get("move_name")
        if not move_name:
            return HttpResponseNotFound(
                "Añade el parámetro ?move_name=XXX/XXX/XXX para borrar ese account move"
            )
        else:
            account_moves = odoo_server.Search(
                "account.move", domain=[("name", "=", move_name)], order="id asc"
            )
            if not account_moves:
                return HttpResponseNotFound(
                    f"No se ha encontrado el asiento contable <b>{move_name}</b>"
                )
            else:
                account_move_id = account_moves[0]  # Reemplaza con el ID correcto
                # Utiliza el método unlink para eliminar el registro del modelo account.move
                print("Account move Id found:", account_move_id)
                response = odoo_server.Write(
                    "account.move", [account_move_id], {"state": "cancel"}
                )  # Pasa a pagado
                print(f"Response cancel: {response}")
                response = odoo_server.Unlink("account.move", [account_move_id])
                print(f"Response unlink: {response}")
                if response:
                    return HttpResponse(
                        f"Éxito eliminando el asiento contable <b>{move_name}</b>.<br><img src='https://media.tenor.com/CnP64S7lszwAAAAi/meme-cat-cat-meme.gif'/> "
                    )
                else:
                    return HttpResponse(
                        f"ERROR eliminando el asiento contable <b>{move_name}</b>.<br>",
                        status=500,
                    )


@login_required
def correct_invoice(request, invoice_id):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:
        duplicated_invoice_id = odoo.invoices.duplicate_invoice(invoice_id)
        if not duplicated_invoice_id:
            messages.error(request, "Ha ocurrido un error al crear la factura rectificativa.")
            return redirect("get_invoice_view", invoice_id)
        
        odoo_server.Write("account.move", duplicated_invoice_id, {"type": "out_refund"})
        odoo_server.Method("account.move", "post", duplicated_invoice_id)
        url_duplicated = reverse(
            "get_invoice_view", kwargs={"invoice_id": duplicated_invoice_id}
        )
        message = mark_safe(
            f'Se ha creado correctamente una factura rectificativa para esta factura. Puedes verla <a href="{url_duplicated}">aquí</a>'
        )
        messages.success(request, message)
        return redirect("get_invoice_view", invoice_id)


@login_required
def duplicate_invoice(request, invoice_id):
    odoo_user_id = request.user.odooprofile.odoo_user_id

    if invoice_id not in odoo.invoices.get_invoices_ids_allowed(odoo_user_id) and not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    
    duplicated_invoice_id = odoo.invoices.duplicate_invoice(invoice_id)
    if not duplicated_invoice_id:
        messages.error(request, "Ha ocurrido un error duplicando la factura.")
        return redirect("get_invoice_view", invoice_id)

    odoo_server.Write("account.move", duplicated_invoice_id, {"state": "draft"})
    url_duplicated = reverse(
        "get_invoice_view", kwargs={"invoice_id": duplicated_invoice_id}
    )
    message = mark_safe(
        f'Se ha duplicado correctamente esta factura. Puedes verla <a href="{url_duplicated}">aquí</a>'
    )
    messages.success(request, message)
    return redirect("get_invoice_view", invoice_id)


@login_required
def download_invoice(request, invoice_id):
    odoo_user_id = request.user.odooprofile.odoo_user_id

    if invoice_id not in odoo.invoices.get_invoices_ids_allowed(odoo_user_id) and not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    
    response = odoo_server.Method('account.move', 'preview_invoice', [invoice_id])
    url = settings.ODOO_DOMAIN + response.get('url') + "&report_type=pdf&download=true"

    return redirect(url)


@login_required
def validate_invoice(request, invoice_id):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:
        odoo_server.Method("account.move", "post", invoice_id)
        invoice_details = odoo_server.Read(
            "account.move",
            [invoice_id],
            # ["name", "invoice_user_id", "state", "sequence_number"],
        )
        print("invoice_details: ", invoice_details)

        if invoice_details:
            # journal_id = invoice_details[0]["journal_id"][0]
            # TODO read and check if the journal_id[] sequence_number_next is not conflicting with a current existing invoice item.

            # Check if the state of the invoice is 'posted'
            if invoice_details[0]["state"] == "posted":
                details = {
                    "invoice_id": invoice_id,
                    "invoice_name": invoice_details[0]["name"],
                }
                invoice_user_id = invoice_details[0]["invoice_user_id"][0]
                user = User.objects.filter(
                    odooprofile__odoo_user_id=invoice_user_id
                ).first()
                if user:
                    details["email_to"] = user.email
                    odoo.utils.send_email_invoice_validated(details)
                OdooProfile.objects.filter(odoo_user_id=invoice_user_id)
                messages.success(
                    request, f"Factura validada y notificaciones enviadas."
                )
                return redirect("get_invoice_view", invoice_id)
            else:
                messages.error(
                    request,
                    "Error: La factura no se pudo validar. Quizás has de revisar y actualizar manualmente el numero siguiente de secuencia en el diario de ingresos del proyecto.",
                )
                return redirect("get_invoice_view", invoice_id)
        else:
            messages.error(request, "Error: No se encontraron detalles de la factura.")
            return redirect("get_invoice_view", invoice_id)


@login_required
def validate_expense_sheet(request, expense_sheet_id):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:  # an admin is logging as that user

        record_details = odoo_server.Read(
            "hr.expense.sheet",
            [expense_sheet_id],
            ["expense_line_ids", "name", "state"],
        )
        record_details = record_details[0]
        print("record_details", record_details)

        if record_details["state"] != "approve":
            return HttpResponse(
                "ERROR el estado de la hoja de gastos no está para aprobar."
            )

        line_ids = odoo_server.SearchRead(
            "hr.expense",
            domain=[("id", "in", record_details["expense_line_ids"])],
            fields=["unit_amount", "quantity"],
        )
        total_amount_before_tax = 0
        for line in line_ids:
            total_amount_before_tax += line["unit_amount"] * line["quantity"]

        hs_name = record_details["name"]
        project_code = hs_name.split("-")[2]  # hs_name is HG-YYY-XXX-NN, we want XXX
        # Search for the journal with the name starting with "GASTOS.XXX %"
        name_pattern = f"G.{project_code}"

        # Search for journals with names starting with the pattern
        journals = odoo_server.SearchRead(
            "account.journal",
            [("code", "like", name_pattern)],
            fields=["id", "code", "name", "sequence_number_next"],
        )
        if journals:
            journal = journals[0]
            print('journal', journal)
            sequence_number_next = journal["sequence_number_next"]
            # Nombre de la hoja de gastos, ej. #HG-2024-XXX-01
            expense_sheet_new_name = f"HG-{gnb.utils.CURRENT_YEAR}-{project_code}-{str(sequence_number_next).zfill(2)}"
        else:
            return HttpResponse(
                f"ERROR encontrando el journal id con nombre: {name_pattern}"
            )

        print("journal", journal)
        project_data = (
            Project.objects.filter(code=project_code).values("account_checking").first()
        )

        if not project_data:
            return HttpResponse(
                "ERROR encontrando el proyecto con el journal id de la hoja",
                status=400,
            )

        if record_details["state"] == "approve":
            print("Publicando asiento contable")
            try:
                odoo_server.Method(
                    "hr.expense.sheet",
                    "action_sheet_move_create",
                    expense_sheet_id
                )
            except Exception as e:
                # To check: Este error lo ignoramos por ahora
                # Hay que investigar de donde viene, el error es al recibir info de Odoo no al enviarla
                # Aunque la funcion action_sheet_move_create se ejecuta correctamente
                print(f"Error ignorado al publicar asiento: {e}")

        print('expense_sheet_new_name', expense_sheet_new_name)
        odoo_server.Write(
            "hr.expense.sheet",
            [expense_sheet_id],
            {"state": "done", "name": expense_sheet_new_name}
        )  # Pasa a pagado

        item_details = odoo_server.Read(
            "hr.expense.sheet", [expense_sheet_id], ["name", "employee_id"]
        )
        if item_details:
            details = {
                "expense_sheet_id": expense_sheet_id,
                "expense_sheet_name": item_details[0]["name"],
            }
            employee_id = item_details[0]["employee_id"][0]
            odoo_user_id = odoo.accounts.get_odoo_user_from_employee(
                employee_id
            )
            if odoo_user_id is None:
                return HttpResponseNotFound(
                    "Employee id or Odoo User id associated not found."
                )
            else:
                user = User.objects.filter(
                    odooprofile__odoo_user_id=odoo_user_id
                ).first()
                if user:
                    details["email_to"] = user.email
                    odoo.utils.send_email_expense_sheet_validated(details)

        messages.success(request, "Hoja de gastos aprobada con éxito.")
        return redirect(gnb.views.get_expense_sheet_view, expense_sheet_id)


@method_decorator(
    csrf_exempt, name="dispatch"
)  # Use this decorator if you want to disable CSRF protection (for testing purposes)
class InvoiceView(View):
    def post(self, request):
        try:
            data = json.loads(
                request.body.decode("utf-8")
            )  # Parse the JSON data from the request
        except json.JSONDecodeError as e:
            return JsonResponse({"error": "Invalid JSON data"}, status=400)
        user = request.user
        user_id = data.get("user_id", None)
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile
        odoo_user_id = odoo_profile.odoo_user_id

        cleaned_data = clean_odoo_form_data(request, data)
        journal = cleaned_data["journal"]
        cleaned_data["odoo_user_id"] = odoo_user_id
        client_vat = data["client_tax"]
        client_name = data["client_name"]
        client_country = data["client_country"]
        # Return a JSON response if needed
        invoice_id, message = create_invoice(cleaned_data)
        if invoice_id:
            details = {
                "username": request.user.username,
                "invoice_id": invoice_id,
                "client_name": client_name,
                "client_vat": client_vat,
                "client_country": client_country,
            }
            try:
                odoo.utils.send_email_new_invoice_created(details)
            except:
                redirect_url = reverse("new_invoice_view", kwargs={"project_code": journal})
                messages.error(request, "Factura creada pero debes comunicar a l@s admins.")
                return HttpResponse(
                    json.dumps({
                        'redirect_url': redirect_url
                    }), content_type="application/json"
                )
            messages.success(request, message)
            redirect_url = reverse("get_invoice_view", kwargs={"invoice_id": invoice_id})
        else:
            messages.error(request, message)
            redirect_url = reverse("new_invoice_view", kwargs={"project_code": journal})
            
        return HttpResponse(
            json.dumps({
                'redirect_url': redirect_url
            }), content_type="application/json"
        )


def clean_odoo_form_data(request, data):
    rows_dict = defaultdict(dict)
    rows = []
    new_data = {}
    for key, value in data.items():
        if "-" not in key:
            new_data[key] = value
        else:
            n = key.split("-")[1]
            if key.startswith("quantity"):
                rows_dict[n]["quantity"] = value
            elif key.startswith("price"):
                rows_dict[n]["price_unit"] = value
            elif key.startswith("taxes"):
                rows_dict[n]["tax_id"] = value
            elif key.startswith("description"):
                rows_dict[n]["description"] = value
            elif key.startswith("product"):
                rows_dict[n]["product_id"] = value
            elif key.startswith("date_line"):
                rows_dict[n]["date"] = value
            elif key.startswith("reference"):
                rows_dict[n]["reference"] = value

    if request.FILES:
        for key, uploaded_file in request.FILES.items():
            if "-" in key:
                n = key.split("-")[1]
                rows_dict[n]["uploaded_file"] = uploaded_file

    sorted_keys = sorted(rows_dict.keys())
    rows = [rows_dict[key] for key in sorted_keys]

    new_data["lines"] = rows
    if not "date" in data:
        new_data["date"] = datetime.datetime.now().date().strftime("%Y-%m-%d")

    return new_data


@login_required
@csrf_exempt
def edit_invoice_line(request):
    if request.method == "POST":

        invoice_id = int(request.POST.get("invoice_id"))
        line_id = int(request.POST.get("line_id"))
        quantity = float(request.POST.get("quantity"))
        price_unit = float(request.POST.get("price_unit"))
        tax_id = int(request.POST.get("tax_id"))
        description = request.POST.get("description")

        print(quantity)
        print(line_id)

        odoo_server.Write("account.move", invoice_id, {
            "invoice_line_ids": [(1, line_id, {
            "tax_ids" : [(6, 0, [tax_id])],
            "price_unit": price_unit,
            "quantity": quantity,
            "name": description
            })]
        })
        
        return JsonResponse({"message": "Factura editada correctamente"})

    return JsonResponse({"error": "Método no permitido"}, status=405)


@login_required
@csrf_exempt
def edit_invoice_contact(request):
    if request.method == "POST":
        client_id = int(request.POST.get("client_id"))

        odoo_server.Write("res.partner", client_id, {
            "name": request.POST.get("name"),
            "street": request.POST.get("street"),
            "city": request.POST.get("city"),
            "zip": int(request.POST.get("zip")),
            "vat": request.POST.get("vat")
        })
        return JsonResponse({"message": "Ciente editado correctamente"})

    return JsonResponse({"message": "Método no permitido"}, status=405)
 

def get_or_update_client(data, country, state):
    """Handle client creation or update in Odoo"""
    try:
        id_tax_number = data["client_tax"]
        clients = odoo_server.SearchRead(
            "res.partner",
            [("vat", "=", id_tax_number)],
            ["vat", "name", "street", "city", "zip", "state_id", "country_id", "company_type"],
        )

        # Obtener el estado correcto basado en el código postal
        zip_code = data["client_zip"]
        if data["client_country"] == "ES":
            # Buscar primero el zip_id
            zip_ids = odoo_server.Search(
                "res.city.zip",
                [("name", "=", zip_code)]
            )
            if zip_ids:
                # Obtener la ciudad asociada al código postal
                zip_data = odoo_server.Read("res.city.zip", [zip_ids[0]], ["city_id"])
                if zip_data and zip_data[0].get("city_id"):
                    # Obtener el estado asociado a la ciudad
                    city_data = odoo_server.Read("res.city", [zip_data[0]["city_id"][0]], ["state_id"])
                    if city_data and city_data[0].get("state_id"):
                        state = city_data[0]["state_id"][0]

        new_partner_data = {
            "vat": id_tax_number,
            "name": data["client_name"],
            "street": data["client_street"],
            "city": data["client_city"],
            "zip": zip_code,
            "country_id": country[0]["id"],
            "state_id": state,            
            "company_type": "company",
        }

        if len(clients):
            return update_existing_client(clients[0], new_partner_data)
        else:
            return create_new_client(new_partner_data)
            
    except Exception as e:
        error_msg = f"Error procesando cliente {data.get('client_name', '')}: {str(e)}"
        _logger.error(error_msg)
        return None, error_msg

def update_existing_client(client_data, new_partner_data):
    """Update existing client if data has changed"""
    try:
        client_id = client_data["id"]
        changes = {}
        
        # Primero procesamos el zip_id y state_id si existen
        location_fields = ["zip_id", "state_id", "country_id"]
        location_changes = {}
        
        for field in location_fields:
            if field in new_partner_data:
                current_value = client_data.get(field)
                new_value = new_partner_data[field]
                
                if not current_value and not new_value:
                    continue
                if not current_value and new_value:
                    location_changes[field] = {"old": current_value, "new": new_value}
                elif current_value and current_value[0] != new_value:
                    location_changes[field] = {"old": current_value, "new": new_value}
        
        # Luego procesamos el resto de campos
        for field, new_value in new_partner_data.items():
            if field not in location_fields:
                if client_data.get(field) != new_value:
                    changes[field] = {"old": client_data.get(field), "new": new_value}

        # Si hay cambios, los aplicamos en el orden correcto
        if location_changes or changes:
            # Primero actualizamos la ubicación si hay cambios
            if location_changes:
                odoo_server.Write(
                    "res.partner",
                    [client_id],
                    {field: values["new"] for field, values in location_changes.items()},
                )
            
            # Luego actualizamos el resto de campos
            if changes:
                odoo_server.Write(
                    "res.partner",
                    [client_id],
                    {field: values["new"] for field, values in changes.items()},
                )

            # Preparamos el contenido del email
            email_content = "Los siguientes cambios han cambiado en ODOO para su verificación:\n"
            
            for field, values in {**location_changes, **changes}.items():
                email_content += f"{field}: {values['old']} -> {values['new']}\n"

            details = {
                "client_id": client_id,
                "vat": new_partner_data["vat"],
                "name": new_partner_data["name"],
            }
            odoo.utils.send_email_client_updated(details, email_content)

        return client_id, None
            
    except Exception as e:
        error_msg = f"Error actualizando cliente {client_data.get('name', '')}: {str(e)}"
        _logger.error(error_msg)
        return None, error_msg

def create_new_client(new_partner_data):
    """Create new client in Odoo"""
    client_id = odoo_server.Create("res.partner", new_partner_data)
    if not client_id:
        return None, "Error creando el nuevo contacto para la factura. Alguno de los datos debe ser erróneo."
    
    new_partner_data["client_id"] = client_id
    odoo.utils.send_email_new_client_created(new_partner_data)
    return client_id, None

def create_invoice_lines(data):
    """Create invoice lines including notes"""
    invoice_line_ids = []
    
    for line in data["lines"]:
        product_id = line["product_id"]
        product = odoo_server.SearchRead(
            "product.product",
            domain=[("id", "ilike", product_id)],
            order="id asc",
            fields=["property_account_income_id"],
        )
        account_id = product[0]["property_account_income_id"][0] if product[0]["property_account_income_id"] else False
        
        if not account_id:
            return None, "ODOO_ACCOUNT_ID_NOT_FOUND_FOR_PRODUCT"

        invoice_line_ids.append((
            0,
            0,
            {
                "name": line["description"],
                "account_id": account_id,
                "product_id": line["product_id"],
                "quantity": line["quantity"],
                "tax_ids": [(6, 0, [int(line["tax_id"])])],
                "price_unit": line["price_unit"],
            },
        ))

    if "note" in data and data["note"]:
        invoice_line_ids.append((
            0,
            0,
            {
                "display_type": "line_note",
                "name": data["note"],
                "product_id": None,
                "account_id": None,
                "tax_ids": None,
                "quantity": 1,
                "price_unit": 0,
            },
        ))

    return invoice_line_ids, None

def create_invoice(data):
    """Main function to create invoice in Odoo"""
    # Validate journal
    journal_ids = odoo_server.Search("account.journal", [("code", "=", data["journal"]), ("type", "=", "sale")])
    if not journal_ids:
        return None, "ODOO_NOT_JOURNAL_ID_FOUND_ERROR"
    journal_id = journal_ids[0]

    # Get country and state data
    country = odoo_server.SearchRead(
        "res.country", [("code", "=", data["client_country"])], ["id", "code"],
    )

    state = None

    # Handle client
    client_id, error = get_or_update_client(data, country, state)
    if error:
        return None, error

    # Create invoice lines
    invoice_line_ids, error = create_invoice_lines(data)
    if error:
        return None, error

    # Create the invoice
    move_vals = {
        "invoice_user_id": int(data["odoo_user_id"]),
        "type": "out_invoice",
        "partner_id": client_id,
        "invoice_date": data["date"],
        "journal_id": journal_id,
        "invoice_line_ids": invoice_line_ids,
        "ref": data["referencia"],
        "state": "draft",
        "invoice_payment_term_id": [(6, 0, 1)],
    }

    invoice_id = odoo_server.Create("account.move", move_vals)
    if not invoice_id:
        return None, "Hubo un error al crear la factura, por favor, revisa los datos introducidos. Si te vuelve a ocurrir contacta con un administrador."

    return invoice_id, None


def add_odoo_atachment_to_resource(uploaded_file, resource_id):
    # Use the Odoo API to create an attachment
    binary_data = base64.b64encode(uploaded_file.read())
    # Convert binary data to ASCII string using 'latin1' encoding
    utf_data = binary_data.decode("utf-8")
    attachment_data = {
        "name": uploaded_file.name,  # Use the uploaded file's name
        "res_name": "Expense Description",  # Set a name for the related record (expense description)
        "res_model": "hr.expense",  # Set the model to which the attachment is related
        "res_id": resource_id,  # Set the ID of the related record
        "type": "binary",
        "datas": utf_data,  # Read the file data
    }

    # Create the attachment in Odoo
    attachments = odoo_server.Create("ir.attachment", [attachment_data])
    print("attachments created:", attachments)


# Use this decorator if you want to disable CSRF protection (for testing purposes)
@method_decorator(csrf_exempt, name="dispatch")
class ExpenseView(View):
    def post(self, request):
        try:
            data = request.POST  # Parse the JSON data from the request
        except:
            return JsonResponse({"error": "Error reading post data"}, status=400)

        user_id = data.get("user_id", None)
        user = request.user
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile
        odoo_employee_id = odoo_profile.get_employee_id()
        iban_account = data.get("iban_account", "")
        iban_receiver = data.get("iban_receiver", "")
        iban_concept = data.get("iban_concept", "")

        cleaned_data = clean_odoo_form_data(request, data)
        cleaned_data["odoo_employee_id"] = odoo_employee_id
        # Return a JSON response if needed
        hr_expense_sheet_id, message = odoo.expenses.create_odoo_expense(cleaned_data)
        if hr_expense_sheet_id:
            expense_details, _ = odoo.expenses.get_expense_sheet(hr_expense_sheet_id)
            details = {
                "username": request.user.username,
                "onbehalf_username": user.username,
                "expense_details": expense_details,
                "hr_expense_sheet_id": hr_expense_sheet_id,
                "iban_account": iban_account.upper(),
                "iban_receiver": iban_receiver,
                "iban_concept": iban_concept,
            }

            odoo.utils.send_email_new_expense_created(details)
            print("cleaned_data", cleaned_data)
            # Para crearse el account move de gasto, ha de salir dinero hacia fuera.
            if cleaned_data["type_expense"] == "receipt_expense" and iban_account:
                project_code = cleaned_data["journal"]
                descriptions = ", ".join(
                    line["description"] for line in cleaned_data["lines"]
                )
                expense_details["reference"] = descriptions
                object_id, message = odoo.expenses.create_expense_account_move(
                    expense_details, cleaned_data["date"], project_code
                )

                if object_id:
                    account_move_id = object_id[0]
                    details = {
                        "username": request.user.username,
                        "onbehalf_username": user.username,
                        "object_id": account_move_id,
                        "from_account": project_code,
                        "to_account": "Proveedores",
                        "amount": expense_details["total_amount"],
                        "untaxed_amount": expense_details["untaxed_amount"],
                        "reference": expense_details["name"],
                        "expense_details": expense_details,
                        "iban_account": iban_account.upper(),
                        "iban_receiver": iban_receiver,
                        "iban_concept": iban_concept,
                    }

                    # Create the external pending payment object
                    payment = Payment(
                        payment_type=PaymentType.DEBIT,
                        payment_scope=PaymentScope.EXTERNAL,
                        iban_account=iban_account.upper(),
                        to_account=iban_receiver,
                        from_account=project_code,
                        currency="€",
                        due_date=timezone.now(),  # Set due date to current datetime
                        amount=expense_details["total_amount"],
                        paid=False,
                        reference=iban_concept,
                        status=PaymentStatus.PENDING,
                        account_move_id=account_move_id,
                    )

                    payment.save()

                    odoo.utils.send_email_new_account_move(details)
        # Total va a debito va cuenta a proveedores
        # el subtotal sin impuestos va a la cuenta del proyecto
        # los impuestos a la cuenta general

        return JsonResponse({"hr_expense_id": hr_expense_sheet_id, "message": message})


@method_decorator(login_required, name="dispatch")
@method_decorator(csrf_exempt, name="dispatch")
class CashAccountMoveView(View):
    def post(self, request):
        data = request.POST  # Parse the JSON data from the request

        cash_account_id = data.get("cash_account_id", None)
        print("Searching cash_account_id", cash_account_id)
        if not cash_account_id:
            return JsonResponse(
                {"error": "CashAccount non existent."},
                status=400,
            )
        user = request.user
        cash_account = CashAccount.objects.get(id=cash_account_id)
        is_admin = user in cash_account.user_admins.all()

        if not is_admin and not request.user.is_superuser:
            # if non admin user chooses a different user id in the params
            return JsonResponse(
                {"error": "Access denied, reported to admins."},
                status=403,
            )

        tx_amount = data.get("tx_amount", None)
        tx_direction = data.get("tx_direction", None)  # can be in our out
        tx_reference = data.get("tx_reference", None)
        if tx_direction == "in":
            account_debit = cash_account.account_number
            account_credit = settings.ODOO_ACCOUNT_CLIENTS
        else:
            account_debit = settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
            account_credit = cash_account.account_number

        tx_amount = float(tx_amount)
        if (
            None in [tx_amount, tx_reference, account_debit, account_credit]
            or tx_amount <= 0
        ):
            return JsonResponse(
                {"error": "Error with transaccion parameters."}, status=400
            )

        object_id, message = odoo.accounts.create_bank_account_move(
            account_debit, account_credit, tx_amount, tx_reference
        )

        if object_id:
            details = {
                "username": request.user.username,
                "object_id": object_id[0],
                "cash_account": cash_account,
                "amount": tx_amount,
                "project": cash_account.project,
                "reference": tx_reference,
                "tx_direction": tx_direction,
            }

            odoo.utils.send_email_new_cash_account_move(details)

        return JsonResponse({"object_id": object_id, "message": message})


@method_decorator(login_required, name="dispatch")
@method_decorator(csrf_exempt, name="dispatch")
class BankAccountMoveView(View):
    def post(self, request):
        try:
            data = request.POST  # Parse the JSON data from the request
        except Exception as e:
            return JsonResponse(
                {"object_id": None, "error": "Error reading post data"}, status=400
            )

        print("POST data", data)
        payment = None
        account_credit = None
        account_debit = None
        user_id = data.get("user_id", None)
        user = request.user
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile

        from_account = data.get("from_account", None)
        if from_account:
            if from_account == "personal_account":
                from_account = user.email
                account_credit = odoo_profile.account_checking
            else:
                account_credit = odoo.accounts.get_odoo_account_code(from_account)

        to_account_internal = data.get("to_account_internal", None)
        if to_account_internal:
            to_account = to_account_internal
            if to_account_internal == "personal_account":
                to_account = user.email
                account_debit = odoo_profile.account_checking
            else:
                account_debit = odoo.accounts.get_odoo_account_code(to_account_internal)

        avoid_notify = (
            True
            if "avoid_notify" in request.POST
            and request.POST.get("avoid_notify") == "on"
            else False
        )
        amount = data.get("transfer_amount", None)
        amount = float(amount)
        fee = 0
        reference = data.get("transfer_ref", None)

        destination_type = data.get("destination_type", None)

        if destination_type == "external_account":
            iban_account = data.get("iban_account", None)
            if iban_account:
                to_account_beneficiary = data.get("to_account_beneficiary", None)
                account_debit = settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
                to_account = iban_account
                avoid_notify = True  # if external transfer, we have nobody to notify

                # Create the payment object
                payment = Payment(
                    payment_type=PaymentType.DEBIT,
                    payment_scope=PaymentScope.EXTERNAL,
                    iban_account=iban_account,
                    to_account=to_account_beneficiary,
                    from_account=from_account,
                    currency="€",
                    due_date=timezone.now(),  # Set due date to current datetime
                    amount=amount,
                    paid=False,
                    reference=reference,
                    status=PaymentStatus.PENDING,
                )
                payment.save()

        elif destination_type == "contributions":
            # contributions go to GNB's main project account
            to_account = "contributions"
            account_debit = settings.ODOO_ACCOUNT_COLLECTIVE_CONTRIBUTION
            avoid_notify = True # if contributions, no receiver is notified

        elif destination_type == "atm_withdrawal":
            account_debit = settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
            avoid_notify = True  # if external transfer, no receiver is notified
            reference = f"Retirada cajero ({amount}€)"
            to_account = "atm_withdrawal"
            fee = 2

            payment = Payment(
                payment_type=PaymentType.DEBIT,
                payment_scope=PaymentScope.ATM_WITHDRAW,
                to_account=to_account,
                from_account=from_account,
                currency="€",
                due_date=timezone.now(),  # Set due date to current datetime
                amount=amount,
                paid=False,
                status=PaymentStatus.PENDING,
            )
            payment.save()

        print(
            "[amount, reference, account_debit, account_credit]",
            [amount, reference, account_debit, account_credit],
        )
        if None in [amount, reference, account_debit, account_credit] or amount <= 0:
            return JsonResponse(
                {"error": "Incorrect transaction parameters. Reported to admins."},
                status=400,
            )
        amount = amount + fee
        object_id, message = odoo.accounts.create_bank_account_move(
            account_debit, account_credit, amount, reference
        )

        if object_id:
            account_move_id = object_id[0]
            if payment:
                payment.account_move_id = account_move_id
                payment.save()

            details = {
                "username": request.user.username,
                "onbehalf_username": user.username,
                "object_id": account_move_id,
                "from_account": from_account,
                "to_account": to_account,
                "amount": amount,
                "reference": reference,
            }

            odoo.utils.send_email_new_account_move(details)
            if not avoid_notify:
                odoo.utils.send_email_new_account_move_to_recipients(details)

        return JsonResponse({"object_id": account_move_id, "message": message})
