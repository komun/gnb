from django.core.mail import send_mail
from django.conf import settings
from gnb.models import Project, ProjectMembership, PaymentScope
from django.urls import reverse


def send_email_new_cashin(details):
    print("details", details)
    invoice_name = details["invoice_name"]
    subject = f"GNB: Nuevo ingreso en tu proyecto con factura {invoice_name}"
    project_code = details["project_code"]
    gnb_url = f"{settings.DEFAULT_DOMAIN}/gnb/projects/{project_code}"

    p = Project.objects.get(code=project_code)
    # only those that are admins in that project will receive the email
    destination_emails = ProjectMembership.objects.filter(
        project=p, role="admin"
    ).values_list("user__email", flat=True)
    message = f"""Tu proyecto [{p.code}: {p.name}] ha recibido un ingreso.
Cantidad después de impuestos: {details['amount_untaxed']}€
Factura: {invoice_name}

Deberás crear un gasto para justificar la entrada y poder mover el dinero.: {gnb_url}"""
    result = send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, destination_emails
    )
    print(f"Sent mail to {destination_emails} with result: {result}")

    subject = f"GNBAdmin: Nuevo cashin añadido: {invoice_name}"

    message = f"""Via admin: {details["action_username"]}
Cantidad añadida: {details["amount_total"]}
Neto sin impuestos de la factura: {details["amount_untaxed"]}
Impuestos pagados? {details["pay_taxes"]}
Factura: {invoice_name}
Diario: [{p.code}: {p.name}]
"""

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def get_destination_emails(to_account):
    if "@" not in to_account:  # it is a project
        p = Project.objects.get(code=to_account)
        # only those that are admins in that project will receive the email
        destination_emails = list(
            ProjectMembership.objects.filter(project=p, role="admin").values_list(
                "user__email", flat=True
            )
        )
    else:
        destination_emails = [to_account]
    return destination_emails


def send_email_about_tx_details_to_recipients(details):
    from_account = details["from_account"]
    destination_emails = get_destination_emails(from_account)
    print("destination_emails", destination_emails)
    if details["payment"]["scope"] == PaymentScope.ATM_WITHDRAW:
        word = "Retirada en cajero"
    elif details["payment"]["scope"] == PaymentScope.EXTERNAL:
        word = "Transferencia SEPA"
    if details["action"] == "accept":
        subject = f"GNB: {word} ejecutada"
    else:
        subject = f"GNB: {word} rechazada"
    message = f"""{details["email_text"]}
    ----
    Detalles de la {word}:
        {details["payment"]["to_account"]}
        {details["payment"]["amount"]}
        {details["payment"]["reference"]}
    """
    result = send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, destination_emails
    )
    print(f"Sent mail to {destination_emails} with result: {result}")


def send_email_new_account_move_to_recipients(details):
    subject = "GNB: Transacción recibida"
    gnb_url = f"{settings.DEFAULT_DOMAIN}/gnb/dashboard/"
    to_account = details["to_account"]
    destination_emails = get_destination_emails(to_account)
    print("destination_emails", destination_emails)
    message = f"""
Cuenta origen: {details['from_account']}
Cuenta destino: {to_account}
Cantidad: {details['amount']} €
Referencia: {details['reference']}
Puedes revisar el saldo en: {gnb_url}"""
    result = send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, destination_emails
    )
    print(f"Sent mail to {destination_emails} with result: {result}")


def send_email_new_user_created(details):
    email = details["user"].email
    fullname = details["user"].first_name + " " + details["user"].last_name
    action_word = (
        "creado" if details["update_details"]["django_created"] else "actualizado"
    )
    subject = f"GNBAdmin: Nuevo usuario {email} {action_word}."

    message = f"""Via admin: {details['action_username']}
Nombre: {fullname}
Email: {email}"""

    message += str(details["update_details"])
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def send_email_new_group_created(details):
    project_code = details["project"].code
    action_word = "creado" if details["update_details"]["created"] else "actualizado"
    subject = f"GNBAdmin: Nuevo proyecto {project_code} {action_word}"
    message = f"""Via admin: {details['action_username']},

Código: {project_code}
Nombre: {details["project"].name}
"""
    message += str(details["update_details"])

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def send_email_new_cash_account_move(details):
    cash_account = details["cash_account"]
    project = details["project"]
    subject = f"GNBAdmin: Movimiento en caja '{cash_account.name}' en proyecto: {project.code}"

    username = details["username"]
    gnb_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
        "project_view", args=[project.code]
    )
    # TODO: get also project admins
    destination_emails = settings.ADMINS_EMAIL
    user_msg = f"""Usuario/a: {username}"""
    message = f"""{user_msg}
Caja: {cash_account.name}
Tipo: {details['tx_direction']}
Cantidad: {details['amount']} €
Referencia: {details['reference']}
Proyecto: {project.code}
Puedes revisar la transacción en: {gnb_url}"""
    result = send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, destination_emails
    )
    print(f"Sent mail to {destination_emails} with result: {result}")


def send_email_new_account_move(details):
    if "expense_details" in details:
        subject = (
            f"GNBAdmin: Nueva transacción externa del gasto {details['reference']}"
        )
    else:
        subject = "GNBAdmin: Nueva transacción"
    username = details["username"]
    onbehalf_username = details["onbehalf_username"]
    object_id = details["object_id"]
    destination_emails = settings.ADMINS_EMAIL
    odoo_url = f"{settings.ODOO_DOMAIN}/web?debug=1#id={object_id}&model=account.move&view_type=form"
    if username != onbehalf_username:
        user_msg = f"""Admin: {username} en nombre de {onbehalf_username}"""
    else:
        user_msg = f"""Usuario/a: {username}"""

    iban_details = ""
    if details["to_account"] == "tx_details":
        subject = "GNBAdmin: Nueva transacción externa"

    if "iban_account" in details and details["iban_account"]:
        subject = "GNBAdmin: Nueva transacción externa"
        iban_details = f"""
IBAN: {details["iban_account"]}
Beneficiario: {details["iban_receiver"]}
Concepto: {details["iban_concept"]}
"""

    if "untaxed_amount" in details:
        untaxed_amount = f'Cantidad sin tasas: {details["untaxed_amount"]} €'
    else:
        untaxed_amount = ""

    message = f"""{user_msg}
Cuenta origen: {details['from_account']}
Cuenta destino: {details['to_account']}
{iban_details}
Cantidad: {details['amount']} €
{untaxed_amount}
Referencia: {details['reference']}
Puedes revisar la transacción en: {odoo_url}"""
    result = send_mail(
        subject, message, settings.DEFAULT_FROM_EMAIL, destination_emails
    )
    print(f"Sent mail to {destination_emails} with result: {result}")


def send_email_invoice_validated(details):
    invoice_name = details["invoice_name"]
    invoice_id = details["invoice_id"]
    email_to = details["email_to"]

    subject = f"GNB: Tu factura {invoice_name} ha sido validada."
    gnb_url = f"{settings.DEFAULT_DOMAIN}/gnb/invoices/{invoice_id}/"
    message = f"""Factura: {invoice_name}
Puedes visualizar e imprimir la factura desde: {gnb_url}"""
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email_to])


def send_email_new_invoice_created(details):
    subject = "GNBAdmin: Nueva factura borrador en Odoo creada."
    invoice_id = details["invoice_id"]
    odoo_url = f"{settings.ODOO_DOMAIN}/web?debug=1#id={invoice_id}&action=216&model=account.move&view_type=form"
    gnb_url = f"{settings.DEFAULT_DOMAIN}/gnb/invoices/{invoice_id}/"
    gnb_validate_url = f"{settings.DEFAULT_DOMAIN}/odoo/invoice/validate/{invoice_id}/"
    message = f"""Usuario/a: {details['username']}
Cliente: {details['client_name']}
CIF: {details['client_vat']}

Enlace GNB de la factura: {gnb_url}
Enlace Odoo: {odoo_url}

Enlace GNB para validar la factura: {gnb_validate_url}
"""
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


# Function to send email for updated client
def send_email_client_updated(details, content):
    client_name = details["name"]
    client_vat = details["vat"]
    subject = f"GNBAdmin: Datos del cliente {client_name}({client_vat}) han sido modificados durante una factura"
    client_id = details["client_id"]
    odoo_url = f"{settings.ODOO_DOMAIN}/web?debug=1#id={client_id}&model=res.partner&view_type=form"
    body = f"{content}\n\nPuedes revisar el nuevo partner en: {odoo_url}"
    send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def send_email_new_client_created(details):
    client_name = details["name"]
    client_vat = details["vat"]
    subject = f"GNBAdmin: Nuevo cliente {client_name}({client_vat})"
    client_id = details["client_id"]
    odoo_url = f"{settings.ODOO_DOMAIN}/web?debug=1#id={client_id}&model=res.partner&view_type=form"

    # Codifica el mensaje a UTF-8
    message = (
        f"Al crear nueva factura el/la usuario/a ha creado un nuevo cliente en la bbdd de Odoo:"
        f"\nCIF: {client_vat}"
        f"\nNombre: {client_name}"
        f"\nPuedes revisar el nuevo cliente en: {odoo_url}"
    )

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def send_email_expense_sheet_validated(details):
    expense_sheet_name = details["expense_sheet_name"]
    expense_sheet_id = details["expense_sheet_id"]
    email_to = details["email_to"]

    subject = f"GNB: Tu hoja de gastos {expense_sheet_name} ha sido validada."
    gnb_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
        "get_expense_sheet_view", args=[expense_sheet_id]
    )
    message = f"""Hoja de gastos: {expense_sheet_name}
Puedes visualizar e imprimir la hoja de gastos desde: {gnb_url}"""
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email_to])


def send_email_new_expense_created(details):
    username = details["username"]
    onbehalf_username = details["onbehalf_username"]
    subject = f"GNBAdmin: Nueva hoja de gastos de {onbehalf_username}"
    expense_sheet_id = details["hr_expense_sheet_id"]
    expense_details = details["expense_details"]
    odoo_url = f"{settings.ODOO_DOMAIN}/web?debug=1#id={expense_sheet_id}&action=292&model=hr.expense.sheet&view_type=form"
    gnb_validate_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
        "odoo:validate_expense", args=[expense_sheet_id]
    )
    gnb_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
        "get_expense_sheet_view", args=[expense_sheet_id]
    )
    iban_details = ""
    if details["iban_account"]:
        iban_details = f"""
IBAN: {details["iban_account"]}
Beneficiario: {details["iban_receiver"]}
Concepto: {details["iban_concept"]}
"""

    if username != onbehalf_username:
        user_msg = f"""Admin: {username} en nombre de {onbehalf_username}"""
    else:
        user_msg = f"""Usuario/a: {username}"""
    message = f"""{user_msg} ha creado hoja de gasto {expense_details['name']}
En Odoo: {odoo_url}.
En GNB: {gnb_url} 
---------
Nombre: {expense_details["name"]}
Cantidad: {expense_details["total_amount"]} €
Fecha Hoja de Gasto: {expense_details["accounting_date"]}
{iban_details}
---------
Puedes validarla como superadmin haciendo click en: {gnb_validate_url}
"""
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)


def send_email_new_message_created(details):
    username = details["username"]
    res_id = details["res_id"]

    if details["odoo_model"] == "account.move":
        subject = f"GNBAdmin: Nuevo comentario en factura {res_id}"
        gnb_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
            "get_invoice_view", args=[res_id]
        )
    elif details["odoo_model"] == "hr.expense.sheet":
        subject = f"GNBAdmin: Nuevo comentario en hoja de gastos {res_id}"
        gnb_url = f"{settings.DEFAULT_DOMAIN}" + reverse(
            "get_expense_sheet_view", args=[res_id]
        )

    message = f"""{username} ha añadido el siguiente comentario:
{details["message"]}

En GNB: {gnb_url} 
"""
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL)
