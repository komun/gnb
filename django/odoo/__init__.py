from django.conf import settings
from odooclient import client

odoo_server = None

print("Connecting to Odoo Server")
odoo_server = client.OdooClient(
    host=settings.ODOO_HOST,
    port=settings.ODOO_PORT,
    dbname=settings.ODOO_DBNAME,
    saas=settings.ODOO_SAAS,
    debug=getattr(settings, "DEBUG_ODOO", False),
)
print("odoo_server", odoo_server)
# Perform initial connection check and authentication if needed
if not odoo_server.IsAuthenticated():
    ok = odoo_server.Authenticate(settings.ODOO_USERNAME, settings.ODOO_PASSWORD)
    if ok:
        print("SUCCESSFULLY Authenticated to Odoo Server")
    else:
        print("FAILED Authenticating to Odoo Server")
