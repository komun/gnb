from . import odoo_server
import re


def get_messages(res_id, model):
    comments = odoo_server.SearchRead(
        "mail.message",
        domain = [
                    ("res_id", "=", res_id),
                    ("model", "=", model),
                    ("message_type", "=", "comment"),],
        order="date asc",
        fields=["date", "body", "message_type", "author_id"],
    )

    if comments:
        for comment in comments:
            remove_html_content_patron = re.compile('<.*?>')
            clean_comment = re.sub(remove_html_content_patron, '', comment["body"])
            comment["body"] = clean_comment
            comment["author"] = comment["author_id"][1] # Get author name

    return comments


def create_message(body, res_id, model, author_id):
    message_data = {
        "model": model,
        "res_id": res_id,
        "message_type": "comment",
        "body": body,
        "author_id": author_id
    }

    message_id = odoo_server.Create("mail.message", [message_data])
    return message_id

