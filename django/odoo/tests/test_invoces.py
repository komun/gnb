from django.test import SimpleTestCase
from odoo.invoices import *


class GetInvoiceByName(SimpleTestCase):

    # def test_get_invoice_by_name(self):
    #     invoice = get_invoice_by_name("FACTURA/2024/0001")
    #     invoices = odoo_server.SearchRead("account.move", domain=[("name", "ilike", "")], order="id asc")
    #     print("FACTURAS:")
    #     for i in invoices:
    #         print(i["name"])

    #     self.assertEqual(invoice["name"], "FACTURA/2024/0001")
    #     self.assertEqual(invoice["amount_total"], 2.42)

    def test_get_invoice_by_name_invoice_not_found(self):
        invoice = get_invoice_by_name("FACTURA/0000/0001")
        self.assertIsNone(invoice)
