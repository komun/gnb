# from django.contrib import admin
# from gnb import views
from baton.autodiscover import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
import notifications.urls
import gnb.views

urlpatterns = [
    path("", gnb.views.frontend_view, name="index"),
    path("gnb/dashboard/", gnb.views.my_dashboard_view, name="my_dashboard_view"),
    path(
        "gnb/accounts/<str:account_username>/", gnb.views.account_view, name="account_view"
    ),
    path("gnb/contact/", gnb.views.contact_view, name="contact_view"),
    path("gnb/expenses/", gnb.views.get_expenses_view, name="get_expenses_view"),
    path(
        "gnb/expenses/<int:expense_sheet_id>/",
        gnb.views.get_expense_sheet_view,
        name="get_expense_sheet_view",
    ),
    path(
        "gnb/transactions/<int:tx_id>/",
        gnb.views.get_transaction_view,
        name="get_transaction_view",
    ),
    path("gnb/send_new_comment/", gnb.views.send_new_comment, name="send_new_comment"),
    path("gnb/invoices/", gnb.views.get_invoices_view, name="get_invoices_view"),
    path(
        "gnb/invoices/<int:invoice_id>/",
        gnb.views.get_invoice_view,
        name="get_invoice_view",
    ),
    path(
        "gnb/invoices/<int:invoice_id>/edit/",
        gnb.views.get_invoice_view, {'edit': True},
        name="get_invoice_view_edit",
    ),
    path(
        "gnb/invoices/<int:invoice_id>/export/",
        gnb.views.download_invoice_facturae,
        name="download_invoice_facturae",
    ),
    path(
        "gnb/invoices/send_facturae_request/<int:invoice_id>/",
        gnb.views.send_facturae_request,
        name="send_facturae_request",
    ),
    path(
        "gnb/invoices/send_unpaid_notice/",
        gnb.views.send_invoice_unpaid_notice,
        name="send_invoice_unpaid_notice",
    ),
    path("gnb/reports/balance/", gnb.views.reports_balance_view, name="reports_balance_view"),
    path("gnb/report/users/", gnb.views.users_report_view, name="users_report_view"),
    path(
        "gnb/report/projects/",
        gnb.views.projects_report_view,
        name="projects_report_view",
    ),
    path("gnb/new_cashin/", gnb.views.new_cashin_view, name="new_cashin_view"),
    path("gnb/new_project/", gnb.views.new_project_view, name="new_project_view"),
    path(
        "gnb/projects/<str:project_code>/", gnb.views.project_view, name="project_view"
    ),
    path(
        "gnb/projects/<str:project_code>/new_invoice/",
        gnb.views.new_invoice_view,
        name="new_invoice_view",
    ),
    path(
        "gnb/projects/<str:project_code>/new_expense/",
        gnb.views.new_expense_view,
        name="new_expense_view",
    ),
    path("gnb/new_user/", gnb.views.new_user_view, name="new_user_view"),
    path(
        "gnb/payments/pending/",
        gnb.views.pending_payments,
        name="pending_payments_view",
    ),
    path(
        "gnb/payments/export/",
        gnb.views.export_payments,
        name="export_payments_view",
    ),
    path(
        "gnb/payments/mark_as_paid/",
        gnb.views.mark_as_paid_payments,
        name="mark_as_paid_payments_view",
    ),
    path(
        "gnb/payments/notify_about_tx_details/",
        gnb.views.notify_about_tx_details,
        name="notify_about_tx_details_view",
    ),

    path(
        "gnb/validate_iban/<str:iban_code>/",
        gnb.views.IBANValidatorView.as_view(),
        name="validate_iban",
    ),
    path("admin/", admin.site.urls),
    path("accounts/", include("django_registration.backends.one_step.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("baton/", include("baton.urls")),
    path("odoo/", include("odoo.urls")),
    path(
        "inbox/notifications/", include(notifications.urls, namespace="notifications")
    ),
    path("__debug__/", include("debug_toolbar.urls")),
]
# urlpatterns += static("api/media/", document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import os.path

    # settings.STATICFILES_DIRS += os.path.join(settings.BASE_DIR, "frontend","static"),
    # print('settings.STATICFILES_DIRS', settings.STATICFILES_DIRS)
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    FRONTEND_URL = "/static/"
    FRONTEND_ROOT_STATIC = os.path.join(settings.BASE_DIR, "gnb", "static")
    print("FRONTEND_ROOT_STATIC", FRONTEND_ROOT_STATIC)
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT_STATIC)
