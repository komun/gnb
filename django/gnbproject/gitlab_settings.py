from .settings import *

DEBUG = True
LOCAL = False
DEV = True
ADMINS_EMAIL = ["admin@example.com"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DB"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": "postgres",
        "PORT": "5432",
    }
}

DEFAULT_DOMAIN = "http://0.0.0.0:8000"
ODOO_DOMAIN = "localhost"
ODOO_JOURNAL_ID_FOR_INTERNAL_TRANSFERENCES = 77
ODOO_ACCOUNT_EXPENSES_PAYMENT = 11111
ODOO_ACCOUNT_CLIENTS = 22222
ODOO_ACCOUNT_SOCIAL_SECURITY = 33333
ODOO_ACCOUNT_LOANS_AVAILABLE = 44444 
ODOO_ACCOUNT_FOR_TAXES = 55555

ODOO_DEFAULT_GROUP_NEW_USERS = 33  # group_user_own_documents_only
BANK_MAIN_ACCOUNT_ACC_NUMBER = "ES00 0000 0001 0002 0003 0004"

ODOO_DBNAME = "odoo_db"
ODOO_USERNAME = "admin@example.com"
ODOO_PASSWORD = "password"
ODOO_PORT = 8069
ODOO_HOST = "localhost"
ODOO_SAAS = False

TELEGRAM_BOT_ID = ""
