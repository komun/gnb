# -*- coding: utf-8 -*-
from xml.etree.ElementTree import SubElement
import pycountry, re

def get_country_code(country_name):
    try:
        return pycountry.countries.lookup(country_name).alpha_3
    except LookupError:
        return None
    
def get_clean_province(province):
    return re.sub(r'\([^)]*\)', '', province)

def create_xml_tree(data, element):
  print(data)
  for key, value in data.items():
    if isinstance(value, dict):
        new_element = SubElement(element, key)
        create_xml_tree(value, new_element)
    elif isinstance(value, list):
        new_element = SubElement(element, key)
        for item in value:
            create_xml_tree(item, new_element)
    else:    
        SubElement(element, key).text = str(value)


def get_taxid_name(tax_id):
    tax_id_name_map = {
        42: 'IVA 4%', 43: 'IVA 10%',
        2: 'IVA 21%', 65: 'Exportación',
        47: 'Sin IVA 0%', 19: 'Servicio intracomunitario 0%',
        35: 'IVA Exento Repercutido No Sujeto'
    }
    return tax_id_name_map.get(tax_id)

def get_taxid_quantity(tax_id):
    tax_id_quantity_map = {
        42: 4, 43: 10,
        2: 21, 65: 0,
        47: 0, 19: 0,
        35: 0, 1: 21
    }
    return tax_id_quantity_map.get(tax_id)


tax_types = {
    "01": "IVA: Impuesto sobre el valor añadido",
    "02": "IPSI: Impuesto sobre la producción, los servicios y la importación",
    "03": "IGIC: Impuesto general indirecto de Canarias",
    "04": "IRPF: Impuesto sobre la Renta de las personas físicas",
    "05": "Otro",
    "06": "ITPAJD: Impuesto sobre transmisiones patrimoniales y actos jurídicos documentados",
    "07": "IE: Impuestos especiales",
    "08": "Ra: Renta aduanas",
    "09": "IGTECM: Impuesto general sobre el tráfico de empresas que se aplica en Ceuta y Melilla",
    "10": "IECDPCAC: Impuesto especial sobre los combustibles derivados del petróleo en la Comunidad Autonoma Canaria",
    "11": "IIIMAB: Impuesto sobre las instalaciones que inciden sobre el medio ambiente en la Baleares",
    "12": "ICIO: Impuesto sobre las construcciones, instalaciones y obras",
    "13": "IMVDN: Impuesto municipal sobre las viviendas desocupadas en Navarra",
    "14": "IMSN: Impuesto municipal sobre solares en Navarra",
    "15": "IMGSN: Impuesto municipal sobre gastos suntuarios en Navarra",
    "16": "IMPN: Impuesto municipal sobre publicidad en Navarra",
    "17": "REIVA: Régimen especial de IVA para agencias de viajes",
    "18": "REIGIC: Régimen especial de IGIC para agencias de viajes",
    "19": "REIPSI: Régimen especial de IPSI para agencias de viajes"
}