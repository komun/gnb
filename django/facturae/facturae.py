# -*- coding: utf-8 -*-

from xml.etree.ElementTree import Element, SubElement, tostring
import io
from facturae.utils import create_xml_tree


class Invoice():
  def __init__(self, number, invoice_class, date, total_untaxed, total_tax, total_amount,  seller, buyer, invoice_lines, iban=None):
    self.number = number
    self.invoice_class = invoice_class
    self.date = date
    self.tax_rate = (total_amount - total_untaxed) / total_untaxed * 100
    self.total_amount = total_amount
    self.total_untaxed = total_untaxed
    self.total_tax = total_tax
    self.seller = seller
    self.buyer = buyer
    self.invoice_lines = invoice_lines
    self.iban = iban
    self.validate()

  def validate(self):
    # Invoice class : OO for normal OR for refund/corrective
    if self.invoice_class not in ["OO", "OR"]:
      print("Invalid invoice class")
     

  def get_header_data(self):
    return {
      "SchemaVersion": "3.2.1",
      "Modality": "I",
      "InvoiceIssuerType": "EM",
      "Batch": {
          "BatchIdentifier": self.number,
          "InvoicesCount": "1",
          "TotalInvoicesAmount": {
              "TotalAmount": '%.2f' % self.total_amount
          },
          "TotalOutstandingAmount": {
              "TotalAmount": '%.2f' % self.total_amount
          },
          "TotalExecutableAmount": {
              "TotalAmount": '%.2f' % self.total_amount
          },
          "InvoiceCurrencyCode": "EUR"
      }
    }
  
  def get_parties_data(self):
    return {
      "SellerParty": self.seller.to_dict(),
      "BuyerParty": self.buyer.to_dict()
    }
  
  def get_invoice_data(self):
    invoice_data = {
      "Invoice": {
        "InvoiceHeader": {
          "InvoiceNumber": self.number,
          "InvoiceSeriesCode": "",
          "InvoiceDocumentType": "FC",
          "InvoiceClass": self.invoice_class
        },
        "InvoiceIssueData": {
          "IssueDate": self.date,
          "InvoiceCurrencyCode": "EUR",
          "TaxCurrencyCode": "EUR",
          "LanguageName": "es"
        },
        "TaxesOutputs": {
          "Tax": {
            "TaxTypeCode": "01",
            "TaxRate": '%.2f' % self.tax_rate,
            "TaxableBase": {
              "TotalAmount": '%.2f' % self.total_untaxed
            },
            "TaxAmount": {
              "TotalAmount": '%.2f' % (self.total_amount - self.total_untaxed)
            }
          }
        },
        "InvoiceTotals": {
          "TotalGrossAmount": '%.2f' % self.total_untaxed,
          "TotalGeneralDiscounts": 0,
          "TotalGeneralSurcharges": 0,
          "TotalGrossAmountBeforeTaxes": '%.2f' % self.total_untaxed,
          "TotalTaxOutputs": '%.2f' % (self.total_amount - self.total_untaxed),
          "TotalTaxesWithheld": 0,
          "InvoiceTotal": '%.2f' % self.total_amount,
          "TotalOutstandingAmount": '%.2f' % self.total_amount,
          "TotalExecutableAmount": '%.2f' % self.total_amount
        },
        "Items": self.invoice_lines,
        "PaymentDetails": {
          "Installment": {
            "InstallmentDueDate": self.date,
            "InstallmentAmount": '%.2f' % self.total_amount,
            "PaymentMeans": "04"
          }
        }
      }
    }

    if self.iban:
      invoice_data["Invoice"]["PaymentDetails"]["Installment"]["AccountToBeCredited"] = {
        "IBAN": self.iban,
        "BranchInSpainAddress": self.seller.address.to_dict()
      }

    return invoice_data
  
  def generate_xml(self):
      # Create root element
      root = Element("fe:Facturae", 
                                {
                                  "xmlns:ds": "http://www.w3.org/2000/09/xmldsig#",
                                  "xmlns:fe": "http://www.facturae.es/Facturae/2014/v3.2.1/Facturae"})
      # Add FileHeader data
      file_header_element = SubElement(root, "FileHeader")
      create_xml_tree(self.get_header_data(), file_header_element)
      
      # Add Parties data
      parties_element = SubElement(root, "Parties")
      create_xml_tree(self.get_parties_data(), parties_element)
  
      # Add Invoice data
      invoice_element = SubElement(root, "Invoices")
      create_xml_tree(self.get_invoice_data(), invoice_element)

      # Convert XML tree to string
      return tostring(root, encoding="UTF-8", method='xml').decode()

  
class InvoiceLine():
  def __init__(self, product_name, quantity, price_untaxed, tax_rate):
    self.product_name = product_name
    self.quantity = quantity
    self.price_untaxed = price_untaxed
    self.tax_rate = tax_rate
  
  def to_dict(self):
    return {
      "InvoiceLine" : {
        "ItemDescription": self.product_name,
        "Quantity": self.quantity,
        "UnitOfMeasure": "01",
        "UnitPriceWithoutTax": '%.2f' % self.price_untaxed,
        "TotalCost": '%.2f' % (self.price_untaxed * self.quantity),
        "GrossAmount": '%.2f' % (self.price_untaxed * self.quantity),
        "TaxesOutputs": {
            "Tax": {
                "TaxTypeCode": "01",
                "TaxRate": '%.2f' % self.tax_rate,
                "TaxableBase": {
                    "TotalAmount": '%.2f' % (self.price_untaxed * self.quantity)
                },
                "TaxAmount": {
                    "TotalAmount": '%.2f' % (self.price_untaxed * self.quantity * self.tax_rate / 100)
                }
            }
        }
      }
    }


class Address():
  def __init__(self, address, postcode, town, province, country_code):
    self.address = address
    self.postcode = postcode
    self.town = town
    self.province = province
    self.country_code = country_code

  def to_dict(self):
    return {
        "Address": self.address,
        "PostCode": self.postcode,
        "Town": self.town,
        "Province": self.province,
        "CountryCode": self.country_code  
    }


class Contact():
  def __init__(self, name, nif, person_type, address, administrative_centres=None):
    self.name = name
    self.nif = nif
    self.person_type = person_type
    self.address = address
    self.administrative_centres = administrative_centres
  
  def to_dict(self):
    contact_dict = {
      "TaxIdentification": {
          "PersonTypeCode": self.person_type,
          "ResidenceTypeCode": "R",
          "TaxIdentificationNumber": self.nif
      }
    }

    if self.administrative_centres:
      contact_dict["AdministrativeCentres"] = self.administrative_centres

    contact_dict["LegalEntity"] = {
        "CorporateName": self.name,
        "TradeName": self.name,
        "AddressInSpain": self.address.to_dict()
    }

    return contact_dict


class AdministrativeCentre():
  def __init__(self, centre_code, role_code, name, address):
    self.centre_code = centre_code
    self.role_code = role_code
    self.name = name
    self.address = address
  
  def to_dict(self):
    return { 
       "AdministrativeCentre" : {
          "CentreCode": self.centre_code,
          "RoleTypeCode": self.role_code,
          "Name": self.name,
          "AddressInSpain": self.address.to_dict()
      }       
    }





# # Ejemplo de uso

#     seller_address = Address("CALLE TERCIA", "22300", "CASAS IBAÑEZ", "ALBACETE", "ESP")
#     seller = Contact("Compañía Ejemplo SL", "B12345678", "J", seller_address)
    
#     buyer_address = Address("CALLE COMPRADOR", "22600", "HELLIN", "ALBACETE", "ESP")
#     administrative_centres = [
#       AdministrativeCentre("L01020194", "01", "Centro Ejemplo", buyer_address).to_dict(),
#       AdministrativeCentre("L01020194", "02", "Centro Ejemplo", buyer_address).to_dict(),
#       AdministrativeCentre("L01020194", "03", "Centro Ejemplo", buyer_address).to_dict()
#     ]
#     buyer = Contact("Comprador Ejemplo", "B12345678", "J", buyer_address, administrative_centres)

#     invoice_lines = [
#         InvoiceLine("Producto Test", 2, 50, 21).to_dict(),
#         InvoiceLine("Producto Test 2", 1, 100, 21).to_dict(),
#     ]

#     invoice = Invoice("2023-RCE-022", "OO", "2023-01-01", 21, 242, seller, buyer, invoice_lines)

#     generated_xml = invoice.generate_xml()
#     print(generated_xml)
#     with io.open("factura.xml", "w", encoding="utf-8") as file:
#         file.write(generated_xml)


