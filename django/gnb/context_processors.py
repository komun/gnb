from . import __VERSION__
import gnb.utils
from django.conf import settings


def global_variables(request):
    return {
        "APP_VERSION": __VERSION__,
        "DEV": settings.DEV,
        "CURRENT_DATABASE": settings.ODOO_DBNAME,
        "ODOO_DOMAIN": settings.ODOO_DOMAIN,
        "DEFAULT_DOMAIN": settings.DEFAULT_DOMAIN,
        "CURRENT_YEAR": gnb.utils.CURRENT_YEAR,
        "INVOICE_ORG_DETAILS": getattr(settings, "INVOICE_ORG_DETAILS", ""),
    }
