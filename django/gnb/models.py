# -*- coding: utf-8 -*-

from django.db import models
from django import forms
from django.utils import timezone
from django.utils.html import mark_safe
import datetime
from time import strftime
from django.contrib.sessions.models import Session
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import date as _date
from django.contrib.auth.models import User
from django.contrib import messages
import os
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.forms import widgets
import odoo.accounts


class PercentageField(models.FloatField):
    widget = widgets.TextInput(attrs={"class": "percentInput"})

    def to_python(self, value):
        val = super(PercentageField, self).to_python(value)
        if type(val) == int or float:
            return val / 100
        return val

    def prepare_value(self, value):
        val = super(PercentageField, self).prepare_value(value)
        if (type(val) == int or float) and not isinstance(val, str):
            return str((float(val) * 100))
        return val


class CharFieldWithTextarea(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 3, "cols": 100})})
        return super(CharFieldWithTextarea, self).formfield(**kwargs)

class AccountTax(models.Model):
    TYPE_TAX_USE_CHOICES = [
        ('sell', 'Ventas'),
        ('purchase', 'Compras'),
    ]
    
    name = models.CharField(max_length=100)
    odoo_account_tax_id = models.IntegerField(help_text="Internal value used for accounting")
    tax_amount = models.DecimalField(max_digits=5, decimal_places=2)
    type_tax_use = models.CharField(
        max_length=10, 
        choices=TYPE_TAX_USE_CHOICES,
        default='sell',
        verbose_name='Ámbito del impuesto'
    )
    is_active = models.BooleanField(default=True)
    order = models.IntegerField(default=0)

    class Meta:
        ordering = ['type_tax_use', 'order']
        verbose_name = "Impuesto"
        verbose_name_plural = "Impuestos"

    def __str__(self):
        return f"{self.name} ({self.tax_amount}%)"

class UserProfile(models.Model):
    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
    )
    language = models.CharField(
        _("Idioma interfaz"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.DEFAULT_LANGUAGE,
        help_text=_("El idioma de la interfaz."),
    )


class Project(models.Model):
    name = models.CharField(_("Nombre del proyecto"), max_length=100)
    code = models.CharField(_("Código del proyecto"), max_length=10, unique=True)
    is_main_project = models.BooleanField(_("Es el proyecto principal"), default=False)
    can_invoice = models.BooleanField(_("Tiene facturas"), default=True)
    archived = models.BooleanField(_("Archivado"), default=False)
    users = models.ManyToManyField(User, through="ProjectMembership")
    percentage_recomended_income_donate = models.FloatField(
        _("% de donaciones"), null=True, blank=True, default=10
    )
    account_checking = models.IntegerField(
        _("Cuenta corriente"), unique=True, null=True, blank=True
    )
    account_cash = models.IntegerField(
        _("Cuenta efectivo"), unique=True, null=True, blank=True
    )
    account_expenses = models.IntegerField(
        _("Cuenta gastos"), unique=True, null=True, blank=True
    )
    account_sales = models.IntegerField(
        _("Cuenta ingresos"), unique=True, null=True, blank=True
    )
    account_loans = models.IntegerField(
        _("Cuenta préstamos"), unique=True, null=True, blank=True
    )

    def __str__(self):
        return self.code


class CashAccount(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="cash_accounts"
    )
    name = models.CharField(_("Nombre de la caja de efectivo"), max_length=100)
    user_admins = models.ManyToManyField(User, related_name="cash_accounts", blank=True)
    account_number = models.IntegerField(
        _("Número de cuenta"), null=True, blank=True, unique=True
    )

    def __str__(self):
        return f"Cash Account for Project {self.project.code}"


class ProjectMembership(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="project_memberships"
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, blank=False, related_name="memberships"
    )
    ROLE_CHOICES = (
        ("member", "Miembro"),
        ("admin", "Administrador"),
        # Add more role options as needed
    )
    role = models.CharField(_("Rol"), max_length=10, choices=ROLE_CHOICES, blank=False)

    class Meta:
        unique_together = ("user", "project")  # Ensure uniqueness of user-project pairs


class PaymentScope(models.TextChoices):
    INTERNAL = "internal", _("Internal")
    EXTERNAL = "external", _("External")
    ATM_WITHDRAW = "atm_withdraw", _("ATM Withdraw")


class PaymentType(models.TextChoices):
    DEBIT = "debit", _("Debit Payment")
    CREDIT = "credit", _("Credit Payment")


class PaymentStatus(models.TextChoices):
    PENDING = "pending", _("Pending")
    PAID = "paid", _("Paid")
    REJECTED = "rejected", _("Rejected")


class RepeatInterval(models.TextChoices):
    NEVER = "never", _("Never")
    DAILY = "daily", _("Daily")
    WEEKLY = "weekly", _("Weekly")
    YEARLY = "yearly", _("Yearly")
    TRIMESTRAL = "trimestral", _("Trimestral")
    CUATRIMESTRAL = "cuatrimestral", _("Cuatrimestral")
    MONTHLY = "monthly", _("Monthly")


class BasePayment(models.Model):
    payment_type = models.CharField(
        max_length=10, choices=PaymentType.choices, default=PaymentType.DEBIT
    )
    payment_scope = models.CharField(
        max_length=20, choices=PaymentScope.choices, default=PaymentScope.INTERNAL
    )
    to_account = models.CharField(max_length=100)
    iban_account = models.CharField(max_length=100, null=True, blank=True)
    from_account = models.CharField(max_length=100)
    currency = models.CharField(max_length=10)

    class Meta:
        abstract = True


class PaymentContract(BasePayment):
    repeat_init_day = models.DateField(null=True, blank=True)
    interval_freq = models.CharField(
        max_length=20, choices=RepeatInterval.choices, default=RepeatInterval.YEARLY
    )
    repeat_end_times = models.IntegerField(null=True, blank=True)
    active = models.BooleanField(default=True)
    created_by_project = models.ForeignKey(
        "Project", null=True, blank=True, on_delete=models.SET_NULL
    )
    created_by_user = models.ForeignKey(User, on_delete=models.CASCADE)
    canceled_by_user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="canceled_payments",
    )
    canceled_date = models.DateTimeField(null=True, blank=True)
    accepted = models.BooleanField(default=False)


class Payment(BasePayment):
    payment_contract = models.ForeignKey(
        PaymentContract,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    due_date = models.DateTimeField()
    account_move_id = models.PositiveIntegerField(null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    paid = models.BooleanField(default=False)
    status = models.CharField(
        max_length=20, choices=PaymentStatus.choices, default=PaymentStatus.PENDING
    )
    reference = models.CharField(max_length=300, null=True, blank=True)

    def to_account_str(self):
        if self.payment_scope == PaymentScope.EXTERNAL:
            return f"{self.iban_account} ({self.to_account})"
        else:
            return self.to_account
