from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from unittest.mock import patch
from django.conf import settings
from django.contrib.messages import SUCCESS, ERROR
from gnb.models import Project
from gnb.views import add_menu_context
from gnb.tests import create_sample_user_with_odooprofile, create_sample_project


class ContactViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("testuser", "test@example.com", "password")
        self.client.login(username="testuser", password="password")

    @patch("gnb.views.send_mail")
    def test_contact_view_sends_email_success(self, mock_send_mail):
        # Simulate form input data
        data = {"contact_msg": "Este es el mensaje de contacto"}

        # Send post request to the view
        response = self.client.post("/gnb/contact/", data=data)

        # Check if the response status code is a redirect (302)
        self.assertEqual(response.status_code, 302)

        # Follow the redirect and check the status code of the destination
        # Note: The 'follow=True' option is used here
        redirect_response = self.client.get(response.url, follow=True)
        self.assertEqual(redirect_response.status_code, 200)

        # Check that the email was sent
        self.assertTrue(mock_send_mail.called)
        call_args_list = mock_send_mail.call_args_list
        self.assertEqual(len(call_args_list), 1)  # Assuming only one email was sent
        call_args = call_args_list[0]
        email_subject = call_args[0][0]
        email_message = call_args[0][1]
        # email_from = call_args[0][2]
        email_to = call_args[0][3]

        # Your assertions on the email content
        self.assertEqual(email_subject, "GNBAdmin: Formulario de Contacto")
        self.assertIn(data["contact_msg"], email_message)
        self.assertIn(self.user.email, email_message)

        # Check that the email was sent to the administrator
        self.assertIn(self.user.email, email_to)
        for admin_email in settings.ADMINS_EMAIL:
            self.assertIn(admin_email, email_to)

        # Check that a success message was displayed
        mymessages = list(redirect_response.context["messages"])
        self.assertEqual(len(mymessages), 1)
        self.assertEqual(mymessages[0].level, SUCCESS)

    @patch("gnb.views.send_mail", side_effect=Exception("Error sending email"))
    def test_contact_view_sends_email_fail(self, mock_send_mail):
        # Simulate form input data
        data = {"contact_msg": "Este es el mensaje de contacto"}

        # Send post request to the view
        response = self.client.post("/gnb/contact/", data=data)

        # Check if the response status code is a redirect (302)
        self.assertEqual(response.status_code, 302)

        # Follow the redirect and check the status code of the destination
        # Note: The 'follow=True' option is used here
        redirect_response = self.client.get(response.url, follow=True)
        self.assertEqual(redirect_response.status_code, 200)

        # Verify that the email was NOT sent
        self.assertTrue(mock_send_mail.called)

        # Verify that an error message was displayed
        mymessages = list(redirect_response.context["messages"])
        self.assertEqual(len(mymessages), 1)
        self.assertEqual(mymessages[0].level, ERROR)


class AddMenuContextTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="user1", email="user1@example.com")
        self.user2 = User.objects.create(username="user2", email="user2@example.com")
        self.client = Client()

    def test_add_menu_context(self):
        response = self.client.get(reverse("index"))
        context = {}
        updated_context = add_menu_context(
            response.wsgi_request, context, "contact_view"
        )

        # Verify that the context is updated correctly
        self.assertIn("all_users", updated_context)
        self.assertIn("navbar", updated_context)
        self.assertEqual(updated_context["navbar"], "contact_view")

        # Verify that the context contains all users
        self.assertEqual(len(updated_context["all_users"]), 2)
        self.assertIn(self.user1, updated_context["all_users"])
        self.assertIn(self.user2, updated_context["all_users"])


class FrontendViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.frontend_view = reverse("index")
        self.user = create_sample_user_with_odooprofile()

    def test_frontend_view_authenticated_user(self):
        self.client.login(username="testuser", password="password")
        response = self.client.get(self.frontend_view)

        self.assertEqual(response.status_code, 302)  # Redirect to dashboard
        self.assertEqual(response.url, reverse("my_dashboard_view"))

        # Follow redirect and check if dashboard template is used after
        redirect_response = self.client.get(response.url, follow=True)
        self.assertEqual(redirect_response.status_code, 200)
        self.assertTemplateUsed(redirect_response, "frontend/my_dashboard.html")

    def test_frontend_view_unauthenticated_user(self):
        response = self.client.get(self.frontend_view)

        # Check if logout page is loaded for unauthenticated users
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")


class InvoicesViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = create_sample_user_with_odooprofile()
        self.project, self.membership = create_sample_project(self.user)
        self.invoices_view = reverse("get_invoices_view")
        self.invoice_view = reverse("get_invoice_view", kwargs={"invoice_id": 1})
        self.new_invoice_view = reverse(
            "new_invoice_view", kwargs={"project_code": "PRO_EJE"}
        )
        self.client.login(username="testuser", password="password")

    @patch("odoo.invoices.get_invoices_by_user")
    def test_get_invoices(self, mock_get_invoices_by_user):
        mock_get_invoices_by_user.return_value = [
            {"id": 1, "name": "2024/PRO_EJE/01"},
            {"id": 2, "name": "2024/PRO_EJE/02"},
        ]

        response = self.client.get(self.invoices_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/get_invoices.html")
        self.assertEqual(len(response.context["items"]), 2)
        self.assertEqual(response.context["items"][0]["name"], "2024/PRO_EJE/01")

    @patch("odoo.invoices.get_invoices_ids_allowed")
    @patch("odoo.invoices.get_invoice")
    def test_get_invoice_onwned_by_user(
        self, mock_get_invoice, mock_get_invoices_ids_allowed
    ):
        mock_get_invoice.return_value = {
            "id": 1,
            "name": "2024/PRO_EJE/01",
            "partner_id": [1],
        }, []
        mock_get_invoices_ids_allowed.return_value = [1]
        response = self.client.get(self.invoice_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/get_invoice.html")

    @patch("odoo.invoices.get_invoices_ids_allowed")
    @patch("odoo.invoices.get_invoice")
    def test_get_invoice_project_admin_user(
        self, mock_get_invoice, mock_get_invoices_ids_allowed
    ):
        mock_get_invoice.return_value = {
            "id": 1,
            "name": "2024/PRO_EJE/01",
            "partner_id": [1],
        }, []
        mock_get_invoices_ids_allowed.return_value = []
        self.membership.role = "admin"
        self.membership.save()
        response = self.client.get(self.invoice_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/get_invoice.html")

    @patch("odoo.invoices.get_invoices_ids_allowed")
    @patch("odoo.invoices.get_invoice")
    def test_get_invoice_not_allowed_user(
        self, mock_get_invoice, mock_get_invoices_ids_allowed
    ):
        mock_get_invoice.return_value = {
            "id": 1,
            "name": "2024/PRO_EJE/01",
            "partner_id": [1],
        }, []
        mock_get_invoices_ids_allowed.return_value = []
        response = self.client.get(self.invoice_view)

        self.assertEqual(response.status_code, 403)

    @patch("odoo.invoices.get_invoices_ids_allowed")
    @patch("odoo.invoices.get_invoice")
    def test_get_invoice_superuser(
        self, mock_get_invoice, mock_get_invoices_ids_allowed
    ):
        mock_get_invoice.return_value = {
            "id": 1,
            "name": "2024/PRO_EJE/01",
            "partner_id": [1],
        }, []
        mock_get_invoices_ids_allowed.return_value = []
        self.user.is_superuser = True
        self.user.save()
        response = self.client.get(self.invoice_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/get_invoice.html")

    @patch("odoo.invoices.get_invoices_ids_allowed")
    @patch("odoo.invoices.get_invoice")
    def test_get_invoice_not_exist(
        self, mock_get_invoice, mock_get_invoices_ids_allowed
    ):
        mock_get_invoice.return_value = {}, []
        mock_get_invoices_ids_allowed.return_value = []
        response = self.client.get(self.invoice_view)

        # To do : Fix in view, it throws an error because invoice not found
        self.assertEqual(response.status_code, 404)


class ExpensesViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = create_sample_user_with_odooprofile()
        self.client.login(username="testuser", password="password")
        self.project, self.membership = create_sample_project(self.user)
        self.expenses_view = reverse("get_expenses_view")
        self.expense_sheet_view = reverse(
            "get_expense_sheet_view", kwargs={"expense_sheet_id": 2}
        )
        self.new_expense_view = reverse(
            "new_expense_view", kwargs={"project_code": self.project.code}
        )

    @patch("odoo.accounts.get_employee_id")
    @patch("odoo.expenses.get_expenses")
    def test_get_expenses(self, mock_get_expenses, mock_get_employee_id):
        mock_get_expenses.return_value = [
            {"id": 1, "name": "Expense Sheet 1", "employee_id": 1, "total_amount": 100},
            {"id": 2, "name": "Expense Sheet 2", "employee_id": 1, "total_amount": 200},
            {"id": 3, "name": "Expense Sheet 3", "employee_id": 1, "total_amount": 300},
        ]
        mock_get_employee_id.return_value = 1
        response = self.client.get(self.expenses_view)

        self.assertEqual(response.status_code, 200)
        self.assertIn("items", response.context)
        self.assertEqual(len(response.context["items"]), 3)
        self.assertEqual(response.context["items"][0]["name"], "Expense Sheet 1")
        self.assertEqual(response.context["items"][0]["total_amount"], 100)
        self.assertTemplateUsed(response, "frontend/get_expenses.html")

    @patch("odoo.expenses.get_journal_code_from_expense_sheet")
    @patch("odoo.accounts.get_employee_id")
    @patch("odoo.expenses.get_expenses")
    def test_get_expenses_employee_not_found(
        self,
        mock_get_expenses,
        mock_get_employee_id,
        mock_get_journal_code_from_expense_sheet,
    ):
        mock_get_journal_code_from_expense_sheet.return_value = "G.JOURNAL"
        mock_get_expenses.return_value = False
        mock_get_employee_id.return_value = 1

        response = self.client.get(self.expenses_view)

        self.assertEqual(response.status_code, 404)

    @patch("odoo.expenses.get_journal_code_from_expense_sheet")
    @patch("odoo.expenses.get_expenses_ids_allowed")
    @patch("odoo.expenses.get_expense_sheet")
    def test_get_expense_sheet_with_valid_data(
        self,
        mock_get_expense_sheet,
        mock_get_expenses_ids_allowed,
        mock_get_journal_code_from_expense_sheet,
    ):
        mock_get_journal_code_from_expense_sheet.return_value = "G.JOURNAL"
        mock_get_expenses_ids_allowed.return_value = [1, 2, 3]
        mock_get_expense_sheet.return_value = (
            {
                "id": 2,
                "name": "Test Expense Sheet 2",
                "expense_line_ids": [1, 2, 3],
                "total_amount": 100,
                "taxed_amount": 30,
                "untaxed_amount": 70,
            },
            [
                {"id": 1, "total_amount": 50, "untaxed_amount": 40},
                {"id": 2, "total_amount": 30, "untaxed_amount": 20},
                {"id": 3, "total_amount": 20, "untaxed_amount": 10},
            ],
        )
        response = self.client.get(self.expense_sheet_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/get_expense.html")
        self.assertEqual(len(response.context["move_lines"]), 3)
        self.assertEqual(response.context["object"]["total_amount"], 100)
        self.assertEqual(response.context["object"]["taxed_amount"], 30)
        self.assertEqual(response.context["object"]["untaxed_amount"], 70)

    @patch("odoo.expenses.get_journal_code_from_expense_sheet")
    @patch("odoo.expenses.get_expenses_ids_allowed")
    def test_get_expense_sheet_not_allowed(
        self,
        mock_get_expenses_ids_allowed,
        mock_get_journal_code_from_expense_sheet,
    ):
        mock_get_journal_code_from_expense_sheet.return_value = "G.JOURNAL"
        mock_get_expenses_ids_allowed.return_value = [5, 6, 7]
        response = self.client.get(self.expense_sheet_view)

        self.assertEqual(response.status_code, 403)

    @patch("odoo.expenses.get_journal_code_from_expense_sheet")
    @patch("odoo.expenses.get_expenses_ids_allowed")
    @patch("odoo.expenses.get_expense_sheet")
    def test_get_expense_sheet_not_found(
        self,
        mock_get_expense_sheet,
        mock_get_expenses_ids_allowed,
        mock_get_journal_code_from_expense_sheet,
    ):
        mock_get_journal_code_from_expense_sheet.return_value = "G.JOURNAL"
        mock_get_expenses_ids_allowed.return_value = [1, 2, 3]
        mock_get_expense_sheet.return_value = (None, None)
        response = self.client.get(self.expense_sheet_view)

        self.assertEqual(response.status_code, 404)

    @patch("odoo.accounts.get_employee_id")
    def test_new_expense_view(self, mock_get_employee_id):
        mock_get_employee_id.return_value = 1
        response = self.client.get(self.new_expense_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/new_expense.html")
        self.assertEqual(response.context["project_code"], self.project.code)


class UsersReportViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = create_sample_user_with_odooprofile()
        self.superuser = User.objects.create_superuser(
            username="admin", email="admin@example.com", password="password"
        )
        self.users_report_view = reverse("users_report_view")

    def test_users_report_view_as_superuser(self):
        self.client.login(username="admin", password="password")

        response = self.client.get(self.users_report_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/report_users.html")
        # Only counts users with odooprofile
        self.assertEqual(len(response.context["total_users"]), 1)

    def test_users_report_view_as_regular_user(self):
        self.client.login(username="testuser", password="password")
        response = self.client.get(self.users_report_view)

        self.assertEqual(response.status_code, 403)


class ProjectsReportViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = create_sample_user_with_odooprofile()
        self.superuser = User.objects.create_superuser(
            username="admin", email="admin@example.com", password="password"
        )
        self.project, self.membership = create_sample_project(self.user)
        self.projects_report_view = reverse("projects_report_view")

    def test_users_report_view_as_superuser(self):
        self.client.login(username="admin", password="password")
        response = self.client.get(self.projects_report_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/report_projects.html")
        self.assertEqual(len(response.context["total_projects"]), 1)

    def test_users_report_view_as_regular_user(self):
        self.client.login(username="testuser", password="password")
        response = self.client.get(self.projects_report_view)

        self.assertEqual(response.status_code, 403)


class NewCashinViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.superuser = User.objects.create_superuser(
            username="admin", email="admin@example.com", password="password"
        )
        self.user = create_sample_user_with_odooprofile()
        self.project, self.membership = create_sample_project(self.user)
        self.new_cashin_view = reverse("new_cashin_view")
        self.post_data = {
            "received_amount": 100.0,
            "unpaid_invoice_id": "3000",  # corresponds to 2024/PRO_EJE/002
            "from_journal": "test_journal",
            "reference": "test_reference",
            "cashin_date": "2023-03-16",
        }

    def test_new_cashin_view_as_regular_user(self):
        self.client.login(username="testuser", password="password")
        response = self.client.get(self.new_cashin_view)

        self.assertEqual(response.status_code, 403)

    def test_new_cashin_view_get_request_as_superuser(self):
        self.client.login(username="admin", password="password")
        response = self.client.get(self.new_cashin_view)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        all_projects = list(Project.objects.values("code", "name"))
        self.assertEqual(
            all_projects,
            [{"code": "PRO_EJE", "name": "Proyecto ejemplo"}],
        )

    @patch("odoo.accounts")
    def test_new_cashin_view_post_without_invoice_success(self, mock_accounts):
        self.client.login(username="admin", password="password")
        mock_accounts.get_account_id_from_journal.return_value = 200
        mock_accounts.get_code_from_account_id.return_value = 123000
        mock_accounts.create_bank_account_move.return_value = 100, None
        self.post_data["unpaid_invoice_id"] = ""
        response = self.client.post(self.new_cashin_view, self.post_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        self.assertContains(response, "Éxito en la operación.")

    @patch("odoo.accounts")
    def test_new_cashin_view_post_without_invoice_error(self, mock_accounts):
        self.client.login(username="admin", password="password")
        mock_accounts.get_account_id_from_journal.return_value = 200
        mock_accounts.get_code_from_account_id.return_value = 123000
        mock_accounts.create_bank_account_move.return_value = None, "Sample error"
        self.post_data["unpaid_invoice_id"] = ""
        response = self.client.post(self.new_cashin_view, self.post_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        self.assertContains(response, "Sample error")

    @patch("odoo.invoices.get_invoice_by_id")
    def test_new_cashin_view_post_with_invoice_not_found(self, mock_get_invoice_by_id):
        self.client.login(username="admin", password="password")
        mock_get_invoice_by_id.return_value = None
        response = self.client.post(self.new_cashin_view, self.post_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        self.assertContains(response, "Error encontrando la factura 3000")
        # To do: Fix in view invoice["name"] access before check if invoice exists

    @patch("odoo.accounts.create_cashin_account_move")
    @patch("odoo.invoices.get_invoice_by_id")
    def test_new_cashin_view_post_with_invoice(
        self, mock_get_invoice_by_id, mock_create_cashin_account_move
    ):
        self.client.login(username="admin", password="password")
        mock_get_invoice_by_id.return_value = {
            "id": 3000,
            "name": "2024/PRO_EJE/002",
            "amount_untaxed": 100,
        }
        mock_create_cashin_account_move.return_value = {
            "id": 101,
            "name": "MOV1",
            "amount_total": 100,
        }, None
        response = self.client.post(self.new_cashin_view, self.post_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        self.assertContains(response, "Éxito pagando la factura 2024/PRO_EJE/002")

    @patch("odoo.accounts.create_cashin_account_move")
    @patch("odoo.invoices.get_invoice_by_id")
    def test_new_cashin_view_post_with_invoice_error(
        self, mock_get_invoice_by_id, mock_create_cashin_account_move
    ):
        self.client.login(username="admin", password="password")
        mock_get_invoice_by_id.return_value = {
            "id": 100,
            "name": "2024/PRO_EJE/002",
            "amount_untaxed": 100,
        }
        mock_create_cashin_account_move.return_value = None, "Error message"
        response = self.client.post(self.new_cashin_view, self.post_data)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "frontend/admin/new_cashin.html")
        self.assertContains(response, "Error message")
