from django.contrib.auth.models import User

from gnb.views import Project, ProjectMembership
from odoo.models import OdooProfile


def create_sample_user_with_odooprofile():
    user = User.objects.create_user("testuser", "test@example.com", "password")
    user.odooprofile = OdooProfile.objects.create(
        user=user,
        odoo_user_id=user.id,
        account_checking=0,
        account_social_capital=0,
        account_loans=0,
        account_voluntary_contribution=0,
    )
    return user


def create_sample_project(sample_user):
    sample_project = Project.objects.create(
        name="Proyecto ejemplo",
        code="PRO_EJE",
        is_main_project=False,
        percentage_recomended_income_donate=10,
        account_checking=123,
        account_cash=10,
        account_expenses=20,
        account_sales=30,
        account_loans=40,
    )

    sample_membership = ProjectMembership.objects.create(
        user=sample_user, project=sample_project, role="member"
    )

    return sample_project, sample_membership
