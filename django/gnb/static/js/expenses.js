//VARIABLES GENERALES

//FUNCIONES
// Establecer la fecha actual como valor por defecto
document.getElementById('date').value = obtenerFechaActual();

// Ocultar el campo de fecha de hoja de gastos manteniendo solo el input hidden
$('#date').parent().hide();

// Función para actualizar la fecha de la hoja de gastos
function updateExpenseSheetDate() {
    var latestDate = null;
    
    // Corregido el selector para que coincida con el ID del HTML
    $('input[id^="date_line-"]').each(function() {
        var currentDate = $(this).val();
        if (currentDate) {
            if (!latestDate || currentDate > latestDate) {
                latestDate = currentDate;
            }
        }
    });
    
    // Actualizar el campo hidden solo si hay una fecha válida de gastos
    if (latestDate) {
        $('#date').val(latestDate);
    } else {
        // Si no hay fechas, dejar el campo vacío
        $('#date').val('');
    }
}

// Corregido el selector en el evento change
$(document).on('change', 'input[id^="date_line-"]', function() {
    updateExpenseSheetDate();
});

// Actualizar la fecha cuando se añada o elimine una fila
$(document).on('click', '.add-row-button, .rem-row-button', function() {
    setTimeout(updateExpenseSheetDate, 100);
});

// Asegurarse de que la fecha se actualice al cargar la página
$(document).ready(function() {
    updateExpenseSheetDate();
});

// Change expense price based on selected type
$('#inputContainer-d').on('change', '.form-select', function() {
    var selectedValue = $(this).val();
    var priceInput = $(this).closest('.input-row').find('#price-0');

    switch (selectedValue) {
        case 'tipo_viaje':
        priceInput.val('0.26');
        break;
        case 'tipo_dietas':
        priceInput.val('26.00');
        break;
        case 'tipo_Pernoctas':
        priceInput.val('53.00');
        break;
        default:
        priceInput.val('1.00');
    }
});

//FUNCIÓN RESTRINGIR PRODUCTO A PROYECTO
// Agregar un evento de cambio al elemento select con el ID 'journal'
$('#journal').on('change', function () {
    // Obtener el valor seleccionado en el elemento 'journal'
    var journalXXX = $('#journal').val();
    selectProductFromJournal($(this), journalXXX);
    // Iterar a través de los elementos select de productos
    $('select[id^="product-"]').each(function () {
        // Habilitar o deshabilitar opciones en función de si la referencia seleccionada está contenida en el atributo 'ref'

    });
});
// FUNCION AÑADIR LÍNEAS
var rowCount = 0; // Inicializamos el contador en 1


// Agregar un evento de clic al botón 'add-row-button'
$(document).on('click', '.add-row-button', function () {
    // Clonar la fila más cercana
    var newRow = $('#new_expense .input-row').last().clone();

    // Incrementar el contador y utilizarlo para crear IDs únicos
    rowCount++;
    newRow.find('[id*="-"]').each(function () {
        var oldID = $(this).attr('id');
        var matches = oldID.match(/-(\d+)$/);

        if (matches) {
            var number = parseInt(matches[1]);
            var newID = oldID.replace('-' + number, '-' + rowCount);
            $(this).attr('id', newID);
            $(this).attr('name', newID);

        }
    });

    // Inicializar los input fields
    newRow.find('input[type="text"]').val('');
    newRow.find('input[type="number"]').val('');
    newRow.find('[id^="taxes-"]').val(2); //21% VAT default
    newRow.find('[id^="quantity-"]').val("1");
    newRow.find('[id^="price-"]').val("1.00");
    newRow.find('[id="expense_number"]').html('Gasto ' + (rowCount + 1));


    // Insertar la nueva fila después de la fila actual
    $('#new_expense .input-row').last().after(newRow);
    resetTypeSelectTrigger();
    newRow.find("select[id^='typeselect-']").trigger("change");
    recalculateTotals();
});

function getInputRowIds() {
    var inputRowNumbers = [];
    $('[id^="inputRow-"]').each(function () {
        var match = this.id.match(/\d+/);
        if (match) {
            inputRowNumbers.push(match[0]);
        }
    });
    return inputRowNumbers;
}

$('input[name="type_expense"]').change(function () {
    // Obtener todas las filas
    var rows = $('.input-row');
    
    // Eliminar todas las filas excepto la primera
    rows.slice(1).remove();
    
    // Reiniciar la primera fila
    var firstRow = rows.first();
    firstRow.find('input[type="text"]').val('');
    firstRow.find('input[type="number"]').val('');
    firstRow.find('select').val('');
    firstRow.find('[id^="taxes-"]').val(2); //21% VAT default
    firstRow.find('[id^="quantity-"]').val("1");
    firstRow.find('[id^="price-"]').val("1.00");
    firstRow.find('[id^="file-"]').val('');  // Limpiar archivo si existe
    
    // Resetear el contador de filas
    rowCount = 0;
    
    // Actualizar los campos visibles según el tipo seleccionado
    ids = [0];  // Solo quedará la primera fila
    for (var i = 0; i < ids.length; i++) {
        toggleFields(ids[i]);
    }
    
    // Recalcular totales
    recalculateTotals();
});

function recalculateTotals() {
    var totalWithoutTaxes = 0;
    var totalTaxes = 0;
    var total = 0;
    
    $('#new_expense .input-row').each(function () {
        var newRow = $(this);
        // Comprobar el tipo de gasto seleccionado
        var typeExpense = $('input[name="type_expense"]:checked').val();
        
        var quantityValue = parseFloat(newRow.find('[id^="quantity-"]').val()) || 0;
        var priceValue = parseFloat(newRow.find('[id^="price-"]').val()) || 0;
        var rowWithoutTaxes = (quantityValue * priceValue);
        var rowTaxes = 0;

        // Solo calcular impuestos si es gasto con factura
        if (typeExpense !== 'normal_expense') {
            var taxesValue = parseFloat(newRow.find('[id^="taxes-"]').find(':selected').data('tax-amount')) || 0;
            rowTaxes = (rowWithoutTaxes * (taxesValue / 100));
        }

        totalWithoutTaxes += rowWithoutTaxes;
        totalTaxes += rowTaxes;
    });

    total = totalWithoutTaxes + totalTaxes;

    $("#subtotalValue").text(totalWithoutTaxes.toFixed(2));
    $("#taxesValue").text(totalTaxes.toFixed(2));
    $("#totalValue").text(total.toFixed(2));
}

// Asegurarse de que los totales se calculen correctamente al cargar la página
$(document).ready(function() {
    // Forzar un recálculo inicial después de que todo esté cargado
    setTimeout(recalculateTotals, 100);
});

$(document).on('change', 'input[id^="quantity-"]', function () {
    recalculateTotals();
});
$(document).on('change', 'input[id^="price-"]', function () {
    recalculateTotals();
});
$(document).on('change', 'select[id^="taxes-"]', function () {
    recalculateTotals();
});

function resetTypeSelectTrigger() {
    $("select[id^='typeselect-']").change(function () {
        // Obtener el valor seleccionado
        var selectedValue = $(this).val();
        // Obtener el número del ID del select
        var idNumber = $(this).attr("id").replace("typeselect-", "");
        // Comprobar el valor seleccionado y escribir la frase correspondiente
        if (selectedValue === "tipo_normal") {
            $("#description-" + idNumber).attr("placeholder", "");
        } else if (selectedValue === "tipo_viaje") {
            $("#description-" + idNumber).attr("placeholder", "Escribe el origen y el destino.Ej. (Albacete - Murcia), añade como unidades el nº de km y como precio 0,26€/km");
        } else if (selectedValue === "tipo_dietas") {
            $("#description-" + idNumber).attr("placeholder", "Escribe donde disfrutaste las dietas. Ej (Dieta en Albacete). El precio aprox. es 26€/día");
        } else if (selectedValue === "tipo_Pernoctas") {
            $("#description-" + idNumber).attr("placeholder", "Escribe donde disfrutaste las pernoctas. Ej (Pernocta en Albacete). El precio aprox. es 53€/día");
        } else if (selectedValue === "tipo_producto") {
            $("#description-" + idNumber).attr("placeholder", "Escribe el concepto del gasto.");
        }
    });
}

// Agregar un evento de clic al botón 'rem-row-button'
$(document).on('click', '.rem-row-button', function () {
    // Obtener todas las filas
    var rows = $('.input-row');

    // Verificar si hay más de una fila antes de eliminar
    if (rows.length > 1) {
        // Eliminar la fila actual
        $(this).closest('.input-row').remove();
    } else {
        // Si solo hay una fila, borra solo los valores en lugar de eliminarla
        $(this).closest('.input-row').find('input[type="text"]').val('');
        $(this).closest('.input-row').find('select').val('');
        $(this).closest('.input-row').find('input[type="number"]').val('');
    }
    recalculateTotals();
});

document.getElementById("iban_account").addEventListener("input", function () {
    var iban = this.value.replace(/\s/g, ""); // Elimina espacios en blanco
    if (iban.length >= 15) {
        var valid = isValidIBAN(iban); // Llama a la función de validación IBAN

        var ibanResult = document.getElementById("iban-result");
        ibanResult.innerHTML = valid ? "✓" : "❌";

        // Establece el color del icono según la validez
        if (valid) {
            ibanResult.style.color = "green";
        } else {
            ibanResult.style.color = "red";
        }
    }
});

// Función para validar el IBAN (puedes personalizarla según tus necesidades)
function isValidIBAN(iban) {
    return IBAN.isValid(iban); // true
}

//REVISAR CAMPOS HABILITADOS
function toggleFields(number) {
    var radio1Checked = document.getElementById('type_expense_normal').checked;
    var radio2Checked = document.getElementById('type_expense_receipt').checked;

    var referenciaDiv = document.getElementById('div-reference-' + number);
    var referenciaField = document.getElementById('reference-' + number);
    var fileInvoiceDiv = document.getElementById('div-file-' + number);
    var fileInvoiceField = document.getElementById('file-' + number);
    var impuestosSel = document.getElementById('div-taxes-' + number);
    var impuestosField = document.getElementById('taxes-' + number);
    var selectField = document.getElementById('div-typeselect-' + number);
    var destinoDiv = document.getElementById('destino');
    var destinoField = document.getElementById('iban_account');
    var destinoField2 = document.getElementById('iban_receiver');
    var destinoField3 = document.getElementById('iban_concept');


    if (radio1Checked) {
        referenciaDiv.style.display = 'none';
        referenciaField.removeAttribute('required');
        fileInvoiceDiv.style.display = 'none';
        fileInvoiceField.removeAttribute('required');
        impuestosSel.style.display = 'none';
        impuestosField.removeAttribute('required');
        selectField.style.display = 'block';
        destinoDiv.classList.add('d-none'); // Agrega la clase 'd-none' a la div 'destino'
        destinoField.style.display = 'none';
        destinoField.removeAttribute('required');
        destinoField2.style.display = 'none';
        destinoField2.removeAttribute('required');
        destinoField3.style.display = 'none';
        destinoField3.removeAttribute('required');

    } else {
        referenciaDiv.style.display = 'block'; // O 'initial' si prefieres restaurar el estilo predeterminado
        referenciaField.setAttribute('required', 'required');
        fileInvoiceDiv.style.display = 'block';
        fileInvoiceField.setAttribute('required', 'required');
        impuestosSel.style.display = 'block';
        impuestosField.setAttribute('required', 'required');
        selectField.style.display = 'none';
        destinoDiv.classList.remove('d-none'); // Remueve la clase 'd-none' de la div 'destino'
        destinoField.style.display = 'block';
        destinoField2.style.display = 'block';
        destinoField3.style.display = 'block';
    }
}

// Llama a la función para configurar los campos inicialmente
toggleFields(0);
$("#normal_expense-" + 0).change(function () {
    toggleFields(0);
});

$("#receipt_expense-" + 0).change(function () {
    toggleFields(0);
});
resetTypeSelectTrigger();

function clearForm() {
    var formElements = $('#new_expense').find(':input, textarea, select');
    formElements.each(function () {
        if ((this.tagName.toLowerCase() === 'input' && this.type !== 'hidden') || this.tagName.toLowerCase() === 'textarea') {
            $(this).val('');
        }
    });
}
 
// Check if all the dates of the expense are in the current quarter
function validateDates() {
    function isCurrentQuarter(date) {
        var inputDate = new Date(date);
        var currentDate = new Date();

        function getQuarter(date) {
            return Math.floor((date.getMonth() / 3)) + 1;
        }

        return inputDate.getFullYear() === currentDate.getFullYear() &&
               getQuarter(inputDate) === getQuarter(currentDate);
    }

    var allDatesValid = true;

    $('input[type="date"]').each(function() {
        var date = $(this).val();
        if (date && !isCurrentQuarter(date)) {
            allDatesValid = false;
            $(this).css('border-color', 'red');
        } else {
            $(this).css('border-color', '');
        }
    });
    return allDatesValid;
}


function sendForm() {
    // Verificar que hay al menos una fecha de gasto
    if (!$('#date').val()) {
        alert("Por favor, añada al menos una fecha de gasto.");
        return;
    }
    
    formValid = validateForm('new_expense');
    var selectedTax = $('#taxes-0').val();

    if (!formValid) {
        alert("Por favor, complete todos los campos requeridos.");
    }

    // Check if the expense with IVA is in the current cuarter
    else if (($('#type_expense_receipt').is(':checked')) && 
            (selectedTax === '42' || selectedTax === '43' || selectedTax === '2') && 
            (!validateDates())) 
    {
        alert("Hay fechas no corresponden al trimestre actual, ya se ha presentado el IVA de los trimestres anteriores.");
    }

    else {
        var formValues = {};
        var formData = new FormData($('#new_expense')[0]); // Create a FormData object with the form data
        var user_id = $('#user_id').val();
        var journal = $('#journal').val();
        showLoadingScreen();
        $.ajax({
            url: '/odoo/expense/', // Specify the URL for your Django view
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                // Handle the response from Django
                if (response.hr_expense_id === null) {
                    // Si hr_expense_id es nulo, muestra el mensaje como un error
                    alert('Error found: ' + response.message);
                } else {
                    alert('Gasto creado con éxito, se te notificará vía mail cuando haya sido aprobado.');
                    clearForm();
                    // Redirigir a la vista del gasto específico
                    var url = '/gnb/expenses/' + response.hr_expense_id + '/';
                    if (user_id) { 
                        url = url + "?user=" + user_id; 
                    }
                    window.location.href = url;
                }
                hideLoadingScreen();
            },
            error: function (xhr, status, error) {
                // Handle errors

                console.log('Error:', error);
                alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend 
                hideLoadingScreen();
            }
        });
    }
}

//ACCIONES
// Agregar un evento de cambio al elemento select con el ID 'journal'
recalculateTotals();