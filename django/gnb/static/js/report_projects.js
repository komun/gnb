
 
// Definición de la función para crear los gráficos
function createChartsForYear(year, yearLabels, yearIngresos, yearGastos) {
    const ctxBar = document.getElementById(`chartIngresosGastos-${year}`).getContext("2d");
    const ctxPie = document.getElementById(`chartIngresosPie-${year}`).getContext("2d");

    // Gráfica de barras para este año
    new Chart(ctxBar, {
        type: "bar",
        data: {
        labels: yearLabels,
        datasets: [
            {
            label: "Ingresos (€)",
            data: yearIngresos,
            backgroundColor: "rgba(75, 192, 192, 0.6)",
            borderColor: "rgba(75, 192, 192, 1)",
            borderWidth: 1,
            },
            {
            label: "Gastos (€)",
            data: yearGastos,
            backgroundColor: "rgba(255, 99, 132, 0.6)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 1,
            },
        ],
        },
        options: {
        responsive: true,
        plugins: {
            legend: {
            position: "top",
            },
        },
        scales: {
            y: {
            beginAtZero: true,
            },
        },
        },
    });

    // Gráfica de pie para este año
    new Chart(ctxPie, {
        type: "pie",
        data: {
        labels: yearLabels,
        datasets: [
            {
            label: "Distribución de Ingresos",
            data: yearIngresos,
            backgroundColor: [
                "rgba(75, 192, 192, 0.6)",
                "rgba(255, 99, 132, 0.6)",
                "rgba(54, 162, 235, 0.6)",
                "rgba(255, 206, 86, 0.6)",
                "rgba(153, 102, 255, 0.6)",
                "rgba(255, 159, 64, 0.6)",
            ],
            borderColor: [
                "rgba(75, 192, 192, 1)",
                "rgba(255, 99, 132, 1)",
                "rgba(54, 162, 235, 1)",
                "rgba(255, 206, 86, 1)",
                "rgba(153, 102, 255, 1)",
                "rgba(255, 159, 64, 1)",
            ],
            borderWidth: 1,
            },
        ],
        },
        options: {
        responsive: true,
        plugins: {
            legend: {
            display: false,
            },
            tooltip: {
            callbacks: {
                label: function (tooltipItem) {
                return tooltipItem.raw.toFixed(2) + " €";
                },
            },
            },
            datalabels: {
            color: 'white',
            anchor: 'end',
            align: 'start',
            formatter: function (value, context) {
                return value.toFixed(2) + " €";
            },
            },
        },
        },
    });
}
function insertTotalsRow(table, tabname) {
    // Verificamos que exista <thead> y <tbody>
    const thead = table.querySelector('thead');
    const tbody = table.querySelector('tbody');
    if (!thead || !tbody) {
        console.warn("insertTotalsRow: No se encontró thead o tbody.");
        return;
    }

    // Obtenemos las celdas del encabezado y determinamos qué columnas se deben sumar
    const headerCells = thead.querySelectorAll('th');
    const sumColumns = [];
    
    headerCells.forEach((th, index) => {
        if (th.getAttribute('data-sum') === "true") {
            sumColumns.push(index);
        }
    });


    // Inicializamos un array para almacenar los totales por columna
    const totals = new Array(headerCells.length).fill(0);

    // Recorremos cada fila del tbody para sumar los valores
    const tbodyRows = tbody.querySelectorAll('tr');
    tbodyRows.forEach((row, rowIndex) => {
        const cells = row.querySelectorAll('td');

        sumColumns.forEach(colIndex => {
            if (cells[colIndex]) {
                const rawText = cells[colIndex].innerText.trim();
                const cellValue = parseEuroValue(rawText);
                totals[colIndex] += cellValue;
            } else {
                console.warn(`Fila ${rowIndex + 1}: No se encontró celda en la columna ${colIndex}`);
            }
        });
    });


    // Creamos la fila de Totales
    const totalsRow = document.createElement('tr');
    totalsRow.classList.add('table-warning'); // Color amarillo para destacar

    headerCells.forEach((th, index) => {
        const td = document.createElement('td');
        if (index === 0) {
            if (tabname !== '') {
                td.innerText = 'Totales ' + tabname;
                td.style.width = '120px';
            } else {
                td.innerText = 'Totales';
            }
            td.style.fontWeight = "bold";
            
        } else if (sumColumns.includes(index)) {
            td.innerText = totals[index].toFixed(2) + " €";
            td.style.fontWeight = "bold";
        } else {
            td.innerText = '';
        }
        totalsRow.appendChild(td);
    });

    // Insertamos la fila de Totales en el <thead> antes del encabezado original
    const firstHeaderRow = thead.querySelector('tr'); // Primera fila del thead
    thead.insertBefore(totalsRow, firstHeaderRow);
}

// Función auxiliar para convertir valores en euros a números
function parseEuroValue(text) {
    if (!text) return 0;
    const value = parseFloat(text.replace(/[^0-9.-]+/g, "")) || 0;
    return value;
}
$(document).ready(function () {
    setTimeout(() => {
        // Agregar data-sum="true" solo a las columnas con la clase "sum-column"
        document.querySelectorAll('thead th.sum-column').forEach(th => {
            th.setAttribute('data-sum', 'true');
        });

        document.querySelectorAll('table[data-toggle="table"]').forEach(table => {
            const year = table.closest('.tab-pane')?.id.replace('content-', '');
            insertTotalsRow(table, (!isNaN(year) ? year : ''));
         });
    }, 1000);
});
