function printInvoice() {
    var content = document.getElementById("main_card").innerHTML;
    var originalBody = document.body.innerHTML;
    document.body.innerHTML = content;
    window.print();
    document.body.innerHTML = originalBody;
}

function printReceipt() {
    const originalContent = $('body').html();
    divContent = $('#' + "payment_receipt").clone();
    divContent.removeClass('d-none');
    $('head').append('<style media="print">@page { margin: 0; }</style>');
    $('body').empty().html(divContent);
    window.print();
    $('head style:last').remove();
    $('body').html(originalContent);
}

function sendInvoiceUnpaidNotice(invoice_id) {
    csrf_token =
        $.ajax({
            type: 'POST',
            url: '/gnb/invoices/send_unpaid_notice/',
            data: {
                invoice_id: invoice_id
            },
            success: function (response) {
                var bodyContent = '<div class="alert alert-success " role="alert">\
            Aviso enviado correctamente.\
            Un administrador generará una factura rectificativa para esta factura y la presentará el siguiente trimestre.</div>\
            ';
                var title = 'Aviso de impago';
                var footerContent = `
            <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cerrar</button>
            `;
                createDynamicModal(title, bodyContent, footerContent);
            },
            error: function (error) {
                alert("Hubo un error al enviar el aviso. Ponte en contacto con un administrador.");
                console.log(error);
            }
        });
}

function showDIR3Modal(partner_id, invoice_id) {
    var bodyContent = 'El cliente para el que quieres solicitar la factura electrónica no tiene códigos DIR3 asignados. Por favor, escribelos para que podamos añadirlos en su ficha y generar la factura electrónica:\
    <form method="post" id="dir3_form" class="bodycontent mt-3">\
    <div class="form-floating mb-2">\
        <input type="text" class="form-control" id="inputOficinaContalbe" placeholder="L01020693" required>\
        <label for="inputOficinaContalbe">Código de la Oficina Contable</label>\
    </div>\
    <div class="form-floating mb-2">\
        <input type="text" class="form-control" id="inputOrganoGestor" placeholder="L01020693" required>\
        <label for="inputOrganoGestor">Código del Órgano Gestor</label>\
    </div>\
    <div class="form-floating mb-2">\
        <input type="text" class="form-control" id="inputUnidadTramitacion" placeholder="L01020693" required>\
        <label for="inputUnidadTramitacion">Código de la Unidad de Tramitación</label>\
    </div>\
    <div class="alert alert-warning " role="alert">\
        Ejemplo: L0102XXXX<br>\
        <a target="_blank" href="https://face.gob.es/es/directorio/administraciones">Busca los códigos DIR3 de tu cliente aquí.</a>\
    </div>\
    </form>\
    ';
    var title = 'Códigos DIR3 del cliente vía FACe';
    var footerContent = `
    <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cancelar</button>
    <button type="button" class="btn btn-primary" onclick="sendDIR3Data('${partner_id}','${invoice_id}')">Enviar datos</button>
    `;
    createDynamicModal(title, bodyContent, footerContent);
}

function sendDIR3Data(partner_id, invoice_id) {
    formValid = validateForm('dir3_form');
    if (!formValid) {
        alert("Por favor, complete todos los campos requeridos.");
    }
    else {
        var oficinaContable = $('#inputOficinaContalbe').val();
        var organoGestor = $('#inputOrganoGestor').val();
        var unidadTramitacion = $('#inputUnidadTramitacion').val();

        $.ajax({
            type: 'POST',
            url: '/gnb/invoices/send_facturae_request/' + invoice_id + '/',
            data: {
                oficina_contable: oficinaContable,
                organo_gestor: organoGestor,
                unidad_tramitacion: unidadTramitacion,
                partner_id: partner_id,
            },
            success: function (response) {
                closeConfirmModal();
                showDataSentModal();
            },
            error: function (error) {
                alert("Hubo un error al enviar los datos. Ponte en contacto con un administrador.");
                console.log(error);
            }
        });
    }
}

function showDataSentModal() {
    var bodyContent = '<div class="alert alert-success " role="alert">\
        Los datos se han enviado correctamente.\
        Un administrador los añadirá al cliente y generará tu factura electrónica</div>\
    ';
    var title = 'Códigos DIR3 del cliente a facturar';
    var footerContent = `
    <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cerrar</button>
    `;
    createDynamicModal(title, bodyContent, footerContent);
}

function sendFacturaeRequest(invoice_id) {
    $.ajax({
        type: 'POST',
        url: '/gnb/invoices/send_facturae_request/' + invoice_id + '/',
        data: {
            complete_client_data: true
        },
        success: function (response) {
            closeConfirmModal();
            var bodyContent = '<div class="alert alert-success " role="alert">\
            Petición enviada correctamente.\
            Un administrador generará tu factura electrónica</div>\
            ';
            var title = 'Códigos DIR3 del cliente a facturar';
            var footerContent = `
            <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cerrar</button>
            `;
            createDynamicModal(title, bodyContent, footerContent);
        },
        error: function (error) {
            alert("Hubo un error al enviar los datos. Ponte en contacto con un administrador.");
            console.log(error);
        }
    });
}

$('#finish_edit_button').click(function() {
    var url = $(this).data('url');
    window.location.href = url;
});


$('.edit_invoice_line_button').click(function() {
    var $button = $(this);
    var $row = $button.closest('tr');
    var $inputs = $row.find('input, select');
    var $invoice_id = $('#invoice_id').val();
    var form_data = {};
    form_data['invoice_id'] = $invoice_id;

    if ($(this).hasClass('btn-primary')) { 
        $(this).removeClass('btn-primary').addClass('btn-success');
        $inputs.prop('disabled', false);
    } else {
        $(this).removeClass('btn-success').addClass('btn-primary');
        $inputs.prop('disabled', true);

        $inputs.each(function() {
            var $element = $(this);
            var name = $element.attr('name');
            var value = $element.val();
            form_data[name] = value;
        });

        $.ajax({
            type: 'POST',
            url: '/odoo/invoice/edit_line/',
            data: form_data,
            success: function (response) {
                location.reload();
            },
            error: function (error) {
                alert("Hubo un error al enviar los datos. Ponte en contacto con un administrador.");
                console.log(error);
            }
        });
    }
});

$('#edit_invoice_client_button').click(function() {
    var $client_data = $("#invoice_client_data");
    var $old_client_data = $("#old_invoice_client_data");
    var $inputs = $client_data.find('input[type="text"], input[type="number"]');
    var $old_inputs = $old_client_data.find('input[type="hidden"]');
    var form_data = {};
    var old_form_data = {};

    if ($(this).hasClass('btn-primary')) { 
        $(this).removeClass('btn-primary').addClass('btn-success');
        $inputs.prop('disabled', false);
    } else {
        $(this).removeClass('btn-success').addClass('btn-primary');
        $inputs.prop('disabled', true);

        $inputs.each(function() {
            var $element = $(this);
            var name = $element.attr('name');
            var value = $element.val();
            form_data[name] = value;
        });

        $old_inputs.each(function() {
            var $element = $(this);
            var name = $element.attr('name');
            var value = $element.val();
            old_form_data[name] = value;
        });

        if (JSON.stringify(form_data) === JSON.stringify(old_form_data)) {
            return;
        }

        form_data["client_id"] = $("#client_id").val();

        $.ajax({
            type: 'POST',
            url: '/odoo/invoice/edit_client/',
            data: form_data,
            success: function (response) {
                location.reload();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseJSON.message);
                console.log(error.responseText);
            }
        });
    }
});