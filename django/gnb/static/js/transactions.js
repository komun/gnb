function openConfirmPaymentModal() {
  formValid = validateForm('new_account_move');
  if (!formValid) {
    alert("Por favor, complete todos los campos requeridos.");
  }
  else {
    formValid = validatePaymentData();
    if (formValid) {
      var $fee = 0;
      if ($('input[name="destination_type"]:checked').val() === 'internal_account') {
        var $to_account = $('#to_account_internal').find('option:selected').text();
      }

      else if ($('input[name="destination_type"]:checked').val() === 'external_account') {
        var $to_account = $('#iban_account').val();
      }
      else if ($('input[name="destination_type"]:checked').val() === 'contributions') {
        var $to_account = 'Cuenta aportaciones a la cooperativa';
      }
      else if ($('input[name="destination_type"]:checked').val() === 'atm_withdrawal') {
        var $to_account = "Retirada en cajeros con soporte DIMO";
        $fee = 2;
      }

      var bodyContent = '¿Está seguro de querer realizar la siguiente transferencia?<br><b>Origen</b>: ' + $('#from_account').val() + '<br><b>Destino</b>: ' + $to_account + '<br><b>Cantidad</b>: ' + $('#transfer_amount').val() + ' €<br><b>Referencia</b>: ' + $('#transfer_ref').val();
      if ($fee > 0) { bodyContent += '<br><b>Comisión</b>: +' + $fee + ' €'; }
      var title = 'Confirmar Transferencia';
      var footerContent = `
          <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cancelar</button>
          <button type="button" class="btn btn-primary" onclick="sendPaymentForm()">Confirmar</button>
        `;
      createDynamicModal(title, bodyContent, footerContent);
    }
  }
}

function info_dimo() {

  var bodyContent = `Recibirás un correo electrónico a tu usuario de GNB con un número de teléfono y un código para poder retirar la cantidad de dinero sin identificación y en cualquier cajero de la red del <b>grupo Caja Rural</b> que soporte la retirada sin tarjeta(sistema DIMO).
  <br><br><small>* Notas: Este servicio tiene una comisión de 2 €. Esta petición puede tardar entre 1 o 2 días dependiendo de la disponibilidad de las administradoras bancarias.</small>`;
  var title = 'Retirada sin tarjeta en más de 11,000 cajeros';
  var footerContent = `<button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cerrar</button>`;
  createDynamicModal(title, bodyContent, footerContent);

}



function validatePaymentData() {
  var account_available = parseFloat($('#available_balance').val());
  var amount_input = $('#transfer_amount')
  var recommendedExtraContributions = $('#recommended-extra-contributions').val();
  var transfer_amount = parseFloat(amount_input.val());
  var valid = true;
  if (isNaN(account_available) || isNaN(transfer_amount) || transfer_amount <= 0) {
    alert("Por favor, ingrese una cantidad válida.");
    amount_input.addClass("is-invalid");
    valid = false;
  }
  $fee = 0;
  if ($('input[name="destination_type"]:checked').val() === 'atm_withdrawal') {
    $fee = 2;
  }
  if (transfer_amount + $fee > account_available) {
    alert("La cantidad de transferencia no debe exceder el saldo disponible.");
    amount_input.addClass("is-invalid");
    valid = false;
  }
  if ($('input[name="destination_type"]:checked').val() === 'internal_account') {
    $from_account_val = $('#from_account option:selected').val();
    $to_account_val = $('#to_account_internal option:selected').val();
    if ($from_account_val === $to_account_val) {
      $('#to_account_internal').next('.dropdown-toggle').addClass("is-invalid");
      $('#from_account').next('.dropdown-toggle').addClass("is-invalid");
      alert("Origen y destino no pueden ser el mismo");
      valid = false;
    }
  } else if ($('input[name="destination_type"]:checked').val() === 'external_account') {
    $iban_account = $('#iban_account')
    $to_account_beneficiary = $('#to_account_beneficiary')
    if ($iban_account.val() === '') {
      $iban_account.addClass("is-invalid");
      alert("Debes añadir una cuenta SEPA destinataria para la transferencia");
      valid = false;
    }
    if ($to_account_beneficiary.val() === '') {
      $to_account_beneficiary.addClass("is-invalid");
      alert("Debes añadir un/a beneficiario/a");
      valid = false;
    }
  }


  return valid;
}

function resetFormFields() {
  $('#new_account_move')[0].reset();

  // Reset specific fields that might need custom handling
  $('#to_account_internal').val(null).selectpicker('refresh'); // Reset the selectpicker
  $('#iban_account').val('');
  $('#to_account_beneficiary').val('');
  $('#transfer_amount').val('');
  $('#transfer_ref').val('');

  // Reset radio buttons to default selection
  $('#internal_account').prop('checked', true);
  $('#external_account, #contributions, #atm_withdrawal').prop('checked', false);

  // If there's a need to uncheck checkboxes
  $('#avoid_notify').prop('checked', false);
}

function sendPaymentForm() {
  formValid = validateForm('new_account_move');
  if (!formValid) {
    alert("Por favor, complete todos los campos requeridos.");
  }
  else {
    formValid = validatePaymentData();
    if (formValid) {
      var formValues = {};
      var formData = new FormData($('#new_account_move')[0]); // Create a FormData object with the form data
      var user_id = $('#user_id').val();
      closeConfirmModal();
      showLoadingScreen();
      $.ajax({
        url: '/odoo/account_move/', // Specify the URL for your Django view
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          // Handle the response from Django
          if (response.object_id === null) {
            // Si invoice_id es nulo, muestra el mensaje como un error
            alert('Error found: ' + response.message); // Puedes reemplazar esto con tu propia lógica para mostrar el mensaje de error.
          } else {
            resetFormFields();
            //hideLoadingScreen();
            alert("Transferencia registrada con éxito. Refrescando historial.")
            //document.body.classList.add('screen-disabled');

            location.reload();

          }
        },
        error: function (xhr, status, error) {
          // Handle errors
          console.log('Error:', error);
          alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend 
          hideLoadingScreen();
        }
      });
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {

  function pay_contribution(amount) {
    $('#contributions').prop('checked', true).trigger('change');
    $('#transfer_ref').val('Aportación.');
    $('#transfer_amount').val(amount);
    $('#transactions-tab').click();

    // Refresh the form state to apply the changes
    toggleDestinationAccountState();
    openConfirmPaymentModal();
  }

  function toggleDestinationAccountState() {
    if ($('#internal_account').is(':checked')) {
      $('#to_account_internal').prop('disabled', false).selectpicker('refresh');
      $('#iban_account').prop('disabled', true).val('');
      $('#to_account_beneficiary').prop('disabled', true).val('');
      $('#transfer_ref').prop('disabled', false);
    } else if ($('#external_account').is(':checked')) {
      $('#to_account_internal').prop('disabled', true).selectpicker('refresh');
      $('#to_account_beneficiary').prop('disabled', false);
      $('#iban_account').prop('disabled', false);
      $('#transfer_ref').prop('disabled', false);
    } else if ($('#contributions').is(':checked')) {
      $('#iban_account').prop('disabled', true).val('');
      $('#to_account_internal').prop('disabled', true).selectpicker('refresh');
      $('#to_account_beneficiary').prop('disabled', true).val('');
      $('#transfer_ref').prop('disabled', false).val('Aportación.');
      $('#transfer_amount').prop('disabled', false);
    } else if ($('#atm_withdrawal').is(':checked')) {
      $('#iban_account').prop('disabled', true).val('');
      $('#to_account_internal').prop('disabled', true).selectpicker('refresh');
      $('#to_account_beneficiary').prop('disabled', true).val('');
      $('#transfer_ref').prop('disabled', true).val('---');
    }
  }

  // Ensure the function is globally available
  window.pay_contribution = pay_contribution;

  $(document).ready(function () {
    toggleDestinationAccountState();

    $('input[name="destination_type"]').change(function () {
      toggleDestinationAccountState();
    });
  });


});


var transactionsTableRows = document.querySelectorAll('#table-transactions tbody tr');

transactionsTableRows.forEach(function (row) {
  var deuda = parseFloat(row.querySelector('td:nth-child(4)').textContent.trim().replace('€', ''));
  var estado = row.querySelector('td:nth-child(6)').textContent.trim(); // Cambiado de 5 a 6
  var estadoCell = row.querySelector('td:nth-child(6)'); // Cambiado de 5 a 6

  if (estado === 'posted') {
    row.classList.add('table-success');
    estadoCell.textContent = 'Enviada';
  } else if (estado === 'cancel') {
    row.classList.add('table-danger');
    estadoCell.textContent = 'Cancelada';
  }

  if (deuda < 0) {
    row.classList.add('table-warning');
  }
});
var invoicesTableRows = document.querySelectorAll('#table-invoices tbody tr');
invoicesTableRows.forEach(function (row) {
  var deuda = parseFloat(row.querySelector('td:nth-child(4)').textContent.trim().replace('€', ''));
  var estado = row.querySelector('td:nth-child(6)').textContent.trim(); // Cambiado de 5 a 6
  var estadoCell = row.querySelector('td:nth-child(6)'); // Cambiado de 5 a 6

  if (estado === 'posted') {
    row.classList.add('table-success');
    estadoCell.textContent = 'Enviada';
  } else if (estado === 'cancel') {
    row.classList.add('table-danger');
    estadoCell.textContent = 'Cancelada';
  }

  if (deuda < 0) {
    row.classList.add('table-warning');
  }
});


function validateAndSendPayment(currentUrl, form) {
  sendGeneralForm('/odoo/cash_account_move/', currentUrl, form)
}

function addCashAccountTx(id, name) {
  var bodyContent = `<form id="form_cash">
      <input type="hidden" name="cash_account_id" value="`+ id + `">
      <label for="tx_direction" class="form-label">Tipo de movimiento:</label>
        <select name="tx_direction"><option value="in">Entrada</option><option value="out">Salida</option></select>
        <br><label for="address" class="form-label">Referencia:</label>
        <input type="text" class="form-control" name="tx_reference" value=""
          required>
        <br><label for="tx_amount" class="form-label">Cantidad</label>
        <input type="number" class="form-control" name="tx_amount"  style="width: 200px;"
        required min="0" step="0.01" pattern="\d+(\.\d{1,2})?" >
        <div class="invalid-feedback">
          Cantidad requerida
        </div>
                        </form>`;
  var title = 'Transferencia en ' + name;
  var footerContent = `
        <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="validateAndSendPayment('','form_cash')">Confirmar</button>
      `;
  createDynamicModal(title, bodyContent, footerContent);
}