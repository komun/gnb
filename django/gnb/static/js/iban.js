$(document).ready(function () {
    $('#iban_account').on('blur', function () {
        var ibanCode = $(this).val().trim().toUpperCase().replace(/\s+/g, '');
        var resultSpan = $('#iban-validation-result');
        var ibanRegex = /^[A-Z0-9]+$/;

        if (ibanCode) {
            if (!ibanRegex.test(ibanCode)) {
                resultSpan.html('<span style="color: red;">&#10007; IBAN contiene caracteres no válidos</span>');
                return;
            }

            $.ajax({
                url: '/gnb/validate_iban/' + encodeURIComponent(ibanCode) + '/',
                type: 'GET',
                success: function (response) {
                    if (response.ok) {
                        resultSpan.html('<span style="color: green;">&#10003; IBAN Correcto. Banco: ' + response.bank + '</span>');
                    } else {
                        resultSpan.html('<span style="color: red;">&#10007; IBAN incorrecto o no válido</span>');
                    }
                },
                error: function () {
                    resultSpan.html('<span style="color: red;">&#10007; Error al validar el IBAN</span>');
                }
            });
        } else {
            resultSpan.empty(); // Clear the result if the input is empty
        }
    });
});