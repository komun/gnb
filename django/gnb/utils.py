import datetime
import string
import secrets

CURRENT_YEAR = str(datetime.date.today().year)


def generate_password(length=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = (
        secrets.choice(string.ascii_lowercase)
        + secrets.choice(string.ascii_uppercase)
        + secrets.choice(string.digits)
        + secrets.choice(string.punctuation)
    )
    password += "".join(secrets.choice(characters) for i in range(length - 4))
    password_list = list(password)
    secrets.SystemRandom().shuffle(password_list)
    return "".join(password_list)


def get_user_projects(user, exclude_main=False):
    queryset = user.project_memberships.select_related("project")
    if exclude_main:
        queryset = queryset.exclude(project__is_main_project=True)
    return {
        membership.project.code: {
            "name": membership.project.name,
            "role": membership.role,
            "archived": membership.project.archived,
            "can_invoice": membership.project.can_invoice,
        }
        for membership in queryset
    }


def group_transactions_by_year(transactions):
    """
    Agrupa una lista de transacciones por año basado en la fecha de cada transacción.

    Args:
        transactions (list): Lista de transacciones, donde cada transacción es un diccionario que incluye la clave "date".

    Returns:
        dict: Un diccionario donde las claves son los años (int) y los valores son listas de transacciones correspondientes a ese año.
    """
    transactions_by_year = {}
    for tx in transactions:
        # Asegúrate de que la fecha esté en formato datetime
        tx_date = datetime.datetime.strptime(tx["date"], "%Y-%m-%d")  # Ajusta el formato si es necesario
        year = tx_date.year

        if year not in transactions_by_year:
            transactions_by_year[year] = []

        transactions_by_year[year].append(tx)
    # Years will get sorted descending
    transactions_by_year = dict(sorted(transactions_by_year.items(), reverse=True))

    return transactions_by_year