from colorama import Fore, Back, Style, init
from pprint import pprint
import io

init()

from sepaxml import SepaTransfer
import datetime
import time
import requests
from lxml import html

from django.http import JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, reverse
from django.contrib.auth.decorators import login_required
from django.http import (
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    FileResponse,
)
from django.utils.safestring import mark_safe
from django.core.cache import cache
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.utils import timezone
from gnb.decorators import check_user_access
from django.core.mail import send_mail
from django.conf import settings

import odoo.invoices
import odoo.expenses
import odoo.accounts
import odoo.messages
import gnb.utils as utils
from gnb.models import Project, ProjectMembership, Payment, PaymentStatus, PaymentScope, AccountTax
from gnb.decorators import check_user_access
from django.core.mail import send_mail
from django.conf import settings
from facturae import facturae, utils as facturae_utils
from django_countries import countries

import logging

_logger = logging.getLogger("GNB")

def add_menu_context(request, context, page):
    context["all_users"] = User.objects.all()
    context["all_projects"] = Project.objects.all()
    context["navbar"] = page
    return context


def frontend_view(request):
    if request.user.is_authenticated:

        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
        }
        context = add_menu_context(request, context, "frontview")
        return redirect("my_dashboard_view")
    else:
        context = {"navbar": "home"}
        response = render(request, "registration/login.html", context)

    return response


@login_required
def contact_view(request):
    user = request.user
    if request.method == "POST":
        contact_msg = request.POST.get("contact_msg")
        user_email = user.email
        subject = "GNBAdmin: Formulario de Contacto"
        message = f"""
Hemos recibido tu mensaje vía GNB con el siguiente texto:

{contact_msg}

Email de respuesta: {user_email}
Se responderá lo más brevemente posible."""

        email_to = [user_email] + settings.ADMINS_EMAIL
        try:
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, email_to)
            messages.success(request, "Formulario enviado exitosamente.")
        except Exception as e:
            print(f"An error occurred sending mail: {e}")
            messages.error(request, "El correo electrónico no se pudo enviar.")

        return redirect("contact_view")
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
    }

    context = add_menu_context(request, context, "contact")
    return render(request, "frontend/contact.html", context)


@check_user_access
@login_required
def get_invoices_view(request, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    items = odoo.invoices.get_invoices_by_user(odoo_user_id)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "items": items,
    }
    context = add_menu_context(request, context, "get_invoices")
    return render(request, "frontend/get_invoices.html", context)


@check_user_access
@login_required
def get_expense_sheet_view(request, expense_sheet_id, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    user_is_project_admin = False
    journal_code = odoo.expenses.get_journal_code_from_expense_sheet(expense_sheet_id)
    project_code = None
    if journal_code.startswith("G."):
        project_code = journal_code[2:]

    if project_code:
        p = Project.objects.filter(code=project_code).first()
        if p:
            user_is_project_admin = ProjectMembership.objects.filter(
                user=user, project=p, role="admin"
            ).exists()

    if (
        not user_is_project_admin
        and expense_sheet_id not in odoo.expenses.get_expenses_ids_allowed(odoo_user_id)
        and not user.is_superuser
    ):
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    expense_sheet, move_lines = odoo.expenses.get_expense_sheet(expense_sheet_id)
    if not expense_sheet:
        return HttpResponseNotFound("Expense Sheet id not found.")
    else:
        odoo_model = "hr.expense.sheet"
        comments = odoo.messages.get_messages(expense_sheet_id, odoo_model)
        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "user_id": user.id,
            "comments": comments,
            "odoo_model": odoo_model,
            "object": expense_sheet,
            "move_lines": move_lines,
        }
        context = add_menu_context(request, context, "expense")
        return render(request, "frontend/get_expense.html", context)


@check_user_access
@login_required
def get_invoice_view(request, invoice_id, user, edit=False):
    user_is_project_admin = False
    odoo_user_id = user.odooprofile.odoo_user_id
    invoice, move_lines = odoo.invoices.get_invoice(invoice_id)

    if not invoice:
        return HttpResponseNotFound("Invoice id not found.")

    project_code = odoo.invoices.get_project_from_invoice_name(invoice["name"])
    if project_code:
        p = Project.objects.filter(code=project_code).first()
        if p:
            user_is_project_admin = ProjectMembership.objects.filter(
                user=user, project=p, role="admin"
            ).exists()

    if (
        not user_is_project_admin
        and invoice_id not in odoo.invoices.get_invoices_ids_allowed(odoo_user_id)
        and not user.is_superuser
    ):
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    partner = odoo.invoices.get_partner(invoice["partner_id"][0])
    if partner:
        facturae = partner.get("facturae", False)
    else:
        facturae = False

    odoo_model = "account.move"
    comments = odoo.messages.get_messages(invoice_id, odoo_model)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "partner": partner,
        "facturae": facturae,
        "invoice": invoice,
        "comments": comments,
        "edit": (edit and invoice["state"] == "draft"),
        "odoo_model": odoo_model,
        "move_lines": move_lines,
        "INVOICE_ORG_DETAILS": getattr(settings, "INVOICE_ORG_DETAILS", ""),
        "INVOICE_ORG_DETAILS_BELOW": getattr(settings, "INVOICE_ORG_DETAILS_BELOW", ""),
    }
    context = add_menu_context(request, context, "invoice")
    return render(request, "frontend/get_invoice.html", context)


@check_user_access
@login_required
def get_transaction_view(request, tx_id, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    transaction = odoo.accounts.get_transaction_by_id(tx_id)

    if not transaction or transaction["type"] != "entry":
        return HttpResponseNotFound("Transaction not found.")

    if (
        tx_id not in odoo.invoices.get_invoices_ids_allowed(odoo_user_id)
        and not user.is_superuser
    ):
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    odoo_model = "account.move"
    comments = odoo.messages.get_messages(tx_id, odoo_model)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "transaction": transaction,
        "bank_name": settings.BANK_NAME,
        "organization_name": settings.ORGANIZATION_NAME,
        "comments": comments,
        "odoo_model": odoo_model,
    }
    context = add_menu_context(request, context, "transaction")
    return render(request, "frontend/get_transaction.html", context)


@check_user_access
@login_required
def download_invoice_facturae(request, user, invoice_id):

    invoice, move_lines = odoo.invoices.get_invoice(invoice_id)
    partner = odoo.invoices.get_partner(invoice["partner_id"][0])

    if not partner["state_id"]:
        return HttpResponseNotFound("El cliente no tiene provincia.")
    else:
        province = facturae_utils.get_clean_province(partner["state_id"][1])

    if not invoice:
        return HttpResponseNotFound("Invoice id not found.")

    seller_address = facturae.Address(
        "Matadero, 7", "02260", "Fuentealbilla", "Albacete", "ESP"
    )
    seller = facturae.Contact(
        "Estraperlo S.Coop. de C-LM", "F02546489", "J", seller_address
    )
    iban = "ES5131270001422008980126"

    country_code = facturae_utils.get_country_code(partner["country_id"][1])
    if not country_code:
        return HttpResponseNotFound("El cliente no tiene un país válido.")

    if not partner["zip"]:
        return HttpResponseNotFound("El cliente no tiene un código postal válido.")

    client_address = facturae.Address(
        partner["street"], partner["zip"], partner["city"], province, country_code
    )
    client_administrative_centres = [
        facturae.AdministrativeCentre(
            partner["organo_gestor"], "02", partner["name"], client_address
        ).to_dict(),
        facturae.AdministrativeCentre(
            partner["unidad_tramitadora"], "03", partner["name"], client_address
        ).to_dict(),
        facturae.AdministrativeCentre(
            partner["oficina_contable"], "01", partner["name"], client_address
        ).to_dict(),
    ]
    client = facturae.Contact(
        partner["name"],
        partner["vat"],
        "J",
        client_address,
        client_administrative_centres,
    )

    invoice_lines = []
    for move_line in move_lines:
        # Only use the first tax line
        # TO DO: Add support for multiple tax lines
        if not move_line["tax_ids"] or not move_line["tax_ids"][0]:
            return HttpResponseNotFound(
                "No hay un valor de impuestos válido para algunas líneas de esta factura."
            )
        tax_quantity = facturae_utils.get_taxid_quantity(move_line["tax_ids"][0])
        invoice_lines.append(
            facturae.InvoiceLine(
                move_line["name"],
                move_line["quantity"],
                move_line["price_unit"],
                tax_quantity,
            ).to_dict()
        )

    facturae_invoice = facturae.Invoice(
        invoice["name"],
        "OO",
        invoice["invoice_date"],
        invoice["amount_untaxed"],
        invoice["amount_tax"],
        invoice["amount_total"],
        seller,
        client,
        invoice_lines,
        iban,
    )

    generated_xml = facturae_invoice.generate_xml()
    print(generated_xml)
    # Escribir el XML en un archivo

    invoice_file = io.BytesIO(generated_xml.encode("utf-8"))
    response = FileResponse(invoice_file)

    # Configurar encabezados para forzar la descarga del archivo
    response["Content-Disposition"] = (
        'attachment; filename="factura_' + invoice["name"] + '.xml"'
    )
    response["Content-Type"] = "application/xml"

    return response


@check_user_access
@login_required
@csrf_exempt
def send_facturae_request(request, user, invoice_id):
    if request.method == "POST":
        partner_id = request.POST.get("partner_id")

        if request.POST.get("complete_client_data"):
            subject = "GNBAdmin: Petición FACe"
            message = f"""
El usuario {user} ha solicitado presentar una factura electrónica.

Enlace GNB de la factura: {settings.DEFAULT_DOMAIN}/gnb/invoices/{invoice_id}/
Enlace Odoo: {settings.ODOO_DOMAIN}/web?#id={invoice_id}&action=216&model=account.move&view_type=form
            """
        else:
            oficina_contable = request.POST.get("oficina_contable")
            organo_gestor = request.POST.get("organo_gestor")
            unidad_tramitacion = request.POST.get("unidad_tramitacion")
            subject = "GNBAdmin: Petición FACe + añadir datos DIR3"
            message = f"""
El usuario {user} ha solicitado presentar una factura electrónica.

Enlace GNB de la factura: {settings.DEFAULT_DOMAIN}/gnb/invoices/{invoice_id}/
Enlace Odoo: {settings.ODOO_DOMAIN}/web?#id={invoice_id}&action=216&model=account.move&view_type=form

Códigos DIR3 a añadir en el contacto del cliente:
Enlace Odoo cliente: {settings.ODOO_DOMAIN}/web#cids=1&id={partner_id}&menu_id=116&model=res.partner&view_type=form
Oficina contable: {oficina_contable}
Organo gestor: {organo_gestor}
Unidad tramitación: {unidad_tramitacion}
            """

        try:
            send_mail(
                subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL
            )
            return JsonResponse({"message": "Datos enviados correctamente"})
        except Exception as e:
            print(f"An error occurred sending dir3 mail: {e}")
            return JsonResponse({"error": "El correo electrónico no se pudo enviar"})

    return JsonResponse({"error": "Método no permitido"}, status=405)


@check_user_access
@login_required
@csrf_exempt
def send_invoice_unpaid_notice(request, user):
    if request.method == "POST":
        invoice_id = request.POST.get("invoice_id")
        subject = f"""GNBAdmin: Aviso impago de factura {invoice_id}"""
        message = f"""
El usuario {user} ha avisado de impago en la factura {invoice_id}.

Enlace GNB de la factura: {settings.DEFAULT_DOMAIN}/gnb/invoices/{invoice_id}/
Enlace Odoo: {settings.ODOO_DOMAIN}/web?#id={invoice_id}&action=216&model=account.move&view_type=form
        """

        try:
            send_mail(
                subject, message, settings.DEFAULT_FROM_EMAIL, settings.ADMINS_EMAIL
            )
            return JsonResponse({"message": "Datos enviados correctamente"})
        except Exception as e:
            print(f"An error occurred sending unpaid mail: {e}")
            return JsonResponse({"error": "El correo electrónico no se pudo enviar"})

    return JsonResponse({"error": "Método no permitido"}, status=405)


@check_user_access
@login_required
def new_invoice_view(request, project_code, user):

    # Returning all the project names except the generic EST
    p = Project.objects.filter(code=project_code).first()
    if not p:
        return HttpResponseForbidden("El proyecto no existe.")
    if p.archived:
        return HttpResponseForbidden("Error. Proyecto archivado.")

    pnames = utils.get_user_projects(user, exclude_main=True).keys()
    if not user.is_superuser and project_code not in pnames:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    products = odoo.invoices.get_products([project_code])
    country_list = list(countries)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "project_code": project_code,
        "user_id": user.id,
        "account_taxes": AccountTax.objects.filter(type_tax_use='sell', is_active=True).order_by('order'),
        "countries": country_list,
        "products": products,
    }
    context = add_menu_context(request, context, "new_invoice")
    return render(request, "frontend/new_invoice.html", context)


@check_user_access
@login_required
def new_expense_view(request, project_code, user):
    # Returning all the project names except the generic EST
    odoo_user_id = user.odooprofile.odoo_user_id
    employee_id = odoo.accounts.get_employee_id(odoo_user_id)
    if employee_id is None:
        return HttpResponseNotFound(
            "El usuario no tiene el campo employee_id activado. Contacta a l@s administrador@s."
        )

    p = Project.objects.filter(code=project_code).first()
    if not p:
        return HttpResponseForbidden("El proyecto no existe.")
    if p.archived:
        return HttpResponseForbidden("Error. Proyecto archivado.")
    pnames = utils.get_user_projects(user, exclude_main=True).keys()
    if not user.is_superuser and project_code not in pnames:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "projects": pnames,
        "account_taxes": AccountTax.objects.filter(type_tax_use='purchase', is_active=True).order_by('order'),
        "project_code": project_code,
        "max_expense_date": datetime.date.today().strftime("%Y-%m-%d"),
        "user_id": user.id,
    }
    context = add_menu_context(request, context, "new_expense")
    return render(request, "frontend/new_expense.html", context)


@check_user_access
@login_required
def get_expenses_view(request, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    employee_id = odoo.accounts.get_employee_id(odoo_user_id)
    if employee_id is None:
        return HttpResponseNotFound(
            "El usuario no tiene el campo employee_id activado. Contacta a l@s administrador@s."
        )
    items = odoo.expenses.get_expenses(odoo_user_id)
    if items is False:
        return HttpResponseNotFound(
            "Error al encontrar los gastos del usuario. Contacta a l@s administrador@s."
        )

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "items": items,
    }
    context = add_menu_context(request, context, "get_expenses")
    return render(request, "frontend/get_expenses.html", context)


@login_required
def users_report_view(request, user_id=None):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    total_users = []
    users = User.objects.filter(odooprofile__account_checking__isnull=False)
    for i, user in enumerate(users):
        print(f"Calculando balances de usuario {user.username} ({i}/{len(users)})")
        data_user = {"username": user.username}
        data_user["account_checking"] = odoo.accounts.get_total_balances(
            user.odooprofile.account_checking, "bank"
        )
        data_user["account_social_capital"] = odoo.accounts.get_total_balances(
            user.odooprofile.account_social_capital
        )
        total_users.append(data_user)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "reports",
        "total_users": total_users,
    }
    context = add_menu_context(request, context, "reports")
    return render(request, "frontend/admin/report_users.html", context)

def calculate_for_year(projects, year):
    """
    Calcula los balances de los proyectos para el año proporcionado.
    Si el año es None (caso 'all'), se calculan para todos los proyectos sin especificar el año.
    """
    if year is None:  # Caso "all", no hay año específico
        year_label = "all"
    else:
        year_label = year

    total_projects_year = {'data': [], 'calculation_date': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}  # Guardar la fecha de cálculo
    for i, project in enumerate(projects):
        print(f"Calculando balances de proyecto {project.code} para el año {year_label} ({i + 1}/{len(projects)})")
        # Si el año es None, pasamos None a la función, caso contrario usamos el año específico
        data_project = calculate_project_data(project, year)
        total_projects_year['data'].append(data_project)
    return total_projects_year
        
@login_required
def projects_report_view(request):
    # Obtener parámetros GET
    refresh_cache = request.GET.get('refresh_cache', '0') == '1'  # Refrescar si es '1'
    year = request.GET.get('year')  # Leer parámetro year (puede ser None)

    start_time = time.time()
    total_projects = {}

    # Obtener los proyectos filtrados
    projects = Project.objects.filter(account_checking__isnull=False).exclude(is_main_project=True)
    num_projects = len(projects)

    if refresh_cache:
        # Si se refresca la caché, verificar si se debe recalcular para un solo año o todos
        if year == "all":
            year = None  # Asignamos None cuando el parámetro año es "all"

        else:
            try:
                # Si el año no es "all", intentamos convertirlo a un número entero
                year = int(year)
                if year not in settings.YEARS:
                    raise ValueError(f"El año {year} no es válido.")
            except ValueError:
                return HttpResponseBadRequest("El año proporcionado no es válido.")

        # Cargar datos previos desde caché
        total_projects = cache.get("projects_report_view", {y: {'data': [], 'calculation_date': None} for y in settings.YEARS})

        # Recalcular para un solo año o para todos los años
        if year is not None:  # Si hay un año específico
            total_projects[year] = calculate_for_year(projects, year)

        elif year is None:  # Si year es None (caso "all")
            total_projects["all"] = calculate_for_year(projects, year=None)

        else:  # Si se recalcula para todos los años
            for year in settings.YEARS:
                total_projects[year] = calculate_for_year(projects, year)

        # Guardar en caché con los nuevos datos
        print(f"Setting cached projects_report_view")
        cache.set("projects_report_view", total_projects, None)  # Caché sin expiración

    else:
        # Si no se refresca la caché, intentar obtener los datos
        total_projects = cache.get("projects_report_view", {y: {'data': [], 'calculation_date': None} for y in settings.YEARS})  # Generar estructura vacía si no hay caché

    print(f"Tiempo total de ejecución: {time.time() - start_time} segundos")

    # Preparar contexto y renderizar plantilla
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "reports",
        "user_id": request.user.id,
        "total_projects": total_projects,
    }
    context = add_menu_context(request, context, "reports")
    return render(request, "frontend/report_projects.html", context)


def calculate_project_data(project, year):
    """
    Calcula los datos de un proyecto para un año específico.
    """
    year_transactions = odoo.accounts.get_transactions(project.account_checking, year, just_for_contributions=True)
    data_project = {
        "code": project.code,
        "name": project.name,
        "account_checking": odoo.accounts.get_total_balances(project.account_checking, "bank", year),
        "account_expenses": -1 * odoo.accounts.get_total_balances(project.account_expenses, "normal", year),
        "account_sales": odoo.accounts.get_total_balances(project.account_sales, "normal", year),
        "contributions": odoo.accounts.get_contributions(project.code, year_transactions, year)
    }

    # Obtener balances de facturas y gastos no pagados
    total_unpaid_invoices, total_unpaid_expenses = odoo.accounts.get_project_balances(project.code, f"G.{project.code}", year)
    data_project["total_unpaid_invoices"] = total_unpaid_invoices
    data_project["total_unpaid_expenses"] = total_unpaid_expenses

    # Calcular el total no justificado si ambos valores están presentes
    if None not in [data_project["account_expenses"], data_project["account_sales"]]:
        data_project["total_unjustified"] = (
            data_project["account_sales"]
            - data_project["total_unpaid_invoices"]
            - data_project["account_expenses"]
        )
    else:
        data_project["total_unjustified"] = None

    return data_project


@csrf_exempt
@login_required
def new_cashin_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    if request.method == "POST":
        # Process POST data
        amount = request.POST.get("received_amount")
        unpaid_invoice_id = request.POST.get("unpaid_invoice_id")
        notify_project = (
            True
            if "notify_project" in request.POST
            and request.POST.get("notify_project") == "on"
            else False
        )
        pay_taxes = (
            True
            if "pay_taxes" in request.POST and request.POST.get("pay_taxes") == "on"
            else False
        )

        cashin_date = request.POST.get("cashin_date")
        amount = float(amount)
        from_journal = request.POST.get("from_journal")
        reference = request.POST.get("reference")

        if not unpaid_invoice_id:
            # Cash in without invoice attached
            account_credit = settings.ODOO_ACCOUNT_CLIENTS
            account_debit = odoo.accounts.get_code_from_account_id(
                odoo.accounts.get_account_id_from_journal(from_journal)
            )

            # Create the account move manually
            object_id, message = odoo.accounts.create_bank_account_move(
                account_debit, account_credit, amount, reference, move_date=cashin_date
            )
            if object_id:
                messages.success(request, "Éxito en la operación.")
            else:
                messages.error(request, f"Error en la operación: {message}.")
        else:
            unpaid_invoice_id = int(unpaid_invoice_id)
            invoice = odoo.invoices.get_invoice_by_id(unpaid_invoice_id)
            if not invoice:
                messages.error(request, f"Error encontrando la factura {unpaid_invoice_id}.")
            else:
                # Check if the invoice is already paid
                if invoice["invoice_payment_state"] == "paid":
                    messages.warning(request, f"Error. La factura {invoice['name']} ya está pagada.")
                else:
                    invoice_name = invoice["name"]
                    project_code = invoice_name.split("/")[1]
                    ok, msg = odoo.accounts.create_cashin_account_move(
                        invoice,
                        journal_id=from_journal,
                        move_date=cashin_date,
                        amount=amount,
                        pay_taxes=pay_taxes,
                    )
                    if not ok:
                        messages.error(request, msg)
                    else:
                        paid_invoiced_id = unpaid_invoice_id
                        link = f'<a target="_blank" href="{settings.ODOO_DOMAIN}/web?#id={paid_invoiced_id}&action=216&model=account.move&view_type=form">{paid_invoiced_id}</a>'
                        message = mark_safe(f"Éxito pagando la factura {invoice_name} (id: {link}).")
                        messages.success(request, message)

                        details = {
                            "action_username": request.user.username,
                            "invoice_name": invoice["name"],
                            "amount_untaxed": invoice["amount_untaxed"],
                            "amount_total": amount,
                            "project_code": project_code,
                            "from_journal": from_journal,
                            "pay_taxes": pay_taxes,
                        }
                        # odoo.utils.send_email_new_account_move(details)
                        if notify_project:
                            odoo.utils.send_email_new_cashin(details)
                            messages.success(request, "Éxito notificando por mail a los admins del proyecto.")

    # Fetch unpaid invoices and other context data
    unpaid_invoices = odoo.invoices.get_all_unpaid_invoices()
    all_projects = list(Project.objects.values("code", "name"))
    payment_accounts = odoo.accounts.get_payment_journals()
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "unpaid_invoices": unpaid_invoices,
        "all_projects": all_projects,
        "user_id": request.user.id,
        "payment_accounts": payment_accounts,
        "navbar": "new_cashin",
    }

    context = add_menu_context(request, context, "new_cashin")
    return render(request, "frontend/admin/new_cashin.html", context)

@csrf_exempt
@check_user_access
@login_required
def pending_payments(request, user):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    pending_payments_SEPA = Payment.objects.filter(
        status=PaymentStatus.PENDING,
        payment_scope=PaymentScope.EXTERNAL,
        due_date__lte=timezone.now(),
    ).order_by("-due_date")
    pending_payments_atm = Payment.objects.filter(
        status=PaymentStatus.PENDING,
        payment_scope=PaymentScope.ATM_WITHDRAW,
        due_date__lte=timezone.now(),
    ).order_by("-due_date")
    paid_payments = Payment.objects.exclude(status=PaymentStatus.PENDING).order_by(
        "-due_date"
    )
    context = {
        "pending_payments_SEPA": pending_payments_SEPA,
        "pending_payments_atm": pending_payments_atm,
        "paid_payments": paid_payments,
        "user_id": user.id,
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
    }
    context = add_menu_context(request, context, "pending_payments")

    return render(request, "frontend/admin/pending_payments.html", context)




@login_required
@csrf_exempt
def export_payments(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    if request.method == "GET":
        payment_ids = request.GET.get("payment_ids")
        try:
            # Convert the comma-separated string into a list of integers
            payment_ids = [int(id) for id in payment_ids.split(",")]
        except ValueError:
            raise HttpResponseNotFound("Invalid payment IDs")

        if len(payment_ids):
            config = {
                "name": "ESTRAPERLO",
                "IBAN": "ES5131270001422008980126",
                "BIC": "BCOEESMM127",
                "batch": True,
                # For non-SEPA transfers, set "domestic" to True, necessary e.g. for CH/LI
                "currency": "EUR",  # ISO 4217
                "address": {
                    # The address and all of its fields are optional but in some countries they are required
                    "address_type": "ADDR",  # valid: ADDR, PBOX, HOME, BIZZ, MLTO, DLVY
                    # "department": "Head Office",
                    # "subdepartment": None,
                    "street_name": "C/Matadero.",
                    "building_number": "7",
                    "postcode": "02260",
                    "town": "Fuentealbilla.",
                    "country": "ES",
                    "country_subdivision": None,
                    # "lines": ["Line 1", "Line 2"],
                },
            }
            sepa = SepaTransfer(config, clean=True)
            payments = Payment.objects.filter(id__in=payment_ids)
            for p in payments:
                # Convertir a centavos manteniendo los decimales
                amount_in_cents = round(float(p.amount) * 100)  # Usar round() y float() para mantener precisión
                
                payment = {
                    "name": p.to_account,
                    "IBAN": p.iban_account.replace(" ", "").upper(),
                    # "BIC": "BANKNL2A",
                    "amount": amount_in_cents,  # in cents of euro
                    # "type": "RCUR",  # FRST,RCUR,OOFF,FNAL
                    "execution_date": datetime.date.today(),
                    # "mandate_id": "1234",
                    "description": p.reference,
                    # "endtoend_id": str(uuid.uuid1()).replace("-", ""),  # autogenerated if obmitted
                }
                sepa.add_payment(payment)

            xml = sepa.export(validate=True)
            print("xml", xml)
            response = HttpResponse(xml, content_type="application/xml")
            response["Content-Disposition"] = 'attachment; filename="payments.xml"'
            return response
    else:
        # Maneja solicitudes no POST
        return JsonResponse({"status": "error", "message": "Invalid request method"})


@login_required
@csrf_exempt
def notify_about_tx_details(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    if request.method == "POST":
        payment_id = request.POST.get("payment_id")
        action = request.POST.get("action")
        from_account = request.POST.get("from_account")
        email_text = request.POST.get("email_text")
        payment = Payment.objects.filter(id=payment_id).first()
        details = {
            "action": action,
            "from_account": from_account,
            "email_text": f"Detalles proporcionados por administración:\n\n{email_text}",
            "payment": {
                "amount": str(payment.amount),
                "reference": payment.reference,
                "to_account": payment.to_account_str(),
                "scope": payment.payment_scope,
            },
        }
        print("details", details)
        odoo.utils.send_email_about_tx_details_to_recipients(details)
        payment.due_date = timezone.now()
        if action == "accept":
            payment.paid = True
            payment.status = PaymentStatus.PAID
        else:
            payment.paid = False
            payment.status = PaymentStatus.REJECTED

        payment.save()
        return JsonResponse({"status": "success", "message": "Notify successful"})
    else:
        # Maneja solicitudes no POST
        return JsonResponse({"status": "error", "message": "Invalid request method"})


@login_required
@csrf_exempt
def mark_as_paid_payments(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    if request.method == "POST":
        payment_ids = request.POST.getlist("payment_ids[]")
        payment_ids = [int(payment_id) for payment_id in payment_ids]
        print("payment_ids", payment_ids)
        payments = Payment.objects.filter(id__in=payment_ids)
        payments.update(paid=True, status=PaymentStatus.PAID, due_date=timezone.now())
        for payment in payments:
            details = {
                "action": "accept",
                "from_account": payment.from_account,
                "email_text": "Tu transferencia bancaria ha sido efectuada en el banco.",
                "payment": {
                    "amount": str(payment.amount),
                    "reference": payment.reference,
                    "to_account": payment.to_account_str(),
                    "scope": payment.payment_scope,
                },
            }
            print("details", details)
            odoo.utils.send_email_about_tx_details_to_recipients(details)
        return JsonResponse({"status": "success", "message": "Mark as paid successful"})
    else:
        # Maneja solicitudes no POST
        return JsonResponse({"status": "error", "message": "Invalid request method"})


@csrf_exempt
@login_required
def new_project_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
        "navbar": "new_project",
    }
    if request.method == "POST":
        # Procesa los datos POST si la solicitud es de tipo POST
        project_code = request.POST.get("project_code")
        project_code = project_code[0:3]  # Get only the first 3 characters
        project_name = request.POST.get("project_name")
        project_is_member = (
            True
            if "project_is_member" in request.POST
            and request.POST.get("project_is_member") == "on"
            else False
        )
        project, created = Project.objects.get_or_create(
            code=project_code, defaults={"name": project_name}
        )
        details = odoo.accounts.create_project(
            project, project_is_member=project_is_member
        )
        details["created"] = created
        details["name"] = project_name
        details["code"] = project_code
        print("Project created or updated:")

        email_details = {
            "action_username": request.user.username,
            "project": project,
            "update_details": details,
        }
        odoo.utils.send_email_new_group_created(email_details)
        context["update_details"] = details
        context["project"] = project

    context = add_menu_context(request, context, "new_project")
    return render(request, "frontend/admin/new_project.html", context)


@csrf_exempt
@login_required
def new_user_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
        "navbar": "new_user",
    }

    if request.method == "POST":
        user_email = request.POST.get("user_email")
        user_firstname = request.POST.get("user_firstname")
        user_lastname = request.POST.get("user_lastname")
        user_is_member = (
            True
            if "user_is_member" in request.POST
            and request.POST.get("user_is_member") == "on"
            else False
        )

        user, created = User.objects.get_or_create(
            username=user_email,
            defaults={
                "email": user_email,
                "first_name": user_firstname,
                "last_name": user_lastname,
            },
        )
        if created:
            password = utils.generate_password()
            print(f"Registrando usuario GNB para {user_email} con password: {password}")
            context["password"] = password
            user.set_password(password)
        user.is_active = True
        user.save()

        details = odoo.accounts.create_user(user, user_is_member=user_is_member)

        details["django_created"] = created
        details["email"] = user.email
        details["first_name"] = user.first_name
        details["last_name"] = user.last_name

        context["update_details"] = details
        context["object"] = user
        print("User created or updated:")
        pprint(context)

        email_details = {
            "action_username": request.user.username,
            "user": user,
            "update_details": details,
        }
        odoo.utils.send_email_new_user_created(email_details)

    context = add_menu_context(request, context, "new_user")
    return render(request, "frontend/admin/new_user.html", context)


def get_all_projects_balances(user):
    total_projects = {}
    if user:
        projects = user.project_set.all()
    else:
        projects = Project.objects.all()

    for project in projects:
        cash_accounts = get_cash_accounts(project)
        project_total, _ = get_project_total(
            project, cash_accounts, show_contributions=False
        )
        if user:
            m = ProjectMembership.objects.filter(user=user, project=project).first()
            m = project_total["membership_role"] = m.role
        total_projects[project.code] = project_total

    return total_projects


@login_required
def reports_balance_view(request):
    project = Project.objects.filter(is_main_project=True).first()

    is_member = ProjectMembership.objects.filter(user=request.user, project=project).exists()

    if not is_member and not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    cash_accounts = get_cash_accounts(project)
    cash_account_codes = cash_accounts.keys()
    accounts = odoo.accounts.get_liquidity_accounts()
    balances = {}
    total_balance = main_project_available = 0
    balance_account_checking = odoo.accounts.get_total_balances(
        project.account_checking, "bank"
    )
    print("Suming balances for Bank Accounts")
    for account in accounts:
        code = account["code"]
        balance = odoo.accounts.get_total_balances(code, "bank")
        if code not in cash_account_codes:
            total_balance += balance
            main_project_available += balance
            balances[code] = balance
    print("Suming balances for Cash Accounts")
    for code in cash_account_codes:
        balance = odoo.accounts.get_total_balances(code, "bank")
        main_project_available += balance

    cash_accounts[0] = {"name": "Cuenta principal", "balance": total_balance}
    project_total = {
        "available_balance": main_project_available,
        "account_checking": balance_account_checking,
    }

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "project_view",
        "user_id": request.user.id,
        "project_total": project_total,
        "project": project,
        "cash_accounts": cash_accounts,
    }
    return render(request, "frontend/report_balance.html", context)

def get_project_total(project, cash_accounts={}, show_contributions=True):
    transactions = []
    print("Getting balances for project", project.code)
    project_total = {}
    project_total["account_checking"] = odoo.accounts.get_total_balances(
        project.account_checking, "bank", year=None
    )
    project_total["account_expenses"] = -1 * odoo.accounts.get_total_balances(
        project.account_expenses, "normal", year=None
    )
    # This is all the sales (including the paid and to be paid invoices)
    project_total["account_sales"] = odoo.accounts.get_total_balances(
        project.account_sales, "normal", year=None
    )
    project_total["account_loans"] = odoo.accounts.get_total_balances(
        project.account_loans
    )
    print("Getting balance unpaid expenses and invoices...")
    total_unpaid_invoices, total_unpaid_expenses = odoo.accounts.get_project_balances(
        project.code, f"G.{project.code}"
    )
    print(f"total_unpaid_expenses = {total_unpaid_expenses} €")
    project_total["total_unpaid_invoices"] = total_unpaid_invoices
    project_total["total_paid_invoices"] = (
        project_total["account_sales"] - project_total["total_unpaid_invoices"]
    )
    project_total["total_unpaid_expenses"] = total_unpaid_expenses
    project_total["name"] = project.name

    if None not in [
        project_total["account_checking"],
        project_total["account_expenses"],
        project_total["account_sales"],
    ]:

        project_total["unjustified_total"] = (
            project_total["total_paid_invoices"] - project_total["account_expenses"]
        )
        project_total["needed_expenses"] = (
            project_total["account_sales"] - project_total["account_expenses"]
        )
        if project_total["needed_expenses"] < 0:
            project_total["needed_expenses"] = 0

        print(
            f'unjustified_total = {project_total["unjustified_total"]}€ =  total_paid_invoices({project_total["total_paid_invoices"]}) - account_expenses ({project_total["account_expenses"]})'
        )
        project_total["frozen_total"] = (
            project_total["unjustified_total"]
            if project_total["unjustified_total"] > 0
            else 0
        )

        all_transactions = odoo.accounts.get_transactions(project.account_checking)
        transactions = {"checking": utils.group_transactions_by_year(all_transactions)}
        if show_contributions:
            project_total["why_contributions_url"] = settings.WHY_DONATIONS_URL
            total_sales = odoo.accounts.get_total_balances(project.account_sales, "normal")
            if project.percentage_recomended_income_donate:
                project_total["percentage_recomended_contribution_percentage"] = project.percentage_recomended_income_donate
                recommended_contributions = (
                    total_sales  #- project_total["total_unpaid_invoices"]
                ) * (project.percentage_recomended_income_donate / 100)
            else:
                recommended_contributions = 0
            
  
            project_total["contributions"] = odoo.accounts.get_contributions(project.code, all_transactions)
            project_total["percentage_contributions"] = (project_total["contributions"] / total_sales) * 100 if total_sales else None
            difference = recommended_contributions - project_total["contributions"]
            if difference >= 0:
                project_total["recommended_extra_contributions"] = difference
            else:
                project_total["recommended_extra_contributions"] = 0

            project_total["why_contributions_url"] = settings.WHY_DONATIONS_URL

            if recommended_contributions:
                if project_total["contributions"] > 0:
                    project_total["contribution_goal_completion"] = (
                        project_total["contributions"] / recommended_contributions
                    ) * 100
                else:
                    project_total["contribution_goal_completion"] = 0
            else:
                project_total["contribution_goal_completion"] = None

            # Si no hay contribuciones extra recomendadas y hay contribuciones, se establece el goal a 100%
            if not project_total["recommended_extra_contributions"] and project_total["contributions"] > 0:
                project_total["contribution_goal_completion"] = 100
              
        else:
            project_total["recommended_extra_contributions"] = "Ver detalles"
            project_total["contributions"] = "Ver detalles"

        if project_total["unjustified_total"] > 0:
            print('if project_total["unjustified_total"] > 0:')
            project_total["available_balance"] = (
                project_total["account_checking"] - project_total["unjustified_total"]
            )
            print(
                f'project_total["available_balance"] ({project_total["available_balance"]} = project_total["account_checking"]({project_total["account_checking"]}) - project_total["unjustified_total"]({project_total["unjustified_total"]})'
            )
            if project_total["available_balance"] < 0:
                project_total["available_balance"] = 0

        else:
            project_total["available_balance"] = project_total["account_checking"]

        for account_code, v in cash_accounts.items():
            project_total["available_balance"] += v["balance"]
            transactions[v["name"]] = odoo.accounts.get_transactions(account_code)

    else:
        project_total["unjustified_total"] = None
        project_total["available_balance"] = None
        project_total["recommended_extra_contributions"] = None
        project_total["contributions"] = None

    return project_total, transactions


@check_user_access
@login_required
def account_view(request, account_username, user):
    account_user = User.objects.get(username=account_username)
    total_personal = {}
    total_personal["name"] = account_user.first_name
    total_personal["account_checking"] = odoo.accounts.get_total_balances(
        account_user.odooprofile.account_checking, "bank"
    )
    total_personal["account_loans"] = odoo.accounts.get_total_balances(
        account_user.odooprofile.account_loans
    )
    account_checking = account_user.odooprofile.account_checking
    print("#Dashboard: Getting user txs")

    # Get the transactions from that account
    all_transactions = odoo.accounts.get_transactions(account_checking)
    transactions = {"checking": utils.group_transactions_by_year(all_transactions)}

    print("#Dashboard: Getting account_user balances")
    reimbursable_social_capital = (
        0
        if not account_user.odooprofile.reimbursable_social_capital
        else account_user.odooprofile.reimbursable_social_capital
    )
    account_social_capital = odoo.accounts.get_total_balances(
        account_user.odooprofile.account_social_capital
    )
    account_social_capital = 0 if not account_social_capital else account_social_capital
    total_personal["account_social_capital"] = (
        account_social_capital + reimbursable_social_capital
    )
    project_data = Project.objects.all().values("code", "name")
    project_dict = {item["code"]: item["name"] for item in project_data}
    user_data = User.objects.all().values("id", "email", "first_name", "last_name")
    user_dict = {
        item["email"]: item["first_name"] + item["last_name"] for item in user_data
    }
    all_accounts = {**user_dict, **project_dict}

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "my_dashboard",
        "user_id": account_user.id,
        "total_personal": total_personal,
        "from_account": account_user.email,
        "available_balance": total_personal["account_checking"],
        "user": account_user,
        "user_email": account_user.email,
        "all_accounts": all_accounts,  # For transactions
        "transactions": transactions,
    }
    context = add_menu_context(request, context, "account_view")
    return render(request, "frontend/account_view.html", context)

@check_user_access
@login_required
def project_view(request, project_code, user):
    start_time = time.time()
    
    if not odoo.odoo_server.IsAuthenticated():
        return HttpResponseNotFound(
            "Se perdió la conexión con el servidor de Odoo. Contacta al círculo de administración."
        )

    project = Project.objects.filter(code=project_code).first()
    if not project:
        return HttpResponseNotFound("Proyecto no encontrado.")

    is_member = ProjectMembership.objects.filter(user=user, project=project).exists()

    if not is_member and not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    # Inicializar variables
    invoices = []
    transactions = []

    # Medir tiempo de get_cash_accounts
    t0 = time.time()
    cash_accounts = get_cash_accounts(project)
    _logger.info(f"get_cash_accounts took {time.time() - t0:.2f} seconds")

    # Medir tiempo de get_project_members
    t0 = time.time()
    project_members = get_project_members(project)
    _logger.info(f"get_project_members took {time.time() - t0:.2f} seconds")
    
    # Medir tiempo de get_journal_id_from_code
    t0 = time.time()
    journal_id = odoo.accounts.get_journal_id_from_code(f"G.{project.code}")
    _logger.info(f"get_journal_id_from_code took {time.time() - t0:.2f} seconds")

    # Medir tiempo de get_expenses_from_journal
    t0 = time.time()
    expenses = odoo.expenses.get_expenses_from_journal(journal_id)
    _logger.info(f"get_expenses_from_journal took {time.time() - t0:.2f} seconds")

    # Medir tiempo de get_project_total
    t0 = time.time()
    project_total, transactions = get_project_total(
        project, cash_accounts, show_contributions=True
    )
    _logger.info(f"get_project_total took {time.time() - t0:.2f} seconds")

    # Medir tiempo de get_invoices_by_project
    t0 = time.time()
    invoices = odoo.invoices.get_invoices_by_project(project.code)
    _logger.info(f"get_invoices_by_project took {time.time() - t0:.2f} seconds")

    # Medir tiempo de preparación del contexto
    t0 = time.time()
    user_is_project_admin = (
        user.email in project_members and project_members[user.email] == "admin"
    )
    user_data = User.objects.all().values("id", "email", "first_name", "last_name")
    user_dict = {
        item["email"]: item["first_name"] + item["last_name"] for item in user_data
    }
    project_data = Project.objects.all().values("code", "name")
    project_dict = {item["code"]: item["name"] for item in project_data}
    all_accounts = {**user_dict, **project_dict}
    _logger.info(f"Context preparation took {time.time() - t0:.2f} seconds")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "project_view",
        "user_is_project_admin": user_is_project_admin,
        "user_id": user.id,
        "user_email": user.email,
        "project_total": project_total,
        "project": project,
        "members": project_members,
        "available_balance": project_total["available_balance"],
        "from_account": project.code,
        "cash_accounts": cash_accounts,
        "transactions": transactions,
        "all_accounts": all_accounts,
        "expenses": expenses,
        "invoices": invoices,
    }
    
    context = add_menu_context(request, context, "project_view")
    
    total_time = time.time() - start_time
    _logger.info(f"Total project_view execution took {total_time:.2f} seconds")
    
    return render(request, "frontend/get_project.html", context)


def get_cash_accounts(project):
    cash_accounts = {}
    for c in project.cash_accounts.all():
        account_code = str(c.account_number)
        total = odoo.accounts.get_total_balances(account_code, "bank")
        cash_accounts[account_code] = {"name": c.name}
        cash_accounts[account_code]["balance"] = total
        cash_accounts[account_code]["admins"] = c.user_admins.all()
        cash_accounts[account_code]["id"] = c.id
    return cash_accounts


def get_project_members(project):
    members = {}
    for m in project.memberships.all():
        members[m.user.email] = m.role
    return members


class IBANValidatorView(View):
    def get(self, request, iban_code):
        url = "https://www.ibancalculator.com/iban_validieren.html"
        form_data = {
            "tx_valIBAN_pi1[iban]": iban_code,
            "tx_valIBAN_pi1[fi]": "fi",
            "no_cache": "1",
            "Action": "validate IBAN, look up BIC",
        }

        response = requests.post(url, data=form_data)

        if response.status_code == 200:
            tree = html.fromstring(response.content)
            iban_checksum_correct = tree.xpath(
                "//p[contains(text(), 'The IBAN checksum is correct.')]/text()"
            )

            # Check for bank information in both <b> and <a> tags
            bank_info_b = tree.xpath(
                "//p[b[contains(text(), 'Bank:')]]/b/following-sibling::text()"
            )
            bank_info_a = tree.xpath("//p[b[contains(text(), 'Bank:')]]/a/text()")

            result = {}
            if iban_checksum_correct:
                result["ok"] = True
                if bank_info_b[0].strip():
                    result["bank"] = bank_info_b[0].strip()
                elif bank_info_a:
                    result["bank"] = bank_info_a[0].strip()
                else:
                    result["bank"] = "Bank information not found."
            else:
                result["ok"] = False

            return JsonResponse(result)
        else:
            return JsonResponse({"error": "Failed to retrieve the page."}, status=500)


@check_user_access
@login_required
def my_dashboard_view(request, user):
    user_projects = utils.get_user_projects(user, exclude_main=True)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "my_dashboard",
        "user_id": user.id,
        "user": user,
        "user_email": user.email,
        "user_projects": user_projects,
    }
    context = add_menu_context(request, context, "my_dashboard")
    return render(request, "frontend/my_dashboard.html", context)


@check_user_access
@login_required
def send_new_comment(request, user):
    if request.method == "POST":
        body = request.POST.get("body")
        res_id = request.POST.get("res_id")
        res_id = int(
            res_id.strip("'")
        )  # We receive res_id as a string so we need to clean and conver to int
        odoo_model = request.POST.get("odoo_model")
        author_res_parner_id = odoo.accounts.get_partner_id(
            user.odooprofile.odoo_user_id
        )
        message = odoo.messages.create_message(
            body, res_id, odoo_model, author_res_parner_id
        )

        if message:
            messages.success(request, "Comentario enviado correctamente.")
            details = {
                "username": user.first_name + " " + user.last_name,
                "odoo_model": odoo_model,
                "res_id": res_id,
                "message": body,
            }
            odoo.utils.send_email_new_message_created(details)
            return redirect(request.META.get("HTTP_REFERER"))
        else:
            messages.error(request, "Ha ocurrido un error enviado el comentario.")
            return redirect(request.META.get("HTTP_REFERER"))

    return JsonResponse({"error": "Método no permitido"}, status=405)
