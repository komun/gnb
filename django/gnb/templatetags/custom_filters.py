from django import template

register = template.Library()

@register.filter
def subtract(value, arg):
    try:
        return float(value) - float(arg)
    except (ValueError, TypeError):
        return value
    
@register.filter(name="change_sign")
def change_sign(value):
    if value:
        return value * -1
    else:
        return 0


@register.filter
def hash(h, key):
    return h.get(key, None)

@register.filter
def contribution_percentage(contributions, account_sales):
    try:
        # Si las contribuciones y las ventas son 0, devolver vacío
        if contributions == 0 and account_sales == 0:
            return ""
        # Si las ventas son 0 pero hay contribuciones, devolver 100%
        if account_sales == 0 and contributions > 0:
            return "100 %"
        # Evitar división por cero y calcular el porcentaje
        percentage = (contributions / account_sales) * 100 if account_sales != 0 else 0
        return f"{percentage:.2f} %"
    except (TypeError, ZeroDivisionError):
        return 0