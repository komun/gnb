from adminsortable2.admin import SortableAdminMixin

from django.contrib import admin
from django.utils.text import slugify
from django.contrib.auth.models import User, Group
from baton.autodiscover.admin import BatonAdminSite

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import (
    UserProfile,
    Project,
    ProjectMembership,
    CashAccount,
    Payment,
    PaymentStatus,
    PaymentScope,
    AccountTax
)
from odoo.models import OdooProfile  # Import the OdooProfile model from the odoo app
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from django import forms
from .models import UserProfile
from odoo.models import OdooProfile  # Import the OdooProfile model from the odoo app
from .utils import get_user_projects




class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        "paid_checkbox",
        "status",
        "due_date",
        "amount",
        "to_account",
        "iban_account",
        "from_account",
        "payment_scope",
    )
    list_filter = ("status", "payment_scope")
    search_fields = ("to_account", "from_account", "iban_account")

    def paid_checkbox(self, obj):
        return obj.paid

    paid_checkbox.boolean = True
    paid_checkbox.short_description = "Paid"


admin.site.register(Payment, PaymentAdmin)


# Define the inline admin class for UserProfile
class UserProfileInline(
    admin.StackedInline
):  # You can use StackedInline or TabularInline as per your preference
    model = UserProfile
    can_delete = False  # Prevent the deletion of UserProfile when editing User


class ProjectMembershipInline(
    admin.TabularInline
):  # or admin.StackedInline for a different display style
    model = ProjectMembership


class CashAccountInline(admin.TabularInline):
    model = CashAccount
    extra = 1  # Puedes ajustar esto según tus necesidades


class ProjectAdminForm(forms.ModelForm):
    class Meta:
        model = Project
        widgets = {
            "account_checking": forms.TextInput(attrs={"style": "width: 200px;"}),
            "account_cash": forms.TextInput(attrs={"style": "width: 200px;"}),
            "account_expenses": forms.TextInput(attrs={"style": "width: 200px;"}),
            "account_sales": forms.TextInput(attrs={"style": "width: 200px;"}),
            "account_loans": forms.TextInput(attrs={"style": "width: 200px;"}),
        }
        fields = "__all__"


class ProjectAdmin(admin.ModelAdmin):
    # Especifica los campos que deseas mostrar en el formulario de creación
    add_fields = ["code", "name"]
    list_display = (
        "code",
        "name",
        "can_invoice",
        "archived",
        "account_checking",
        "account_expenses",
        "account_sales",
        "account_loans",
        "cash_account_numbers",
    )
    inlines = [ProjectMembershipInline, CashAccountInline]
    form = ProjectAdminForm
    search_fields = (
        "code",
        "name",
        "account_checking",
        "account_expenses",
        "account_sales",
        "account_loans",
    )

    def cash_account_numbers(self, obj):
        return ", ".join(
            str(account.account_number) for account in obj.cash_accounts.all()
        )

    cash_account_numbers.short_description = "Cuentas efectivo"

    # Sobrescribe el método get_fields para personalizar los campos según el contexto (creación o edición)
    def get_fields(self, request, obj=None):
        if obj:  # Edición
            return [
                "code",
                "name",
                "is_main_project",
                "can_invoice",
                "archived",
                "account_checking",
                "account_expenses",
                "account_sales",
                "account_loans",
                "percentage_recomended_income_donate",
            ]
        else:  # Creación
            return self.add_fields


admin.site.register(Project, ProjectAdmin)


# Extend the default UserAdmin
class CustomUserAdmin(DefaultUserAdmin):
    inlines = [UserProfileInline]
    list_display = (
        "username",
        "user_projects",
        "email",
        "first_name",
        "last_name",
        "is_active",
        "odooprofile_account_checking",
        "odooprofile_account_voluntary_contribution",
        "odooprofile_account_social_capital",
        "odooprofile_account_loans",
        "odooprofile_account_cash",
    )
    search_fields = (
        "username",
        "email",
        "first_name",
        "last_name",
        "odooprofile__odoo_user_id",
        "odooprofile__account_checking",
        "odooprofile__account_voluntary_contribution",
        "odooprofile__account_social_capital",
        "odooprofile__account_loans",
        "odooprofile__account_cash",
    )

    @admin.display(description="C.Préstamos")
    def odooprofile_account_loans(self, obj):
        return obj.odooprofile.account_loans if obj.odooprofile else None

    @admin.display(description="C.Efectivo")
    def odooprofile_account_cash(self, obj):
        return obj.odooprofile.account_cash if obj.odooprofile else None

    @admin.display(description="C.Corriente")
    def odooprofile_account_checking(self, obj):
        return obj.odooprofile.account_checking if obj.odooprofile else None

    @admin.display(description="C.Aportación Voluntaria")
    def odooprofile_account_voluntary_contribution(self, obj):
        return (
            obj.odooprofile.account_voluntary_contribution if obj.odooprofile else None
        )

    @admin.display(description="C.Capital Social")
    def odooprofile_account_social_capital(self, obj):
        return obj.odooprofile.account_social_capital if obj.odooprofile else None

    @admin.display(description="Projects")
    def user_projects(self, obj):
        pnames = get_user_projects(obj).keys()
        return ", ".join(pnames)

    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal Info"), {"fields": ("first_name", "last_name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )


# Register the custom admin class for User
try:
    admin.site.unregister(
        User
    )  # Unregister the default UserAdmin (if previously registered)
except:
    admin.site.register(User, CustomUserAdmin)  # Register your custom UserAdmin


class OdooProfileInline(
    admin.StackedInline
):  # You can use StackedInline or TabularInline as per your preference
    model = OdooProfile
    max_num = 1  # Set the number of empty forms to display (0 means none)
    can_delete = False  # Prevent the deletion of OdooProfile when editing User


# Register the OdooProfileInline within the CustomUserAdmin
CustomUserAdmin.inlines += [OdooProfileInline, ProjectMembershipInline]

# Move this registration after all other model registrations

class AccountTaxAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('order', 'name', 'tax_id_display', 'tax_amount', 'type_tax_use_display', 'is_active')
    list_editable = ['is_active']
    list_filter = ['type_tax_use']
    search_fields = ['name']
    ordering = ['order', 'type_tax_use']
    sortable_by = 'order'  # Especifica que solo el campo order es ordenable
    sortable_excludes = ['type_tax_use']  # Excluye type_tax_use del ordenamiento
    sortable_by_group = 'type_tax_use'  # Agrupa el ordenamiento por type_tax_use

    @admin.display(description='Tax ID')
    def tax_id_display(self, obj):
        return obj.odoo_account_tax_id

    @admin.display(description='Ámbito del impuesto')
    def type_tax_use_display(self, obj):
        return 'Ventas' if obj.type_tax_use == 'sell' else 'Compras'

admin.site.register(AccountTax, AccountTaxAdmin)
