#!/usr/bin/python
# -*- coding: utf-8 -*-

import pprint
import logging
import xmlrpc.client
import ssl

_logger = logging.getLogger("OdooClient ")


class ServiceManager(object):
    """
    Class Will service Odoo service proxy:
    @param url      : Database Connection URL
    @param service  : Name of the service called
    @param version  : Odoo WebService Version No default(2)

    """

    def __str__(self):
        return "<Object XMLRPC-ConnectionManger>"

    def __init__(self, url, service, version=2):

        if service not in ('common', 'object', 'report', 'db'):
            raise NotImplementedError("Unknown Service {service}.".format(service=service))
        self._service = service
        self._version = version
        self._url = url
        if version:
            self._proxy = xmlrpc.client.ServerProxy("{url}/xmlrpc/{version}/{service}".format(url=self._url, version=self._version, service=self._service), allow_none=True, context=ssl._create_unverified_context())
        else:
            self._proxy = xmlrpc.client.ServerProxy("{url}/xmlrpc/{service}".format(url=self._url, service=self._service), allow_none=True, context=ssl._create_unverified_context())

    def Trasmit(self, method, *args, **kwargs):
        try:
            response = getattr(self._proxy, method)(*args)
            _logger.debug("RPC Response of Method `%s` -> %s"%(method, response))
            return response
        except xmlrpc.client.ProtocolError as err:
            _logger.debug("A protocol error occurred: \n - URL :{url}\n - Error Code : {code}\n - Error message: {msg}".format(url=err.url, code=err.errcode, msg=err.errmsg))
            raise err
        except Exception as er:
            _logger.debug("Unexpected Error : \n{e}".format(e=er))
            raise er


import logging

_logger = logging.getLogger(__name__)

class Connection:
    """
    Odoo Connection Handler

    @param url      : Database Connection URL
    @param version  : Odoo Web Service Version (default: 2)
    """

    def __init__(self, url, version=2):
        self._url = url
        self._version = version
        self._serverinfo = {}

        # Inicializa ServiceManager una vez
        self._common_service = ServiceManager(self._url, 'common', self._version)
        self._object_service = ServiceManager(self._url, 'object', self._version)
        self._report_service = ServiceManager(self._url, 'report', self._version)

    def __str__(self):
        return f"<Connection-{self._url}>"

    def GetServerInfo(self):
        """Obtiene información del servidor Odoo."""
        try:
            self._serverinfo = self._common_service.Trasmit('version')
            return self._serverinfo
        except Exception as e:
            _logger.error(f"Error fetching server info: {e}")
            raise  # Re-lanza la excepción

    def Authenticate(self, db, user, password, session=None):
        """Autenticación en Odoo."""
        session = session or {}
        try:
            response = self._common_service.Trasmit('authenticate', db, user, password, session)
            if response:
                _logger.debug(f"Successful Authentication: {user} on {db}")
            else:
                _logger.warning(f"Failed Authentication: {user} on {db}")
            return response
        except Exception as e:
            _logger.error(f"Authentication Exception: {e}")
            raise

    def Model(self, db, uid, password, model, method, *args, **kwargs):
        """Ejecuta métodos sobre modelos Odoo."""
        try:
            return self._object_service.Trasmit('execute_kw', db, uid, password, model, method, args, kwargs)
        except Exception as e:
            _logger.error(f"Model Exception: {e}")
            raise

    def Report(self, db, uid, password, report_service, record_ids, *args, **kwargs):
        """Genera reportes en Odoo."""
        try:
            return self._report_service.Trasmit('render_report', db, uid, password, report_service, record_ids, args, kwargs)
        except Exception as e:
            _logger.error(f"Report Exception: {e}")
            raise
