#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from . import connection

FORMAT = "%(asctime)-15s - %(url)s - %(user)s :: "
# logging.basicConfig(format=FORMAT, level=logging.DEBUG)

_logger = logging.getLogger("GNB")


class OdooClient(object):
    """
    >>> from odooclient import client
    >>> client.OdooClient(protocol='xmlrpc', host='firebug.odoo.com',  port=443, dbname='firebug', saas=True)


    """

    def __init__(
        self,
        protocol="xmlrpc",
        host="localhost",
        port=8069,
        version=2,
        dbname=None,
        saas=False,
        debug=False,
    ):
        if debug:
            logging.basicConfig(level=logging.DEBUG)
        if saas:
            protocol, port = "xmlrpcs", 443

        self._served_protocols = {"xmlrpc": "http", "xmlrpcs": "https"}

        self._protocol = self.__GetProtocol(protocol)
        self._host = host
        self._port = port
        self._db = dbname
        self._version = version

        self._login = False
        self._password = False
        self._uid = False

        self._serverinfo = {}

        self._url = "{protocol}://{host}:{port}".format(
            protocol=self._protocol,
            host=self._host,
            port=self._port,
        )
        _logger.debug("Url -> {url}".format(url=self._url))
        # ✅ Conexión persistente
        self._connection = None
        self._initialize_connection()


    def __str__(self):
        return f"<Object ServerProxy-{self._url}>"

    def __GetProtocol(self, protocol):
        if protocol not in self._served_protocols:
            raise NotImplementedError(
                "The protocol '{0}' is not supported by the\
                         OdooClient. Please choose a protocol among these ones: {1}\
                        ".format(
                    protocol, self._served_protocols
                )
            )
        return self._served_protocols.get(protocol)

    def _initialize_connection(self):
        """Initialize or reinitialize the connection"""
        _logger.info(f"Initializing connection to {self._url}")
        self._connection = connection.Connection(self._url, version=self._version)
        _logger.info("Connection initialized successfully")

    def _check_connection(self):
        """Check if the connection is healthy and reconnect if necessary"""
        try:
            _logger.debug("Checking connection health...")
            self._connection.GetServerInfo()
            _logger.debug("Connection check successful")
            return True
        except Exception as e:
            _logger.warning(f"Connection check failed: {e}. Attempting to reconnect...")
            try:
                self._initialize_connection()
                if self.IsAuthenticated():
                    _logger.info("Re-authenticating after reconnection...")
                    self.Authenticate(self._login, self._password)
                return True
            except Exception as e:
                _logger.error(f"Reconnection failed: {e}")
                return False

    def _ensure_connection(func):
        """Decorator to ensure connection is healthy before making calls"""
        def wrapper(self, *args, **kwargs):
            retry_count = 3
            for attempt in range(retry_count):
                try:
                    if not self._check_connection():
                        raise Exception("Could not establish connection to Odoo")
                    return func(self, *args, **kwargs)
                except Exception as e:
                    if attempt == retry_count - 1:  # Last attempt
                        _logger.error(f"Failed after {retry_count} attempts: {e}")
                        raise
                    _logger.warning(f"Attempt {attempt + 1} failed: {e}. Retrying...")
            return None
        return wrapper

    def ServerInfo(self):
        """
        Code :
            common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
            common.version()
        """
        self._serverinfo = self._connection.GetServerInfo()
        return self._serverinfo

    def IsAuthenticated(self):
        cn = all([self._uid, self._login, self._password]) and True or False
        return cn

    def Authenticate(self, login, pwd):
        """
        Code :
            common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
            uid = common.authenticate(db, username, password, {})
        """
        self._login, self._password = login, pwd
        self._uid = self._connection.Authenticate(self._db, login, pwd, {})
        return self._uid

    def Login(self, login, pwd):
        """
        Code :
            common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
            uid = common.authenticate(db, username, password, {})
        """
        self._login, self._password = login, pwd
        self._uid = self._connection.Login(self._db, login, pwd)
        return self._uid

    def CheckSecurity(self, model, operation_modes=["read"]):
        """
        models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
        models.execute_kw(db, uid, password,
                                        'res.partner', 'check_access_rights',
                                        ['read'], {'raise_exception': False})
        """
        """Verifica los permisos de seguridad para las operaciones especificadas."""
        results = {mode: False for mode in operation_modes}
        for mode in operation_modes:
            response = self._connection.Model(
                self._db,
                self._uid,
                self._password,
                model,
                "check_access_rights",
                mode,
                raise_exception=False,
            )
            results[mode] = response
        return results

    def Method(self, model, method, *args, **kwargs):
        """
        Generic Method Call if you don't find specific implementation.

        models.execute_kw(db, uid, password,
            '<any model>', '<any method>', args1, args1, ..., argsN
            {'kwy': ['val1', 'val2', 'valn'], 'key2': val2})
        """
        """Llama a un método genérico de un modelo."""
        return self._connection.Model(
            self._db, self._uid, self._password, model, method, *args, **kwargs
        )

    @_ensure_connection
    def Read(self, model, document_ids, fields=False, context=None):
        """
        models.execute_kw(db, uid, password,
                    'res.partner', 'read',
                    [ids], {'fields': ['name', 'country_id', 'comment']})
        """
        _logger.info(f"READ: model={model}, ids={document_ids}, fields={fields}, context={context}")
        try:
            result = self._connection.Model(
                self._db, self._uid, self._password, model, "read", document_ids, fields=fields, context=context
            )
            _logger.info(f"READ RESULT: Found {len(result) if result else 0} records")
            return result
        except Exception as e:
            _logger.error(f"READ ERROR: {str(e)}")
            raise

    @_ensure_connection
    def Search(self, model, domain=False, context=None, **kwargs):
        """
        search(self, cr, user, args, offset=0, limit=None, order=None, context=None, count=False)
        """
        _logger.info(f"SEARCH: model={model}, domain={domain}, context={context}, kwargs={kwargs}")
        try:
            result = self._connection.Model(
                self._db, self._uid, self._password, model, "search", domain or [], **kwargs
            )
            _logger.info(f"SEARCH RESULT: Found {len(result) if result else 0} records")
            return result
        except Exception as e:
            _logger.error(f"SEARCH ERROR: {str(e)}")
            raise

    @_ensure_connection
    def SearchCount(self, model, domain=False, context=None, **kwargs):
        """
        search_count(self, cr, user, args, context=None):
        """
        _logger.info(f"SEARCH_COUNT: model={model}, domain={domain}, context={context}, kwargs={kwargs}")
        try:
            result = self._connection.Model(
                self._db, self._uid, self._password, model, "search_count", domain or [], **kwargs
            )
            _logger.info(f"SEARCH_COUNT RESULT: Count = {result}")
            return result
        except Exception as e:
            _logger.error(f"SEARCH_COUNT ERROR: {str(e)}")
            raise

    @_ensure_connection
    def SearchRead(self, model, domain=False, fields=False, context=None, **kwargs):
        """
        search_read(self, cr, uid, domain=None, fields=None, offset=0,
                        limit=None, order=None, context=None):
        """
        _logger.info(f"SEARCH_READ: model={model}, domain={domain}, fields={fields}, context={context}, kwargs={kwargs}")
        try:
            result = self._connection.Model(
                self._db, self._uid, self._password, model, "search_read", domain or [], fields=fields, **kwargs
            )
            _logger.info(f"SEARCH_READ RESULT: Found {len(result) if result else 0} records")
            return result
        except Exception as e:
            _logger.error(f"SEARCH_READ ERROR: {str(e)}")
            raise

    @_ensure_connection
    def NameSearch(self, model, name, domain=False, context=None, **kwargs):
        """
        name_search(name='', domain=None, operator='ilike', limit=100)

        models.execute_kw(db, uid, password,
            'res.partner', 'name_search',<name_to_search>
            [[['is_company', '=', True], ['customer', '=', True]]],
            {'offset': 10, 'limit': 5})

        """
        """Realiza una búsqueda de nombres de registros."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "name_search", name, domain or [], **kwargs
        )

    @_ensure_connection
    def Create(self, model, values, context=None, **kwargs):
        """
        create(self, vals):

        id = models.execute_kw(db, uid, password, 'res.partner', 'create', [{
            'name': "New Partner",
        }])

        """
        """Crea un nuevo registro con los valores proporcionados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "create", values, **kwargs
        )

    @_ensure_connection
    def NameCreate(self, model, name, context=None):
        """
        name_create(name, context)

        models.execute_kw(db, uid, password,
            'res.partner', 'name_create',<name_to_search>
            [[['is_company', '=', True], ['customer', '=', True]]],
            {'offset': 10, 'limit': 5})

        """
        """Crea un registro basado en el nombre proporcionado."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "name_create", name, **kwargs
        )

    @_ensure_connection
    def Write(self, model, document_ids, values, context=None, **kwargs):
        """
        write(self, vals):

        models.execute_kw(db, uid, password, 'res.partner', 'write', [[id], {
            'name': "Newer partner"
        }])
        """
        """Actualiza los valores de los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "write", document_ids, values, **kwargs
        )

    @_ensure_connection
    def GetFields(self, model, context=None, attributes=None, **kwargs):
        """
        fields_get(self, cr, user, allfields=None, context=None,
                                    write_access=True, attributes=None)
        models.execute_kw(
            db, uid, password, 'res.partner', 'fields_get',
            [], {'attributes': ['string', 'help', 'type']})
        """
        """Obtiene los campos del modelo especificado."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "fields_get", **kwargs
        )

    @_ensure_connection
    def Unlink(self, model, document_ids, context=None, **kwargs):
        """
        models.execute_kw(db, uid, password,
                          'res.partner', 'unlink', [[id]])
        """
        """Elimina los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "unlink", document_ids, **kwargs
        )

    @_ensure_connection
    def Copy(self, model, document_ids, default=None, context=None, **kwargs):
        """
        copy(default=None)
        models.execute_kw(db, uid, password,
                          'res.partner', 'copy', [id], {'field1': "default values"})
        """
        """Copia un registro, aplicando valores predeterminados si es necesario."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "copy", document_ids, default=default or {}, **kwargs
        )

    @_ensure_connection
    def CreateWorkflow(self, model, document_ids, context=None, **kwargs):
        """
        def create_workflow(self, cr, uid, ids, context=None):
            Create a workflow instance for each given record IDs
        """
        """Crea una instancia de flujo de trabajo para los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "create_workflow", document_ids, **kwargs
        )

    @_ensure_connection
    def UnlinkWorkflow(self, model, document_ids, context=None, **kwargs):
        """
        def delete_workflow(self, cr, uid, ids, context=None):
        Delete the workflow instances bound to the given record IDs.
        """
        """Elimina las instancias de flujo de trabajo de los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "delete_workflow", document_ids, **kwargs
        )

    @_ensure_connection
    def StepWorkflow(self, model, document_ids, context=None, **kwargs):
        """
        def step_workflow(self, cr, uid, ids, context=None):
        Reevaluate the workflow instances of the given record IDs.
        """
        """Realiza un paso de reevaluación del flujo de trabajo para los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "step_workflow", document_ids, **kwargs
        )

    @_ensure_connection
    def SignalWorkflow(self, model, document_ids, signal, context=None, **kwargs):
        """
        def signal_workflow(self, cr, uid, ids, signal, context=None):
        Send given workflow signal and return a dict mapping ids to workflow results
        """
        """Envía una señal de flujo de trabajo a los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "signal_workflow", document_ids, signal, **kwargs
        )

    @_ensure_connection
    def RedirectWorkflow(self, model, old_new_ids, context=None, **kwargs):
        """
        def redirect_workflow(self, cr, uid, old_new_ids, context=None):
        Rebind the workflow instance bound to the given 'old' record IDs to
            the given 'new' IDs. (``old_new_ids`` is a list of pairs ``(old, new)``.
        """
        """Redirige el flujo de trabajo de los registros antiguos a los nuevos."""
        kwargs.setdefault("context", context or {})
        return self._connection.Model(
            self._db, self._uid, self._password, model, "redirect_workflow", old_new_ids, **kwargs
        )

    @_ensure_connection 
    def PrintReport(self, report_service, record_ids, context=None, **kwargs):
        """
        invoice_ids = models.execute_kw(
            db, uid, password, 'account.invoice', 'search',
            [[('type', '=', 'out_invoice'), ('state', '=', 'open')]])
        report = xmlrpclib.ServerProxy('{}/xmlrpc/2/report'.format(url))
        result = report.render_report(
            db, uid, password, 'account.report_invoice', invoice_ids)
        report_data = result['result'].decode('base64')

        """
        """Genera un informe para los registros especificados."""
        kwargs.setdefault("context", context or {})
        return self._connection.Report(
            self._db, self._uid, self._password, report_service, record_ids, **kwargs
        )
