# Recomendaciones de desarrollo

## Instalación de entorno

## Requisitos previos

--- PC con gnu/linux - Las pruebas las estoy realizando con Ubuntu.22 intuyo que con una debian o entornos de paquetería .deb puede ser similar. ---

- python +3.10
- python3-dev libpq-dev
- python3-pip

### Proceso de instalación:

`sudo apt-get install python3.10 python3.10-venv python3.10-dev python3-venv python3-dev`


`pip3 install virtualenv`

`pip3 install virtualenvwrapper`

`pip3 install workon`


### Añadimos al archivo al .bashrc


`export WORKON_HOME=$HOME/.virtualenvs`

`export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3`

`export VIRTUALENVWRAPPER_VIRTUALENV=/home/$USER/.local/bin/virtualenv`

Ubicación predeterminada donde se crearán los directorios de los proyectos al utilizar el comando mkproject:

`export PROJECT_HOME=$HOME/python_projets`

Buscar la ruta de "virtualenvwrapper.sh" y añadir la siguiente linea tambien a bashrc:

`sudo find / -name virtualenvwrapper.sh 2>/dev/null`

Y añadimos la ruta, por ejemplo **(no tiene porque ser esta)**:

`source /usr/local/bin/virtualenvwrapper.sh`


### Creamos el entorno virtual:

Recargamos la terminal con:

`source ~/.bashrc`

`mkvirtualenv --python=/usr/bin/python3.10 gnb`

Con `deactivate` salimos del entorno virtual.

Entramos dentro del entorno:

`workon gnb`


## Descarga el source dentro del virtualenv
- git clone https://framagit.org/komun/gnb.git
- cd django

## Instala el resto de Requisitos
- pip install -r requierements.txt

## Activa modo desarrollo
- python manage.py makemigrations
- python manage.py migrate
- python manage.py loaddata initial_account_taxes
- python manage.py createsuperuser
- python manage.py runserver

Comprueba que funciona en el navegador https://127.0.0.1:8000

