import re
import subprocess
from collections import defaultdict
from datetime import datetime


def get_current_version():
    with open("django/gnb/__init__.py", "r") as f:
        content = f.read()
        match = re.search(r'__VERSION__\s*=\s*[\'"](.+?)[\'"]', content)
        if match:
            return match.group(1)
    return None


def get_commits():
    result = subprocess.run(
        ["git", "log", "--pretty=format:%H|%s|%ai"],
        capture_output=True,
        text=True,
    )
    return result.stdout.split("\n")


def get_version_from_commit(commit_hash):
    result = subprocess.run(
        ["git", "show", f"{commit_hash}:django/gnb/__init__.py"],
        capture_output=True,
        text=True,
    )
    content = result.stdout
    match = re.search(r'__VERSION__\s*=\s*[\'"](.+?)[\'"]', content)
    if match:
        return match.group(1)
    return None


def generate_changelog():
    current_version = get_current_version()
    commits = get_commits()
    changelog = defaultdict(list)

    for commit in commits:
        commit_hash, message, date_str = commit.split("|", 2)
        version = get_version_from_commit(commit_hash)
        if version:
            date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S %z")
            changelog[version].append((message, date))

    print("# Changelog\n")

    # Sort versions in descending order (newer to older)
    sorted_versions = sorted(changelog.keys(), reverse=True)

    for version in sorted_versions:
        # Get the most recent date for this version
        latest_date = max(date for _, date in changelog[version])
        print(f"## [Versión {version}] - {latest_date.strftime('%Y-%m-%d')}\n")

        # Group commits by type
        grouped_commits = defaultdict(list)
        for message, _ in changelog[version]:
            if message.lower().startswith("add"):
                grouped_commits["Añadido"].append(message)
            elif message.lower().startswith("fix"):
                grouped_commits["Corregido"].append(message)
            elif message.lower().startswith("change") or message.lower().startswith(
                "update"
            ):
                grouped_commits["Cambiado"].append(message)
            else:
                grouped_commits["Otros"].append(message)

        # Print grouped commits
        for group, messages in grouped_commits.items():
            if messages:
                print(f"### {group}")
                for msg in messages:
                    print(f"- {msg}")
                print()

    if current_version not in changelog:
        print(f"## [Versión {current_version}] - {datetime.now().strftime('%Y-%m-%d')}")
        print("- No hay commits para esta versión aún.\n")


if __name__ == "__main__":
    generate_changelog()
