# Registro de cambios

## [Versión 0.8.5] - 2024-06-25

### Añadido
- Ahora puedes ver el estado de las transacciones externas pendientes.
- Verificación de IBAN y obtención del nombre del banco.

---

## [Versión 0.8.4] - 2024-06-25

### Otros cambios
- Se recargan los scripts de JavaScript cuando cambia la versión.
- Ya no es obligatorio adjuntar un archivo en los gastos con factura.
- Se muestra el enlace al hacer facturas rectificadas.
- Se muestra el autor en gastos y facturas.

---

## [Versión 0.8.3] - 2024-06-24

### Otros cambios
- Solución rápida para varios errores menores.
- Mejoras en la búsqueda administrativa.
- Cambios en el diario de gastos ahora se hacen por código G.XXX en lugar de por nombre.
- Cambio de usuario desde administradores ahora omite el anclaje.
- Cambio de fecha de vencimiento después de confirmar pagos.
- Envío de correos para confirmar/rechazar pagos SEPA.
- Interfaz mejorada al completar transacciones con éxito.
- Comprobaciones adicionales al retirar efectivo en cajeros automáticos.
- Script para mostrar cambios por versión.
- Recordatorio de donaciones.

### Corregido
- Arreglado problema cuando no se encontraba un empleado para un usuario.

## [Versión 0.8.2] - 2024-06-22

### Añadido
- Capacidad para modificar datos del cliente durante la creación de facturas, con notificación por correo a los administradores.
- Visualización de facturas pendientes en la vista de proyecto.
- Creación automática de `employee_id` en Odoo al crear un nuevo usuario, si no existe.
- Asociación automática del Banco de la cooperativa en Odoo al crear diarios bancarios para nuevos usuarios o proyectos.
- Nueva columna "Creada por" en los listados de gastos y facturas de proyectos.

### Mejorado
- Simplificación de las cajas de información de balances para mayor claridad.
- Optimización en la carga de listados de gastos y facturas, priorizando la velocidad sobre el desglose detallado del IVA.
- Mejoras en la verificación de transferencias y donaciones
- Mejoras en la conexión con Odoo, ahora detecta cuando la conexión se ha perdido y avisa al usuario.

### Corregido
- Solucionado el error al añadir apuntes en caja de efectivo que se quejaba de campos vacíos en otro formulario.

### Nota técnica
- La extracción de montos sin IVA de cada ítem se ha pospuesto para evitar ralentizar significativamente la carga de listados de gastos y facturas.

## [Versión 0.8.0] - 2024-06-21
- Mejoras significativas en la gestión de proyectos
- Implementación de facturas rectificativas
- Soporte para Facturae
- Mejoras en la búsqueda de clientes por CIF o por nombre
- Implementación de notificaciones para retiros en cajeros automáticos

## [Versión 0.7.0] - 2024-06-08
- Soporte para centros administrativos
- Mejoras en la exportación de facturas (Facturae)
- Implementación de pagos pendientes con soporte SEPA XML
- Mejoras en la visualización de transacciones
- Integración continua y correcciones de pruebas

## [Versión 0.6.5] - 2024-04-06
- Mejoras en la interfaz de usuario del dashboard
- Implementación de modo demo
- Correcciones en la creación de usuarios y proyectos

## [Versión 0.6.4] - 2024-03-21
- Correcciones en la autenticación y visualización de donaciones

## [Versión 0.6.3] - 2024-03-21
- Mejoras en la visualización de facturas y donaciones
- Correcciones en las pruebas y vistas

## [Versión 0.6.2] - 2024-03-19
- Refactorización de comprobaciones de perfil Odoo
- Mejoras en las vistas de facturas y gastos
- Implementación de pruebas adicionales

## [Versión 0.6.1] - 2024-02-28
- Implementación de validación de facturas
- Correcciones en la visualización de transacciones

## [Versión 0.6.0] - 2024-02-27
- Implementación de caché para mejorar el rendimiento
- Mejoras en la visualización de cuentas de efectivo y gastos
- Correcciones en el cálculo y visualización de saldos

## [Versión 0.5.0] - 2024-02-24
- Implementación de cuentas de efectivo
- Mejoras en la interfaz móvil
- Refactorización del dashboard y transacciones
- Implementación de préstamos y mejoras en la gestión de cuentas